import GithubListRepositories from './GithubListRepositories';
import SpotifyGetPlaylists from './SpotifyGetPlaylists';
import BourseConvertCurrency from "./BourseConvertCurrency";
import WeatherCityTemperature from "./WeatherCityTemperature";
import GithubCountFollowers from "./GithubCountFollowers";
import GithubListFollowers from "./GithubListFollowers";

// Export default jsx
export default {
    'github_list_repositories': GithubListRepositories,
    'spotify_get_playlists': SpotifyGetPlaylists,
    'bourse_convert_currency': BourseConvertCurrency,
    'weather_city_temperature': WeatherCityTemperature,
    'github_count_followers': GithubCountFollowers,
    'github_list_followers': GithubListFollowers
};