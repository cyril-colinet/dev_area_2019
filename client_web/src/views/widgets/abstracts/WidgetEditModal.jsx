import React, {Component} from "react";
import {
    Button,
    Col,
    Form,
    FormGroup, Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText, Modal,
    Row,
    UncontrolledAlert
} from "reactstrap";
import request from "../../../utils/request";

class WidgetEditModal extends Component {

    /**
     * Constructor of widget
     * @param props Properties passed as parameter
     */
    constructor(props) {
        super(props);

        // Set state of widget
        this.state = {
            editModal: false,
            widgetParams: [],
            formDataObj: {
                service: this.props.uuid
            }
        };
    }

    /**
     * Toggle modal for widget
     */
    toggleModal() {
        this.props.onToggle();
    }

    /**
     * When component init mounting
     */
    componentDidMount() {
        request.get('/api/user/widgetInfo/' + this.props.uuid).then(res => {
            let data = res.data;
            if (data == null)
                return;

            this.setState({
                formDataObj: Object.assign(this.state.formDataObj, data.params),
                widgetParams: Object.keys(data.params)
            });
        });
    }

    /**
     * When user would like to edit the current widget
     */
    editWidget() {
        if (this.state.formSubmited)
            return;

        // Check if size and frequency is set
        let keys = Object.keys(this.state.formDataObj);
        if (!keys.includes("size") || !keys.includes("refresh"))
            return this.setState({error: "Veillez à ne pas oublier la fréquence de raffraichissement et la taille !"});

        // Check if all parameters is set
        let missing = false;
        // eslint-disable-next-line array-callback-return
        this.state.widgetParams.map(function(param) {
            if (!keys.includes(param))
                missing = true;
        });

        // Add error if missing parameters
        if (missing)
            return this.setState({error: "Veillez à ne pas oublier de remplir tout les champs du formulaire !"});

        // Form submit and add widget
        request.request({
            method: 'POST',
            url: "/api/user/editWidget",
            data: this.state.formDataObj
        }).then(res => {
            let data = res.data;
            if (data == null)
                return;

            window.location.reload();
        }).catch(console.error);
    }

    /**
     * Display widget parameters with values, and update it id changed
     * @returns {*}
     */
    displayWidgetParametersWithValues() {
        if (Object.keys(this.state.formDataObj).length === 0)
            return "";

        return Object.keys(this.state.formDataObj).map(key => {
            if (key === "title" || key === "size" || key === "refresh" || key === "service")
                return "";

            // Add params
            let param = this.state.formDataObj[key];
            return (
                <FormGroup className="mb-3" key={key}>
                    <InputGroup className="input-group-alternative">
                        <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                                <i className="ni ni-planet" />
                            </InputGroupText>
                        </InputGroupAddon>
                        <Input value={param}
                               name={key}
                               required={true}
                               onChange={(input) => {
                                   let paramObject = this.state.formDataObj;
                                   paramObject[key] = input.target.value;

                                   // Set state with value
                                   this.setState({formDataObj: paramObject})
                               }}
                               type="text" />
                    </InputGroup>
                </FormGroup>
            );
        });
    }

    /**
     * Render component
     * @returns {*}
     */
    render() {
        return (
            <>
                <Modal
                    className="modal-dialog-centered modal-lg"
                    isOpen={this.props.editModal}>
                    <div className="modal-header">
                        <h6 className="modal-title" style={{fontSize: '16px'}} id="modal-title-default">
                            {this.state.formDataObj.title}
                        </h6>
                        <button
                            aria-label="Close"
                            className="close"
                            data-dismiss="modal"
                            type="button"
                            onClick={() => this.toggleModal()}>
                            <span aria-hidden={true}>&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        {
                            // Display error
                            this.state.error != null ?
                                (
                                    <UncontrolledAlert color="danger">{this.state.error}</UncontrolledAlert>
                                ) : ""
                        }
                        <Form role="form">
                            {this.displayWidgetParametersWithValues()}
                            <Row>
                                <Col md="6">
                                    <h4 className="mb-3">Nom d'affichage du widget</h4>
                                    <FormGroup className="mb-3">
                                        <InputGroup className="input-group-alternative">
                                            <InputGroupAddon addonType="prepend">
                                                <InputGroupText>
                                                    <i className="ni ni-ruler-pencil" />
                                                </InputGroupText>
                                            </InputGroupAddon>
                                            <Input placeholder="Entrez un nom de Widget"
                                                   required={true}
                                                   value={this.state.formDataObj.title}
                                                   onChange={(input) => {
                                                       let paramObject = this.state.formDataObj;
                                                       paramObject["title"] = input.target.value;

                                                       // Set state with value
                                                       this.setState({formDataObj: paramObject})
                                                   }}
                                                   type="text" />
                                        </InputGroup>
                                    </FormGroup>
                                </Col>
                                <Col md="6">
                                    <h4 className="mb-3">Taille du widget en longneur (en pourcentage)</h4>
                                    <Button color={this.state.formDataObj.size === "3" ? "dark" : "info"}
                                            type="button"
                                            onClick={() => {
                                                let paramObject = this.state.formDataObj;
                                                paramObject['size'] = "3";

                                                // Set state with value
                                                this.setState({formDataObj: paramObject})
                                            }}>
                                        25%
                                    </Button>
                                    <Button color={this.state.formDataObj.size === "6" ? "dark" : "info"}
                                            type="button"
                                            onClick={() => {
                                                let paramObject = this.state.formDataObj;
                                                paramObject['size'] = "6";

                                                // Set state with value
                                                this.setState({formDataObj: paramObject})
                                            }}>
                                        50%
                                    </Button>
                                    <Button color={this.state.formDataObj.size === "9" ? "dark" : "info"}
                                            type="button"
                                            onClick={() => {
                                                let paramObject = this.state.formDataObj;
                                                paramObject['size'] = "9";

                                                // Set state with value
                                                this.setState({formDataObj: paramObject})
                                            }}>
                                        75%
                                    </Button>
                                    <Button color={this.state.formDataObj.size === "12" ? "dark" : "info"}
                                            type="button"
                                            onClick={() => {
                                                let paramObject = this.state.formDataObj;
                                                paramObject['size'] = "12";

                                                // Set state with value
                                                this.setState({formDataObj: paramObject})
                                            }}>
                                        100%
                                    </Button>
                                </Col>
                                <Col md="12">
                                    <h4 className="mb-3">Fréquence de raffraichissement du widget</h4>
                                    <Button color={this.state.formDataObj.refresh === "30000" ? "dark" : "warning"}
                                            type="button"
                                            onClick={() => {
                                                let paramObject = this.state.formDataObj;
                                                paramObject['refresh'] = "30000";

                                                // Set state with value
                                                this.setState({formDataObj: paramObject})
                                            }}>
                                        30 secondes
                                    </Button>
                                    <Button color={this.state.formDataObj.refresh === "60000" ? "dark" : "warning"}
                                            type="button"
                                            onClick={() => {
                                                let paramObject = this.state.formDataObj;
                                                paramObject['refresh'] = "60000";

                                                // Set state with value
                                                this.setState({formDataObj: paramObject})
                                                console.log((this.state.formDataObj))
                                            }}>
                                        1 minute
                                    </Button>
                                    <Button color={this.state.formDataObj.refresh === "90000" ? "dark" : "warning"}
                                            type="button"
                                            onClick={() => {
                                                let paramObject = this.state.formDataObj;
                                                paramObject['refresh'] = "90000";

                                                // Set state with value
                                                this.setState({formDataObj: paramObject})
                                            }}>
                                        1 minute et 30 secondes
                                    </Button>
                                    <Button color={this.state.formDataObj.refresh === "120000" ? "dark" : "warning"}
                                            type="button"
                                            onClick={() => {
                                                let paramObject = this.state.formDataObj;
                                                paramObject['refresh'] = "120000";

                                                // Set state with value
                                                this.setState({formDataObj: paramObject})
                                            }}>
                                        2 minutes
                                    </Button>
                                    <Button color={this.state.formDataObj.refresh === "300000" ? "dark" : "warning"}
                                            type="button"
                                            onClick={() => {
                                                let paramObject = this.state.formDataObj;
                                                paramObject['refresh'] = "300000";

                                                // Set state with value
                                                this.setState({formDataObj: paramObject})
                                            }}>
                                        5 minutes
                                    </Button>
                                </Col>
                            </Row>
                        </Form>
                    </div>
                    <div className="modal-footer">
                        <Button color="success"
                                type="button"
                                onClick={() => this.editWidget()}>
                            Modifier
                        </Button>
                        <Button
                            className="ml-auto"
                            color="danger"
                            data-dismiss="modal"
                            type="button"
                            onClick={() => this.toggleModal()}>
                            Annuler
                        </Button>
                    </div>
                </Modal>
            </>
        );
    }

}

export default WidgetEditModal;