import React from "react";
import request from "../../utils/request";
import {
    Button,
    Card,
    Col,
    Row,
    CardBody,
    CardTitle
} from "reactstrap";
import WidgetEditModal from "./abstracts/WidgetEditModal";

/**
 * GithubListRepositories widget
 * @author mrlizzard
 */
class GithubCountFollowers extends React.Component {

    widgetName = "github_count_followers";

    /**
     * Constructor of widget
     * @param props
     */
    constructor(props) {
        super(props);

        // Configure state
        this.state = {
            editModal: false,
            count: 0
        };
    }

    /**
     * When component did mounted
     */
    componentDidMount() {
        // Configure interval
        this.interval = setInterval(() => {
            request.get('/api/github/countFollowers/' + this.props.username).then(res => {
                let data = res.data;
                if (data == null)
                    return;

                this.setState({count: data.count});
            });
        }, this.props.refresh);

        // Then request
        request.get('/api/github/countFollowers/' + this.props.username).then(res => {
            let data = res.data;
            if (data == null)
                return;

            this.setState({count: data.count});
        });
    }

    /**
     * Clear interval on unmounting
     */
    componentWillUnmount() {
        clearInterval(this.interval);
    }

    /**
     * Render component
     * @returns {*}
     */
    render() {
        return (
            <Col className="mb-5" md={this.props.size}>
                <Card className="card-stats mb-4 mb-xl-0">
                    <CardBody>
                        <Row>
                            <div className="col">
                                <CardTitle tag="h5"
                                           className="text-uppercase text-muted mb-0">
                                    {this.props.title}
                                </CardTitle>
                                <span className="h2 font-weight-bold mb-0">
                                    {this.state.count || "n/a"} followers
                                </span>
                            </div>
                            <Col className="col-auto">
                                <div className="icon icon-shape bg-dark text-white rounded-circle shadow">
                                    <i className="fab fa-github-alt" />
                                </div>
                            </Col>
                        </Row>
                        <div className="text-left">
                            <p className="mt-3 mb-0 text-muted text-sm">
                                <span className="text-nowrap">
                                    Nombre relatif au compte de <b>{this.props.username}</b>
                                </span>
                            </p>
                        </div>
                        <div className="mt-2 left">
                            <Button
                                color="dark"
                                onClick={(e) => this.setState({editModal: !this.state.editModal})}
                                size="sm">
                                Modifier
                            </Button>
                            <Button
                                color="danger"
                                onClick={(e) => {
                                    e.preventDefault();
                                    request.get('/api/user/deleteWidget/' + this.props.uuid).then(res => {
                                        window.location.reload();
                                    })
                                }}
                                size="sm">
                                &times;
                            </Button>
                        </div>
                    </CardBody>
                </Card>
                <WidgetEditModal {...this.props}
                                 editModal={this.state.editModal}
                                 onToggle={() => this.setState({editModal: !this.state.editModal})} />
            </Col>
        );
    }
}

export default GithubCountFollowers;