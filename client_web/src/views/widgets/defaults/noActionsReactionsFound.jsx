import React, {Component} from "react";
import {Button, Card, Container, Col} from "reactstrap";
import {ReactSVG} from "react-svg";

export default class NoActionsReactionsFound extends Component {

    render() {
        return (
            <Col className="mb-5 offset-4" md="4">
                <Card className="shadow">
                    <Container align="center">
                        <ReactSVG src={require("../../../assets/img/waiting.svg")}
                                  beforeInjection={this.animateVectorialImage} />
                        <Container className="mb-3" style={{width: '70%'}}>
                            <div className="col">
                                <h3 className="mb-0">You don't have any AREA configured</h3>
                            </div>&nbsp;
                            <p className="mt-3 mb-0 text-muted text-sm">
                                <span>
                                    If you want to add a widget you can simply click to the
                                    &nbsp;<label className="badge badge-info">Configure new AREA</label>&nbsp;
                                    button.
                                </span>
                            </p>
                            <p className="mt-3 mb-0 text-muted text-sm">
                                <span>
                                    You have a list of preconfigured presets in <b>Actions</b> and <b>Reactions</b> pages.
                                </span>
                            </p>
                            <Button
                                className="mt-3"
                                color="success"
                                href="/dashboard/configure"
                                size="lg">
                                Configure new AREA
                            </Button>
                        </Container>
                    </Container>
                </Card>
                <script>
                    {this.animateVectorialImage}
                </script>
            </Col>
        );
    }
}