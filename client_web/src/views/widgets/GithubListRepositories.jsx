import request from "../../utils/request";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Row,
    Table,
    UncontrolledAlert
} from "reactstrap";
import React from "react";
import WidgetEditModal from "./abstracts/WidgetEditModal";

/**
 * GithubListRepositories widget
 * @author mrlizzard
 */
class GithubListRepositories extends React.Component {

    widgetName = "github_list_repositories";

    /**
     * Constructor
     * @param props Properties of the widget
     */
    constructor(props) {
        super(props);

        // Configure state
        this.state = {
            editModal: false,
            respositories: []
        };
    }

    /**
     * When component did mounted
     */
    componentDidMount() {
        // Configure interval
        this.interval = setInterval(() => {
            request.get('/api/github/repositoriesList/' + this.props.username + '/' + this.props.max)
                .then(res => {
                    let data = res.data;
                    if (data == null)
                        return;

                    this.setState({respositories: data.list});
                });
        }, this.props.refresh);

        request.get('/api/github/repositoriesList/' + this.props.username + '/' + this.props.max)
            .then(res => {
                let data = res.data;
                if (data == null)
                    return;

                this.setState({respositories: data.list});
            })
    }

    /**
     * Clear interval on unmounting
     */
    componentWillUnmount() {
        clearInterval(this.interval);
    }

    /**
     * Display github repositories in list
     * @returns {*[]}
     */
    displayContent() {
        if (this.state.respositories.length === 0) {
            return (
                <tr>
                    <th colSpan="5">
                        <UncontrolledAlert color="danger">
                            Aucun repo public n'est disponible... :/
                        </UncontrolledAlert>
                    </th>
                </tr>
            );
        }

        // Not empty list
        return this.state.respositories.map(repo => {
            return (
                <tr key={repo.name}>
                    <th scope="row">{repo['name']}</th>
                    <td>{repo['stargazers_count']}</td>
                    <td>{repo['watchers_count']}</td>
                    <td>{repo['updated_at']}</td>
                    <td>
                        <img src={require('../../assets/img/icons/common/github.svg')}
                            height="20"
                            width="20"
                            className="mr-2"
                            alt={repo.name}/>
                        <a href={repo['url']} about="_blank">Voir sur Github</a>
                    </td>
                </tr>
            );
        });
    }

    /**
     * Render component
     * @returns {*}
     */
    render() {
        return (
            <Col className="mb-5" md={this.props.size}>
                <Card className="shadow">
                    <CardHeader className="border-0">
                        <Row className="align-items-center">
                            <div className="col">
                                <h3 className="mb-0">{this.props.title}</h3>
                            </div>
                            <div className="col text-right">
                                <Button
                                    color="dark"
                                    onClick={(e) => this.setState({editModal: !this.state.editModal})}
                                    size="sm">
                                    Modifier
                                </Button>
                                <Button
                                    color="danger"
                                    onClick={(e) => {
                                        e.preventDefault();
                                        request.get('/api/user/deleteWidget/' + this.props.uuid).then(res => {
                                            window.location.reload();
                                        })
                                    }}
                                    size="sm">
                                    &times;
                                </Button>
                            </div>
                        </Row>
                    </CardHeader>
                    <Table className="align-items-center table-flush" responsive>
                        <thead className="thead-light">
                        <tr>
                            <th scope="col">Nom du repo</th>
                            <th scope="col">Favoris</th>
                            <th scope="col">Watchers</th>
                            <th scope="col">Dernière mise à jour</th>
                            <th scope="col">&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.displayContent()}
                        </tbody>
                    </Table>
                </Card>
                <WidgetEditModal {...this.props}
                                 editModal={this.state.editModal}
                                 onToggle={() => this.setState({editModal: !this.state.editModal})} />
            </Col>
        );
    }
}

export default GithubListRepositories;