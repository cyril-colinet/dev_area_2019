import React from "react";
import request from "../../utils/request";
import {Button, Card, CardHeader, Col, Row, Table, Badge, UncontrolledAlert} from "reactstrap";
import WidgetEditModal from "./abstracts/WidgetEditModal";

/**
 * GithubListRepositories widget
 * @author mrlizzard
 */
class SpotifyGetPlaylists extends React.Component {

    /**
     * Constructor of widget
     * @param props
     */
    constructor(props) {
        super(props);

        // Configure state
        this.state = {
            editModal: false,
            playlists: []
        };
    }

    /**
     * When component did mounted
     */
    componentDidMount() {
        // Configure interval
        this.interval = setInterval(() => {
            request.get('/api/spotify/getPlaylists/' + this.props.spotify_id).then(res => {
                let data = res.data;
                if (data == null)
                    return;

                this.setState({playlists: data.playlists});
            });
        }, this.props.refresh);

        // Then request
        request.get('/api/spotify/getPlaylists/' + this.props.spotify_id).then(res => {
            let data = res.data;
            if (data == null)
                return;

            this.setState({playlists: data.playlists});
        });
    }

    /**
     * Clear interval on unmounting
     */
    componentWillUnmount() {
        clearInterval(this.interval);
    }

    /**
     * Display github repositories in list
     * @returns {*[]}
     */
    displayContent() {
        if (this.state.playlists.length === 0) {
            return (
                <tr>
                    <th colSpan="5">
                        <UncontrolledAlert color="danger">
                            Aucune playlist n'a été crée par {this.props.spotify_id} !
                            Go aller les contacter pour en faire une (ou plusieurs) !
                        </UncontrolledAlert>
                    </th>
                </tr>
            );
        }

        // Not empty list
        return this.state.playlists.map(playlist => {
            return (
                <tr key={playlist.name}>
                    <th scope="row">
                        <img height="130" width="130" src={playlist.picture} alt={playlist.name} />
                    </th>
                    <th>{playlist.name}</th>
                    <th>
                        {
                            playlist.collaborative ? (
                                <Badge color="success">Collaborative</Badge>
                            ) : (
                                <Badge color="danger">Non Collaborative</Badge>
                            )
                        }
                    </th>
                    <th>
                        <Button
                            href={playlist.uri}
                            size="sm"
                            color="default">
                            <img src={require('../../assets/img/icons/common/spotify.svg')}
                                 height="20"
                                 width="20"
                                 className="mr-2"
                                 alt={playlist.name}/>
                            Ouvrir sur Spotify
                        </Button>
                    </th>
                </tr>
            );
        });
    }

    /**
     * Render component
     * @returns {*}
     */
    render() {
        return (
            <Col className="mb-5" md={this.props.size}>
                <Card className="shadow">
                    <CardHeader className="border-0">
                        <Row className="align-items-center">
                            <div className="col">
                                <h3 className="mb-0">{this.props.title}</h3>
                            </div>
                            <div className="col text-right">
                                <Button
                                    color="dark"
                                    onClick={(e) => this.setState({editModal: !this.state.editModal})}
                                    size="sm">
                                    Modifier
                                </Button>
                                <Button
                                    color="danger"
                                    onClick={(e) => {
                                        e.preventDefault();
                                        request.get('/api/user/deleteWidget/' + this.props.uuid).then(res => {
                                            window.location.reload();
                                        })
                                    }}
                                    size="sm">
                                    &times;
                                </Button>
                            </div>
                        </Row>
                    </CardHeader>
                    <Table className="align-items-center table-flush" responsive>
                        <tbody>
                        {this.displayContent()}
                        </tbody>
                    </Table>
                </Card>
                <WidgetEditModal {...this.props}
                                 editModal={this.state.editModal}
                                 onToggle={() => this.setState({editModal: !this.state.editModal})} />
            </Col>
        );
    }
}

export default SpotifyGetPlaylists;