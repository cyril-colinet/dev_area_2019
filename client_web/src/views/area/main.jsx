import React from "react";
import {Container, Row, Card, CardHeader, Button, UncontrolledAlert, Form} from "reactstrap";

import Header from "../../components/Headers/Header.jsx";
import NoActionsReactionsFound from "../widgets/defaults/noActionsReactionsFound";
import BaseRequest from "../../utils/request";
import serviceTheme from "../../utils/serviceTheme";
import {Redirect} from "react-router-dom";

/**
 * Main from admin layout
 */
export default class Main extends React.Component {

    /**
     * Contructor
     * @param props
     */
    constructor(props) {
        super(props);

        // Configure requests headers
        this.headers = {
            'Authorization': 'Bearer ' + localStorage.getItem("token")
        };

        // Configure state
        this.state = {
            services: [],
            redirection: false,
            error: null,
            success: false,
        };
    }

    /**
     * Before render of component
     */
    componentWillMount() {
        // Get repositories informations
        BaseRequest.get("/api/account/areas", {headers: this.headers}).then(res => {
            let data = res.data;
            if (data == null)
                return;
            // Change state
            this.setState({services: data.data});
        });
    }

    /**
     * Delete AREA form
     */
    deleteArea(e, id) {
        e.preventDefault();

        // Send request to
        BaseRequest.delete("/api/area/" + id, {headers: this.headers}).then(res => {
            let data = res.data;

            // If no errors occured, redirect
            if (data.result === "success") {
                this.setState({
                    error: null,
                    success: true,
                });

                // Redirect after time
                setTimeout(() => this.setState({ redirection: true }), 2000);
                return;
            }

            // Display error
            this.setState({error: "Error occured: " + data.message});
        }).catch(err => this.setState({error: err.message}));
    }

    /**
     * Display all widgets in this page
     * @returns {*[]}
     */
    displayAllAreas() {
        if (this.state.services.length === 0)
            return (<NoActionsReactionsFound />);

        console.log(this.state.services);
        return this.state.services.map(service => {
            console.log(service);
            let actionColor = serviceTheme[service.action.belongs_to_service].backgroundColor;
            let reactionColor = serviceTheme[service.reaction.belongs_to_service].backgroundColor;

            return (
                    <div key={service.id} className="mt-2 mb-3">
                        <Row>
                            <Card className="shadow border-rounded m-2" style={{display: "inline-block", flex: "0 0 auto", float: "none", width: "300px", color: "white", backgroundColor: actionColor, opacity: "65%"}}>
                                <CardHeader>
                                    <Row>
                                        <h3 className="text-left ml-2">{serviceTheme[service.action.belongs_to_service].icon}&nbsp;{service.action.belongs_to_service.charAt(0).toUpperCase() + service.action.belongs_to_service.slice(1)}</h3>
                                    </Row>
                                    <h3 className="text-right">Action</h3>
                                </CardHeader>
                                <Container className="ml-1" style={{whiteSpace: "pre-line"}}>
                                    <p>
                                        <small style={{fontWeight: "bold"}}>Description:</small>
                                    </p>
                                    <p className="mt--3">
                                        <small>{service.action.description}</small>
                                    </p>
                                </Container>
                                <Container className="ml-1" style={{whiteSpace: "pre-line"}}>
                                    <p>
                                        <small style={{fontWeight: "bold"}}>Config:</small>
                                    </p>
                                    {Object.keys(service.action.config).map(config => {
                                        return (
                                                <p className="mt--3">
                                                    <small>{config}: {service.action.config[config]}</small>
                                                </p>
                                        )
                                    })}
                                </Container>
                            </Card>
                            <Card className="shadow border-rounded m-2" style={{display: "inline-block", flex: "0 0 auto", float: "none", width: "300px", color: "white", backgroundColor: reactionColor, opacity: "65%"}}>
                                <CardHeader>
                                    <Row>
                                        <h3 className="text-left ml-2">{serviceTheme[service.reaction.belongs_to_service].icon}&nbsp;{service.reaction.belongs_to_service.charAt(0).toUpperCase() + service.reaction.belongs_to_service.slice(1)}</h3>
                                    </Row>
                                    <h3 className="text-right">Reaction</h3>
                                </CardHeader>
                                <Container className="ml-1" style={{whiteSpace: "pre-line"}}>
                                    <p>
                                        <small style={{fontWeight: "bold"}}>Description:</small>
                                    </p>
                                    <p className="mt--3">
                                        <small>{service.reaction.description}</small>
                                    </p>
                                </Container>
                                <Container className="ml-1" style={{whiteSpace: "pre-line"}}>
                                    <p>
                                        <small style={{fontWeight: "bold"}}>Config:</small>
                                    </p>
                                    {Object.keys(service.reaction.config).map(config => {
                                        return (
                                                <p className="mt--3">
                                                    <small><span style={{fontWeight: "bold"}}>{config}:</span> {service.reaction.config[config]}</small>
                                                </p>
                                        )
                                    })}
                                </Container>
                            </Card>
                        </Row>
                        <Button className="btn"
                                style={{Width:"60%"}}
                                color="danger"
                                type="text"
                                onClick={e => this.deleteArea(e, service.id)}>
                            Delete Area
                        </Button>
                    </div>
            )
        });
    }

    /**
     * Render component
     */
    render() {
        if (this.state.redirection)
            return (<Redirect to="/"/>);
        return (
                <>
                    <Header />
                    <Container className="mt-7" fluid>
                        {
                            // Display error
                            this.state.error != null ? (<Button className="btn-block text-left" disabled={true} color="danger" style={{whiteSpace: "pre-line"}}>{this.state.error}</Button>) : ""
                        }
                        {
                            // Area deleted
                            this.state.success ?
                                    (
                                            <UncontrolledAlert color="success">
                                                The area has been deleted of your list of areas !<br />
                                                Page reload in 2 seconds...
                                            </UncontrolledAlert>
                                    ) : ""
                        }
                        {this.displayAllAreas()}
                    </Container>
                </>
        );
    }
}