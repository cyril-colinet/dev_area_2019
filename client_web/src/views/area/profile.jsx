import React from "react";

import {Card, CardHeader, CardBody, FormGroup, Form, Input, Container, Row, Col} from "reactstrap";
import UserHeader from "../../components/Headers/UserHeader.jsx";
import Loader from "react-loader-spinner";
import BaseRequest from "../../utils/request";
import serviceTheme from '../../utils/serviceTheme';

/**
 * Profile component class
 * Ectends to React.Component
 */
class Profile extends React.Component {

    windowHandle;   // reference to the window object we will create
    intervalId = null;  // For setting interval time between we check for authorization code or token
    loopCount = 600;   // the count until which the check will be done, or after window be closed automatically.
    intervalLength = 100;   // the gap in which the check will be done for code.

    /**
     * Constructor
     * @param props
     */
    constructor(props) {
        super(props);

        // Configure requests headers
        this.headers = {
            'Authorization': 'Bearer ' + localStorage.getItem("token")
        };

        // Configure state
        this.state = {
            userInfo: [],
            service: [],
        };

        // Bind function
        this.oauthNewWindow = this.oauthNewWindow.bind(this);
    }

    doAuthorization(url, isRegisterAction) {
        let loopCount = this.loopCount;

        // Disable security

        this.windowHandle = this.oauthNewWindow(url, 'OAuth login');
        this.intervalId = window.setInterval(() => {
            if (loopCount-- < 0) {
                window.clearInterval(this.intervalId);
                this.windowHandle.close();
            } else {
                this.windowHandle.addEventListener('loadstart', function(event) {
                    console.log('in loadstart ', event.url);
                });

                /*let href;
                try {
                    href = this.windowHandle.location.href;
                } catch (e) {
                    // Origin control access..
                    console.log('Error:', e);
                }

                if (href != null) {
                    const getQueryString = function(field, url) {
                        const windowLocationUrl = url ? url : href;
                        const reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
                        const string = reg.exec(windowLocationUrl);
                        return string ? string[1] : null;
                    };

                    if (href.match('code')) {
                        window.clearInterval(this.intervalId);
                        this.authorizationCode = getQueryString('code', href);
                        this.windowHandle.close();

                        if (isRegisterAction) {

                        } else {

                        }
                    } else if (href.match('oauth_token')) { // Twitter implementation
                        window.clearInterval(this.intervalId);
                        this.oAuthToken = getQueryString('oauth_token', href);
                        this.oAuthVerifier = getQueryString('oauth_verifier', href);
                        this.windowHandle.close();

                        if (isRegisterAction) {

                        } else {

                        }
                    }
                }*/
            }
        }, this.intervalLength);
    }

    oauthNewWindow(url, title) {
        return window.open(url, "Connect to " + title, "location=0,status=0,width=800,height=400");
    }

    /**
     * When component did mount
     */
    componentDidMount() {
        BaseRequest.get("/api/account/info", {headers: this.headers}).then(res => {
            let data = res.data;
            if (data == null)
                return;

            // Change state
            this.setState({userInfo: data});
        });
        BaseRequest.get("/api/services/user", {headers: this.headers}).then(res => {
            let data = res.data;
            if (data == null)
                return;

            // Change state
            this.setState({service: data.data});
        });
    }

    /**
     * Display all services linked or not in this page
     * @returns {*[]}
     */
    displayAllServices() {
        return this.state.service.map(service => {
            if (!service.need_oauth)
                return null;

            let color = serviceTheme[service.service_name].backgroundColor;
            if (!service.is_oauth) {
                return (
                    <Card className="shadow border-rounded m-2" key={service.service_name} style={{color: "white", backgroundColor: color, opacity: "65%"}}>
                        <CardBody className="m--2">
                            <Row>
                                <div className="col-8">
                                    <h4 className="text-left" style={{color: "white"}}>
                                        {serviceTheme[service.service_name].iconLight}
                                        &nbsp;&nbsp;
                                        {service.service_name.charAt(0).toUpperCase() + service.service_name.slice(1)}
                                    </h4>
                                </div>
                                <div className="col-4">
                                    <button style={{height: "30px"}}
                                            className="btn-sm btn-primary mt--1 ml-2 h6"
                                            onClick={() => {
                                                BaseRequest.post("/api/services/subscribe", {service_id: service.service_id}, {headers: this.headers})
                                                    .then(res => {
                                                        let data = res.data;
                                                        if (data == null || data.result !== "ok")
                                                            return;

                                                        // Create authorization
                                                        this.doAuthorization(data.link_oauth, false);
                                                    });
                                            }}>
                                        Subscribe
                                    </button>
                                </div>
                            </Row>
                        </CardBody>
                    </Card>
                )
            } else {
                return (
                    <Card className="shadow border-rounded m-2" key={service.service_name} style={{color: "white", backgroundColor: color, opacity: "65%"}}>
                        <CardBody>
                            <Row>
                                <div className="col-8">
                                    <h4 className="text-left" style={{color: "white"}}>{serviceTheme[service.service_name].iconLight}&nbsp;{service.service_name.charAt(0).toUpperCase() + service.service_name.slice(1)}</h4>
                                </div>
                                <div className="col-4">
                                    <button style={{height: "30px"}} className="btn-sm btn-success mt--1 ml-2 h6" >
                                        Linked
                                    </button>
                                </div>
                            </Row>
                        </CardBody>
                    </Card>
                )
            }
        });
    }


    /**
     * Render component
     */
    render() {
        console.log(this.state.service);
        // If user info is correctly set
        if (Object.keys(this.state.userInfo).length === 0)
            return <Loader type="MutatingDots" color="#000000" height={200} width={200} />;

        // Return rendering of default page with data
        return (
            <>
                <UserHeader />
                {/* Page content */}
                <Container className="mt--5" fluid>
                    <Row>
                        <Col className="order-xl-2 mb-5 mb-xl-0" xl="4">
                            <Card className="card-profile shadow">
                                <Row className="justify-content-center">
                                    <Col className="order-lg-2" lg="3">
                                        <div className="card-profile-image">
                                            <a href="#pablo" onClick={e => e.preventDefault()}>
                                                <img
                                                    alt="..."
                                                    className="rounded-circle"
                                                    src={require("../../assets/img/theme/team-3-800x800.jpg")}
                                                />
                                            </a>
                                        </div>
                                    </Col>
                                </Row>
                                <CardHeader className="text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
                                </CardHeader>
                                <CardBody className="pt-0 pt-md-4 mt-6">
                                    {this.displayAllServices()}
                                </CardBody>
                            </Card>
                        </Col>
                        <Col className="order-xl-1" xl="8">
                            <Card className="bg-secondary shadow">
                                <CardHeader className="bg-white border-0">
                                    <Row className="align-items-center">
                                        <Col xs="8">
                                            <h3 className="mb-0">Account information</h3>
                                        </Col>
                                    </Row>
                                </CardHeader>
                                <CardBody>
                                    <h6 className="heading-small text-muted mb-4">
                                        User information
                                    </h6>
                                    <div className="pl-lg-4">
                                        <Row>
                                            <Col lg="6">
                                                <FormGroup>
                                                    <label
                                                        className="form-control-label"
                                                        htmlFor="input-username">
                                                        Username
                                                    </label>
                                                    <Input
                                                        className="form-control-alternative"
                                                        id="input-username"
                                                        name="username"
                                                        placeholder="Please enter your username"
                                                        defaultValue={localStorage.getItem("email").toString().split('@')[0]}
                                                        type="text"
                                                        disabled={true}
                                                    />
                                                </FormGroup>
                                            </Col>
                                            <Col lg="6">
                                                <FormGroup>
                                                    <label
                                                        className="form-control-label"
                                                        htmlFor="input-email">
                                                        Email address
                                                    </label>
                                                    <Input
                                                        className="form-control-alternative"
                                                        id="input-email"
                                                        placeholder="Please enter your email address"
                                                        type="email"
                                                        name="email"
                                                        defaultValue={localStorage.getItem("email")}
                                                        disabled={true}
                                                    />
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                    </div>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </>
        );
    }
}

export default Profile;
