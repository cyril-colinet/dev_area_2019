import * as React from "react";

import {Redirect} from "react-router-dom";
import Header from "../../components/Headers/Header";
import {
    Button,
    Col,
    Container, Form,
    FormGroup, Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText, Modal,
    Row,
    UncontrolledAlert
} from "reactstrap";
import BaseRequest from "../../utils/request";
import serviceTheme from '../../utils/serviceTheme';

export default class ConfigureAREA extends React.Component {

    /**
     if (this.state.formDataObj.
     * Constructor component
     * @param props
     */
    constructor(props) {
        super(props);

        // Configure requests headers
        this.headers = {
            'Authorization': 'Bearer ' + localStorage.getItem("token")
        };

        // Configure state
        this.state = {
            services: [],
            actions: [],
            reactions: [],
            error: null,

            actionService: null,
            actionName: null,
            actionId: null,
            actionConfig: null,
            actionValid: false,
            actionError: null,

            reactionService: null,
            reactionName: null,
            reactionId: null,
            reactionConfig: null,
            reactionValid: false,
            reactionError: null,

            areaCreated: false,
            redirection: false,
        };

        // Bind method
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    /**
     * Execute requests and fill state
     * Get all services action/reaction from the api and configure it
     */
    componentDidMount() {
        BaseRequest.get("/api/services/user", {headers: this.headers}).then(res => {
            let data = res.data;
            if (data == null)
                return;

            // Change state
            this.setState({services: data.data});
        });
        BaseRequest.get("/api/actions/available", {headers: this.headers}).then(res => {
            let data = res.data;
            if (data == null)
                return;

            // Change state
            this.setState({actions: data});
        });
        BaseRequest.get("/api/reactions/available", {headers: this.headers}).then(res => {
            let data = res.data;
            if (data == null)
                return;

            // Change state
            this.setState({reactions: data});
        });
    }

    /**
     * Reset action params
     */
    resetActionParams() {
        this.setState({actionValid: false});
        this.setState({actionName: null});
        this.setState({actionId: null});
        this.setState({actionConfig: null});
    }

    /**
     * Reset reaction params
     */
    resetReactionParams() {
        this.setState({reactionValid: false});
        this.setState({reactionName: null});
        this.setState({reactionId: null});
        this.setState({reactionConfig: null});
    }


    /**
     * Select a service for action
     */
    selectActionService(e, service) {
        this.setState({actionService: service.service_name});
        this.setState({actionError: "Select the type of area"});
        this.resetActionParams();
    }

    /**
     * Select a service for reaction
     */
    selectReactionService(e, service) {
        this.setState({reactionService: service.service_name});
        this.setState({reactionError: "Select the type of area"});
        this.resetReactionParams();
    }

    /**
     * Select an action
     */
    selectAction(e, area) {
        this.setState({actionValid: false});
        this.setState({actionName: area.name});
        this.setState({actionId: area.id});
        this.setState({actionConfig: area.required_config});
        this.setState({actionError: null});

        if (area.need_oauth && !area.is_oauth) {
            this.setState({actionError: "You need to subscribe to the service: '" + this.state.actionService + "', take a look on your profile"});
            return;
        }
        this.setState({actionValid: true});
        if (Object.keys(area.required_config).length !== 0)
            this.setState({actionError: "You need to configure the action"})
    }

    /**
     * Select a reaction
     */
    selectReaction(e, area) {
        this.setState({reactionValid: false});
        this.setState({reactionName: area.name});
        this.setState({reactionId: area.id});
        this.setState({reactionConfig: area.required_config});
        this.setState({reactionError: null});

        if (area.need_oauth && !area.is_oauth) {
            this.setState({reactionError: "You need to subscribe to the service: '" + this.state.reactionService + "', take a look on your profile"});
            return;
        }
        this.setState({reactionValid: true});
        if (Object.keys(area.required_config).length !== 0)
            this.setState({reactionError: "You need to configure the reaction"})
    }

    /**
     * Display all services with their action
     */
    displayAllActionServices() {
        return this.state.services.map(service => {
            let color = serviceTheme[service.service_name].backgroundColor;
            let opacity = "65%";

            if (this.state.actionService === service.service_name)
                opacity = "100%";

            return (
                    <div key={service.service_name} className="col-xl-4 col-md-6 m-1">
                        <button className="btn" type='button' style={{color: "white", backgroundColor: color, opacity: opacity}} onClick={(e) => this.selectActionService(e, service)}>
                            {serviceTheme[service.service_name].iconLight}&nbsp;{service.service_name.charAt(0).toUpperCase() + service.service_name.slice(1)}
                        </button>
                    </div>
            )
        })
    }

    /**
     * Display all services with their reaction
     */
    displayAllReactionServices() {
        return this.state.services.map(service => {
            let color = serviceTheme[service.service_name].backgroundColor;
            let opacity = "65%";

            if (this.state.reactionService === service.service_name)
                opacity = "100%";

            return (
                    <div key={service.service_name} className="col-xl-4 col-md-6 m-1">
                        <button className="btn" type='button' style={{color: "white", backgroundColor: color, opacity: opacity}} onClick={(e) => this.selectReactionService(e, service)}>
                            {serviceTheme[service.service_name].iconLight}&nbsp;{service.service_name.charAt(0).toUpperCase() + service.service_name.slice(1)}
                        </button>
                    </div>
            )
        })
    }

    /**
     * Display action
     */
    displayAction() {
        if (!this.state.actionService)
            return;

        let areas = [];

        for (let i = 0; i < this.state.actions.length; i++) {
            if (this.state.actions[i].service_name === this.state.actionService)
                areas = this.state.actions[i].actions;
        }

        if (!areas || !areas[0])
            return;

        return areas.map(area => {
            let color = serviceTheme[this.state.actionService].backgroundColor;
            let opacity = "65%";

            if (this.state.actionName === area.name)
                opacity = "100%";

            return (
                    <div key={area.name} className="col-12 m-2">
                        <button className="btn" type='button' style={{color: "white", backgroundColor: color, opacity: opacity}} onClick={(e) => this.selectAction(e, area)}>
                            {area.name}
                        </button>
                    </div>
            )
        })
    }

    /**
     * Display reaction
     */
    displayReaction() {
        if (!this.state.reactionService)
            return;

        let areas = [];

        for (let i = 0; i < this.state.reactions.length; i++) {
            if (this.state.reactions[i].service_name === this.state.reactionService)
                areas = this.state.reactions[i].reactions;
        }

        if (!areas || !areas[0])
            return;

        return areas.map(area => {
            let color = serviceTheme[this.state.reactionService].backgroundColor;
            let opacity = "65%";

            if (this.state.reactionName === area.name)
                opacity = "100%";

            return (
                    <div key={area.name} className="col-12 m-2">
                        <button className="btn" type='button' style={{color: "white", backgroundColor: color, opacity: opacity}} onClick={(e) => this.selectReaction(e, area)}>
                            {area.name}
                        </button>
                    </div>
            )
        })
    }

    /**
     * Display action form for configuration
     */
    displayActionForm() {
        if (!this.state.actionService || !this.state.actionName || Object.keys(this.state.actionConfig).length === 0)
            return;

        return Object.keys(this.state.actionConfig).map(config => {
            return (
                    <FormGroup className="mb-3" key={config}>
                        <InputGroup className="input-group-alternative">
                            <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                    <h4>{config} :</h4>
                                </InputGroupText>
                            </InputGroupAddon>
                            <Input id={config} placeholder={this.state.actionConfig[config]} type="text" name={config} />
                        </InputGroup>
                    </FormGroup>
            )
        })
    }

    /**
     * Display reaction form for configuration
     */
    displayReactionForm() {
        if (!this.state.reactionService || !this.state.reactionName || Object.keys(this.state.reactionConfig).length === 0)
            return;

        return  Object.keys(this.state.reactionConfig).map(config => {
            return (
                    <FormGroup className="mb-3" key={config}>
                        <InputGroup className="input-group-alternative">
                            <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                    <h4>{config} :</h4>
                                </InputGroupText>
                            </InputGroupAddon>
                            <Input id={config} placeholder={this.state.reactionConfig[config]}  type="text" name={config} />
                        </InputGroup>
                    </FormGroup>
            )
        })
    }

    /**
     * Submit new area
     */
    handleSubmit(e) {
        e.preventDefault();
        let formData = {};
        let actionData = {};
        let reactionData = {};

        Object.keys(this.state.actionConfig).map(config => {
            actionData[config] =  document.getElementById(config).value;
        });
        Object.keys(this.state.reactionConfig).map(config => {
            reactionData[config] =  document.getElementById(config).value;
        });

        formData = {
            action_id: this.state.actionId,
            action_config: JSON.stringify(actionData),
            reaction_id: this.state.reactionId,
            reaction_config: JSON.stringify(reactionData),
        };

        // Send request to
        BaseRequest.post("/api/area", formData, {headers: this.headers}).then(res => {
            let data = res.data;

            // If no errors occured, redirect
            if (data.result === "success") {
                this.setState({
                    error: null,
                    areaCreated: true,
                });

                // Redirect after time
                setTimeout(() => this.setState({ redirection: true }), 2000);
                return;
            }

            // Display error
            this.setState({error: "Error occured: " + data.message});
        }).catch(err => this.setState({error: err.message}));
    }

    /**
     * Render the configuration page
     */
    render() {
        if (this.state.redirection)
            return (<Redirect to="/"/>);

        return (
                <>
                    <Header />
                    <Container className="mt--5" fluid>
                        <Form className="mt-7" role="form" onSubmit={this.handleSubmit}>
                            <Row>
                                <div className="col-6">
                                    <h2>Service</h2>
                                    <hr />
                                    <Row>
                                        {this.displayAllActionServices()}
                                    </Row>
                                    <hr />
                                    <div className="m-2">
                                        <button className="btn btn-primary" type='button' style={{color: "white"}} disabled={true}>
                                            Action
                                        </button>
                                    </div>
                                    <hr />
                                    {
                                        this.state.actionService ?
                                                (
                                                        <div>
                                                            <hr />
                                                            <h2>
                                                                {
                                                                    this.state.actionService.charAt(0).toUpperCase() + this.state.actionService.slice(1) +
                                                                    " Action"
                                                                }
                                                            </h2>
                                                            <hr />
                                                            <Row>
                                                                {this.displayAction()}
                                                            </Row>
                                                        </div>
                                                ) : ""
                                    }
                                    {
                                        this.state.actionService && this.state.actionName && Object.keys(this.state.actionConfig).length !== 0 ?
                                                (
                                                        <div>
                                                            <hr />
                                                            <h2>Configuration Form</h2>
                                                            <hr />
                                                            {this.displayActionForm()}
                                                        </div>
                                                ) : ""
                                    }
                                    <hr />
                                    {
                                        // Display error
                                        this.state.actionError != null ? (<Button className="btn-block text-left" disabled={true} color="danger" style={{whiteSpace: "pre-line"}}>{this.state.actionError}</Button>) : ""
                                    }
                                </div>
                                <div className="col-6">
                                    <h2>Service</h2>
                                    <hr />
                                    <Row>
                                        {this.displayAllReactionServices()}
                                    </Row>
                                    <hr />
                                    <div className="m-2">
                                        <button className="btn btn-primary" type='button' style={{color: "white"}} disabled={true}>
                                            Reaction
                                        </button>
                                    </div>
                                    <hr />
                                    {
                                        this.state.reactionService ?
                                                (
                                                        <div>
                                                            <hr />
                                                            <h2>
                                                                {
                                                                    this.state.reactionService.charAt(0).toUpperCase() + this.state.reactionService.slice(1) +
                                                                    " Action"
                                                                }
                                                            </h2>
                                                            <hr />
                                                            <Row>
                                                                {this.displayReaction()}
                                                            </Row>
                                                        </div>
                                                ) : ""
                                    }
                                    {
                                        this.state.reactionService && this.state.reactionName && Object.keys(this.state.reactionConfig).length !== 0 ?
                                                (
                                                        <div>
                                                            <hr />
                                                            <h2>Configuration Form</h2>
                                                            <hr />
                                                            {this.displayReactionForm()}
                                                        </div>
                                                ) : ""
                                    }
                                    <hr />
                                    {
                                        // Display error
                                        this.state.reactionError != null ? (<Button className="btn-block text-left" disabled={true} color="danger" style={{whiteSpace: "pre-line"}}>{this.state.reactionError}</Button>) : ""
                                    }
                                </div>
                            </Row>
                            {
                                // Area created
                                this.state.areaCreated ?
                                        (
                                                <UncontrolledAlert color="success">
                                                    The area has been added to your list of areas !<br />
                                                    Redirection in 2 seconds...
                                                </UncontrolledAlert>
                                        ) : ""
                            }
                            {
                                // Display error
                                this.state.error != null ? (<Button className="btn-block text-left mt-3" disabled={true} color="danger" style={{whiteSpace: "pre-line"}}>{this.state.error}</Button>) : ""
                            }
                            <div className="mt-5 pb-5">
                                <Col className="offset-4" md="4">
                                    {this.state.actionValid && this.state.reactionValid ? (
                                            <Button className="btn-block"
                                                    style={{Width:"60%"}}
                                                    color="success"
                                                    type="submit">
                                                Add Area
                                            </Button>
                                    ) : (
                                            <Button className="btn-block"
                                                    style={{Width:"60%"}}
                                                    color="success"
                                                    type="submit"
                                                    disabled={true}>
                                                Add Area
                                            </Button>
                                    )}
                                </Col>
                            </div>
                        </Form>
                    </Container>
                </>
        );
    }
}