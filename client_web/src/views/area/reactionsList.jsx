import React from "react";
import {Container, Row, Card, CardHeader} from "reactstrap";

import Header from "../../components/Headers/Header.jsx";
import BaseRequest from '../../utils/request';
import serviceTheme from '../../utils/serviceTheme';
import NoActionsReactionsFound from "../widgets/defaults/noActionsReactionsFound";

class ReactionsList extends React.Component {

    /**
     * Component constructor with state generation
     * @param props
     */
    constructor(props) {
        super(props);

        // Configure requests headers
        this.headers = {
            'Authorization': 'Bearer ' + localStorage.getItem("token")
        };

        // Configure state
        this.state = {
            services: [],
        };
    }

    /**
     * Before render of component
     */
    componentWillMount() {
        // Get repositories informations
        BaseRequest.get("/api/reactions/available", {headers: this.headers}).then(res => {
            let data = res.data;
//            if (data == null || data.reactions == null)
            if (data == null)
                return;

            // Change state
            this.setState({services: data});
        });
    }

    /**
     * Display all reactions in this page
     * @returns {*[]}
     */
    displayAllReactions() {

        if (!this.state.services) {
            return (<NoActionsReactionsFound/>);
        }

        return this.state.services.map(service => {
            if (!service.reactions[0])
                return null;
            return (
                <div className="col-12 mt-3 p-4" style={{backgroundColor: "white"}} key={service.service_name}>
                    <Row style={{overflowX: "auto", flexWrap: "nowrap", whiteSpace: "nowrap"}}>
                        {service.reactions.map(reaction => {
                            let color = serviceTheme[service.service_name].backgroundColor;
                            return (
                                <Card className="shadow border-rounded m-2" style={{display: "inline-block", flex: "0 0 auto", float: "none", width: "300px", color: "white", backgroundColor: color, opacity: "65%"}} key={reaction.name}>
                                    <CardHeader>
                                        <Row>
                                            <h3 className="text-left ml-2">{serviceTheme[service.service_name].icon}&nbsp;{service.service_name.charAt(0).toUpperCase() + service.service_name.slice(1)}</h3>
                                        </Row>
                                        <h3 className="text-right">Reaction</h3>
                                    </CardHeader>
                                    <Container className="ml-1" style={{whiteSpace: "pre-line"}}>
                                        <p>
                                            <small style={{fontWeight: "bold"}}>Description:</small>
                                        </p>
                                        <p className="mt--3">
                                            <small>{reaction.description}</small>
                                        </p>
                                    </Container>
                                </Card>
                            )
                        })}
                    </Row>
                </div>
            )
        });
    }

    /**
     * Render component
     */
    render() {
        return (
            <>
                <Header />
                <Container className="mt-4 mb-5" fluid>
                    {this.displayAllReactions()}
                </Container>
            </>
        )
    }
}


export default ReactionsList