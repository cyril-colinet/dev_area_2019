import React from "react";

import {Redirect} from "react-router-dom";
import BaseRequest from '../../utils/request';
import {
    Button, Card, CardHeader, CardBody, FormGroup, Input,
    InputGroupAddon, InputGroupText, InputGroup, Row, Col, UncontrolledAlert
} from "reactstrap";

/**
 * Login component
 */
class Login extends React.Component {

    /**
     * Constructor
     * @param props
     */
    constructor(props) {
        super(props);

        // Configure state
        this.state = {
            urls: {},
            error: null,
            success: null,
            redirection: false
        };

        // Add message from current url
        let params = (new URL(window.document.location)).searchParams;
        if (params.get("error") != null) {
            this.state.error = params.get("error");
        } else if (params.get("result") != null) {
            this.state.error = params.get("result");
        }

        // Bind method
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    /**
     * Handle submit form and perform request method
     */
    handleSubmit(event) {
        event.preventDefault();
        let then = this;
        let formData = new FormData(event.target);

        // Send request to
        BaseRequest.post("/api/login", Object.fromEntries(formData), {
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => {
            let data = res.data;

            // If no errors occured, redirect
            if (data.result === "success") {
                localStorage.setItem("token", data.token);
                localStorage.setItem("email", formData.get("email").toString());
                then.setState({
                    error: null,
                    success: "Successfully logged in! You will be redirected in 3 seconds..."
                });

                // Redirect after time
                setTimeout(() => this.setState({ redirection: true }), 3000);
                return;
            }

            // Display error
            this.setState({error: "Error occured: " + data.message});
        }).catch(err => then.setState({error: err.message}));
    }

    /**
     * Render page
     * @returns {*}
     */
    render() {
        // Able to redirect
        if (this.state.redirection)
            return (<Redirect from="/" to="/area/index"/>);

        // Default render
        return (
            <>
                <Col lg="6" md="8">
                    <Card className="bg-secondary shadow border-0">
                        <CardHeader className="bg-transparent pb-5">
                            {
                                // Display error
                                this.state.error != null ?
                                    (
                                        <UncontrolledAlert color="danger">{this.state.error}</UncontrolledAlert>
                                    ) : ""
                            }
                            {
                                // Display success
                                this.state.success != null ?
                                    (
                                        <UncontrolledAlert color="success">{this.state.success}</UncontrolledAlert>
                                    ) : ""
                            }
                            <div className="text-muted text-center mt-2 mb-3">
                                <small>Login with</small>
                            </div>
                            <div className="btn-wrapper text-center">
                                <Button
                                    className="btn-neutral btn-icon"
                                    color="default"
                                    href={this.state.urls['github']}>
                                    <span className="btn-inner--icon">
                                        <img
                                            alt="..."
                                            src={require("../../assets/img/icons/common/github.svg")}
                                        />
                                    </span>
                                </Button>
                                <Button
                                    className="btn-neutral btn-icon"
                                    color="default"
                                    href={this.state.urls['google']}>
                                    <span className="btn-inner--icon">
                                        <img
                                            alt="..."
                                            src={require("../../assets/img/icons/common/google.svg")}
                                        />
                                    </span>
                                </Button>
                                <Button
                                    className="btn-neutral btn-icon"
                                    color="default"
                                    href={this.state.urls['facebook']}>
                                    <span className="btn-inner--icon">
                                        <img
                                            alt="..."
                                            src={require("../../assets/img/icons/common/facebook.svg")}
                                        />
                                    </span>
                                </Button>
                                <Button
                                    className="btn-neutral btn-icon"
                                    color="default"
                                    href={this.state.urls['spotify']}>
                                    <span className="btn-inner--icon">
                                        <img
                                            alt="..."
                                            src={require("../../assets/img/icons/common/spotify.svg")}
                                        />
                                    </span>
                                </Button>
                                <Button
                                    className="btn-neutral btn-icon"
                                    color="default"
                                    href={this.state.urls['twitter']}>
                                    <span className="btn-inner--icon">
                                        <img
                                            alt="..."
                                            src={require("../../assets/img/icons/common/twitter.svg")}
                                        />
                                    </span>
                                </Button>
                            </div>
                        </CardHeader>
                        <CardBody className="px-lg-5 py-lg-5">
                            <div className="text-center text-muted mb-4">
                                <small>Or login with your credentials</small>
                            </div>
                            <form onSubmit={this.handleSubmit}>
                                <FormGroup className="mb-3">
                                    <InputGroup className="input-group-alternative">
                                        <InputGroupAddon addonType="prepend">
                                            <InputGroupText>
                                                <i className="ni ni-email-83" />
                                            </InputGroupText>
                                        </InputGroupAddon>
                                        <Input placeholder="Email" type="email" name="email" />
                                    </InputGroup>
                                </FormGroup>
                                <FormGroup>
                                    <InputGroup className="input-group-alternative">
                                        <InputGroupAddon addonType="prepend">
                                            <InputGroupText>
                                                <i className="ni ni-lock-circle-open" />
                                            </InputGroupText>
                                        </InputGroupAddon>
                                        <Input placeholder="Password" type="password" name="password" />
                                    </InputGroup>
                                </FormGroup>
                                <div className="text-center mt-3">
                                    <Button className="btn-block my-4" color="primary" type="submit">
                                        Login
                                    </Button>
                                </div>
                            </form>
                            <Row className="mt-5">
                                <Col className="text-left">
                                    <a
                                        className="btn"
                                        href="/auth/forgotPassword">
                                        Forgot password
                                    </a>
                                </Col>
                                <Col className="text-right">
                                    <a
                                        className="btn"
                                        href="/auth/register">
                                        Register
                                    </a>
                                </Col>
                            </Row>
                        </CardBody>
                    </Card>
                </Col>
            </>
        );
    }
}

export default Login;
