import React from "react";

import {Redirect} from "react-router-dom";
import BaseRequest from '../../utils/request';
import {
    Button, Card, CardHeader, CardBody, FormGroup, Input,
    InputGroupAddon, InputGroupText, InputGroup, Row, Col, UncontrolledAlert
} from "reactstrap";

/**
 * Forgot password component
 */
class ChangePassword extends React.Component {

    /**
     * Constructor
     * @param props
     */
    constructor(props) {
        super(props);

        // Configure state
        this.state = {
            urls: {},
            error: null,
            success: null,
            redirection: false,
            password: '',
            confirmPassword: ''
        };

        // Bind method
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    /**
     * Handle submit form and perform request method
     */
    handleSubmit(event) {
        event.preventDefault();
        let then = this;
        let formData = new FormData(event.target);

        // perform all neccassary validations
        if (this.state.password !== this.state.confirmPassword) {
            this.setState({error: "Error occured: Your password and confirmation password do not match"});
            return;
        }

        // Send request to
        BaseRequest.post("/api/changePassword", JSON.stringify(Object.fromEntries(formData)), {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }).then(res => {
            let data = res.data;

            // If no errors occured, redirect
            if (data.result === "success") {
                then.setState({
                    error: null,
                    success: "Password changed successfully ! You will be redirected in 3 seconds..."
                });

                // Redirect after time
                setTimeout(() => this.setState({ redirection: true }), 3000);
                return;
            }

            // Display error
            this.setState({error: "Error occured: " + data.message});
        }).catch(err => then.setState({error: err.message}));
    }

    /**
     * Render page
     * @returns {*}
     */
    render() {
        // Able to redirect
        if (this.state.redirection)
            return (<Redirect from="/" to="/area/index"/>);

        // Default render
        return (
            <>
                <Col lg="5" md="7">
                    <Card className="bg-secondary shadow border-0">
                        <CardHeader className="bg-transparent pb-5">
                            {
                                // Display error
                                this.state.error != null ?
                                    (
                                        <UncontrolledAlert color="danger">{this.state.error}</UncontrolledAlert>
                                    ) : ""
                            }
                            {
                                // Display success
                                this.state.success != null ?
                                    (
                                        <UncontrolledAlert color="success">{this.state.success}</UncontrolledAlert>
                                    ) : ""
                            }
                        </CardHeader>
                        <CardBody className="px-lg-5 py-lg-5">
                            <div className="text-center text-muted mb-4">
                                <small>Enter your new password</small>
                            </div>
                            <form onSubmit={this.handleSubmit}>
                                <FormGroup>
                                    <InputGroup className="input-group-alternative">
                                        <InputGroupAddon addonType="prepend">
                                            <InputGroupText>
                                                <i className="ni ni-lock-circle-open" />
                                            </InputGroupText>
                                        </InputGroupAddon>
                                        <Input placeholder="Password" type="password" name="password" />
                                    </InputGroup>
                                </FormGroup>
                                <FormGroup>
                                    <InputGroup className="input-group-alternative">
                                        <InputGroupAddon addonType="prepend">
                                            <InputGroupText>
                                                <i className="ni ni-lock-circle-open" />
                                            </InputGroupText>
                                        </InputGroupAddon>
                                        <Input placeholder="Confirm password" type="password" name="confirmPassword" />
                                    </InputGroup>
                                </FormGroup>
                                <div className="text-center mt-3">
                                    <Button className="btn-block my-4" color="primary" type="submit">
                                        Submit
                                    </Button>
                                </div>
                            </form>
                            <Row className="mt-5">
                                <Col className="text-right">
                                    <a
                                        className="btn"
                                        href="/auth/login">
                                        Login
                                    </a>
                                </Col>
                            </Row>
                        </CardBody>
                    </Card>
                </Col>
            </>
        );
    }
}

export default ChangePassword;