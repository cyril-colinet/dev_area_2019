import React from "react";
import BaseRequest from '../../utils/request';

import {Button, Card, CardBody, FormGroup, Row, Input, InputGroupAddon, InputGroupText,
    InputGroup, Col, UncontrolledAlert, CardHeader
} from "reactstrap";
import {Redirect} from "react-router-dom";

/**
 * Register class
 * Extendede to Auth layout
 */
class Register extends React.Component {

    /**
     * Constructor of the register page
     */
    constructor(props) {
        super(props);

        // Create state
        this.state = {
            redirection: false,
            error: null,
            success: null,
            password: '',
            confirmPassword: ''
        };

        // Bind this to function
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    /**
     * Handle submit form and perform request method
     */
    handleSubmit(event) {
        event.preventDefault();
        let then = this;
        let formData = new FormData(event.target);
        formData.append("redirect_uri", window.location.hostname + "/auth/login");

        // perform all neccassary validations
        if (this.state.password !== this.state.confirmPassword) {
            this.setState({error: "Error occured: Your password and confirmation password do not match"});
            return;
        }

        // Send request to
        BaseRequest.post("/api/register", Object.fromEntries(formData), {
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => {
            let data = res.data;

            // If no errors occured, redirect
            if (data.result === "success") {
                then.setState({
                    error: null,
                    success: "Acount successfully created! Go check your Email address to validate your account creation.."
                });

                // Redirect after time
                setTimeout(() => this.setState({ redirection: true }), 5000);
                return;
            }

            // Display error
            this.setState({error: "Error occured: " + data.message});
        }).catch(err => then.setState({error: err.message}));
    }

    /**
     * Render component
     */
    render() {
        // Able to redirect
        if (this.state.redirection) {
            return (<Redirect from="/" to="/auth/login"/>);
        }

        // Default render
        return (
            <>
                <Col lg="6" md="8">
                    <Card className="bg-secondary shadow border-0">
                        <CardHeader className="bg-transparent pb-5">
                            {
                                // Display error
                                this.state.error != null ?
                                    (
                                        <UncontrolledAlert color="danger">{this.state.error}</UncontrolledAlert>
                                    ) : ""
                            }
                            {
                                // Display success
                                this.state.success != null ?
                                    (
                                        <UncontrolledAlert color="success">{this.state.success}</UncontrolledAlert>
                                    ) : ""
                            }
                            <div className="text-muted text-center mt-2 mb-4">
                                <small>Register with these services</small>
                            </div>
                            <div className="text-center">
                                <Button
                                    className="btn-neutral btn-icon"
                                    color="default"
                                    href="#pablo"
                                    onClick={e => e.preventDefault()}>
                                    <span className="btn-inner--icon">
                                        <img
                                            alt="..."
                                            src={require("../../assets/img/icons/common/github.svg")}
                                        />
                                    </span>
                                </Button>
                                <Button
                                    className="btn-neutral btn-icon"
                                    color="default"
                                    href="#pablo"
                                    onClick={e => e.preventDefault()}>
                                    <span className="btn-inner--icon">
                                        <img
                                            alt="..."
                                            src={require("../../assets/img/icons/common/google.svg")}
                                        />
                                    </span>
                                </Button>
                                <Button
                                    className="btn-neutral btn-icon"
                                    color="default"
                                    href="#pablo"
                                    onClick={e => e.preventDefault()}>
                                    <span className="btn-inner--icon">
                                        <img
                                            alt="..."
                                            src={require("../../assets/img/icons/common/facebook.svg")}
                                        />
                                    </span>
                                </Button>
                                <Button
                                    className="btn-neutral btn-icon"
                                    color="default"
                                    href="#pablo"
                                    onClick={e => e.preventDefault()}>
                                    <span className="btn-inner--icon">
                                        <img
                                            alt="..."
                                            src={require("../../assets/img/icons/common/dropbox.svg")}
                                        />
                                    </span>
                                </Button>
                                <Button
                                    className="btn-neutral btn-icon"
                                    color="default"
                                    href="#pablo"
                                    onClick={e => e.preventDefault()}>
                                    <span className="btn-inner--icon">
                                        <img
                                            alt="..."
                                            src={require("../../assets/img/icons/common/twitter.svg")}
                                        />
                                    </span>
                                </Button>
                            </div>
                        </CardHeader>
                        <CardBody className="px-lg-5 py-lg-5">
                            <div className="text-center text-muted mb-4">
                                <small>Or create an account</small>
                            </div>
                            <form onSubmit={this.handleSubmit}>
                                <FormGroup>
                                    <InputGroup className="input-group-alternative mb-3">
                                        <InputGroupAddon addonType="prepend">
                                            <InputGroupText>
                                                <i className="ni ni-email-83" />
                                            </InputGroupText>
                                        </InputGroupAddon>
                                        <Input placeholder="Email" type="email" name="email" />
                                    </InputGroup>
                                </FormGroup>
                                <FormGroup>
                                    <InputGroup className="input-group-alternative">
                                        <InputGroupAddon addonType="prepend">
                                            <InputGroupText>
                                                <i className="ni ni-lock-circle-open" />
                                            </InputGroupText>
                                        </InputGroupAddon>
                                        <Input placeholder="Password" type="password" name="password" />
                                    </InputGroup>
                                </FormGroup>
                                <FormGroup>
                                    <InputGroup className="input-group-alternative">
                                        <InputGroupAddon addonType="prepend">
                                            <InputGroupText>
                                                <i className="ni ni-lock-circle-open" />
                                            </InputGroupText>
                                        </InputGroupAddon>
                                        <Input placeholder="Confirm password" type="password" name="confirmPassword" />
                                    </InputGroup>
                                </FormGroup>
                                <div className="text-center mt-3">
                                    <Button className="btn-block mt-4" color="primary" type="submit">
                                        Register
                                    </Button>
                                </div>
                            </form>
                            <Row className="mt-5">
                                <Col className="text-right">
                                    <a
                                        className="btn"
                                        href="/auth/login">
                                        Login
                                    </a>
                                </Col>
                            </Row>
                        </CardBody>
                    </Card>
                </Col>
            </>
        );
    }
}

export default Register;
