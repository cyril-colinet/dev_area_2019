import React from "react";

import {Redirect} from "react-router-dom";
import BaseRequest from '../../utils/request';
import {
    Button, Card, CardHeader, CardBody, FormGroup, Input,
    InputGroupAddon, InputGroupText, InputGroup, Row, Col, UncontrolledAlert
} from "reactstrap";

/**
 * Forgot password component
 */
class ForgotPassword extends React.Component {

    /**
     * Constructor
     * @param props
     */
    constructor(props) {
        super(props);

        // Configure state
        this.state = {
            urls: {},
            error: null,
            success: null,
            redirection: false,
        };

        // Bind method
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    /**
     * Handle submit form and perform request method
     */
    handleSubmit(event) {
        event.preventDefault();
        let then = this;
        let formData = new FormData(event.target);

        // Send request to
        BaseRequest.post("/api/forgotPassword", JSON.stringify(Object.fromEntries(formData)), {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }).then(res => {
            let data = res.data;

            // If no errors occured, redirect
            if (data.result === "success") {
                then.setState({
                    error: null,
                    success: "Email sended successfully, go check your email address"
                });
                return;
            }

            // Display error
            this.setState({error: "Error occured: " + data.message});
        }).catch(err => then.setState({error: err.message}));
    }

    /**
     * Render page
     * @returns {*}
     */
    render() {
        // Able to redirect
        if (this.state.redirection)
            return (<Redirect from="/" to="/area/index"/>);

        // Default render
        return (
            <>
                <Col lg="5" md="7">
                    <Card className="bg-secondary shadow border-0">
                        {
                            // Display error
                            this.state.error != null ?
                                (
                                    <UncontrolledAlert color="danger">{this.state.error}</UncontrolledAlert>
                                ) : ""
                        }
                        {
                            // Display success
                            this.state.success != null ?
                                (
                                    <UncontrolledAlert color="success">{this.state.success}</UncontrolledAlert>
                                ) : ""
                        }
                        <CardBody className="px-lg-5 py-lg-5">
                            <div className="text-center text-muted mb-4">
                                <small>Enter your email address</small>
                            </div>
                            <form onSubmit={this.handleSubmit}>
                                <FormGroup className="mb-3">
                                    <InputGroup className="input-group-alternative">
                                        <InputGroupAddon addonType="prepend">
                                            <InputGroupText>
                                                <i className="ni ni-email-83" />
                                            </InputGroupText>
                                        </InputGroupAddon>
                                        <Input placeholder="Email" type="email" name="email" />
                                    </InputGroup>
                                </FormGroup>
                                <div className="text-center mt-3">
                                    <Button className="btn-block my-4" color="primary" type="submit">
                                        Send an email
                                    </Button>
                                </div>
                            </form>
                            <Row className="mt-5">
                                <Col className="text-right">
                                    <a
                                        className="btn"
                                        href="/auth/login">
                                        Login
                                    </a>
                                </Col>
                            </Row>
                        </CardBody>
                    </Card>
                </Col>
            </>
        );
    }
}

export default ForgotPassword;
