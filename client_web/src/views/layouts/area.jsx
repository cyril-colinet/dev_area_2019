import React from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import AdminNavbar from "../../components/Navbars/AdminNavbar.jsx";
import Sidebar from "../../components/Sidebar/Sidebar.jsx";

import routes from "../../config/routes.js";

/**
 * area layout
 */
class Area extends React.Component {

    logged = false;
    headers = {};

    /**
     * Constructor
     * @param props
     */
    constructor(props) {
        super(props);

        // Configure logged variable
        if (localStorage.getItem("token") != null) {
            this.logged = true;

            // Configure requests headers
            this.headers = {
                'Authorization': 'Bearer ' + localStorage.getItem("token")
            };
        }
    }

    /**
     * When component was update
     * @param e
     */
    componentDidUpdate(e) {
        document.documentElement.scrollTop = 0;
        document.scrollingElement.scrollTop = 0;
        this.refs.mainContent.scrollTop = 0;
    }

    /**
     * Get admin routes
     * @param routes
     * @returns {*}
     */
    getRoutes(routes) {
        return routes.map((prop, key) => {
            if (prop.layout === "/area" && prop.navigable) {
                return (
                    <Route
                        path={prop.layout + prop.path}
                        component={prop.component}
                        key={key}
                    />
                );
            } else {
                return null;
            }
        });
    };

    /**
     * Get branding text
     * @param path
     * @returns {string|*}
     */
    getBrandText(path) {
        for (let i = 0; i < routes.length; i++) {
            if (this.props.location.pathname.indexOf(routes[i].layout + routes[i].path) !== -1)
                return routes[i].name;
        }

        return "Brand";
    };

    /**
     * Render content
     * @returns {*}
     */
    render() {
        // User not logged, redirect to auth
        if (!this.logged) {
            return (
                <Redirect from="/" to="/auth/login" />
            );
        }

        // User logged, perform requests
        return (
            <>
                <Sidebar
                    {...this.props}
                    routes={routes.filter(item => item.navigable)}
                    logo={{
                        innerLink: "/area/index",
                        imgSrc: require("../../assets/img/logo_light.png"),
                        imgAlt: "Logo"
                    }}
                />
                <div className="main-content" ref="mainContent">
                    <AdminNavbar
                        {...this.props}
                        brandText={this.getBrandText(this.props.location.pathname)}
                    />
                    <Switch>
                        {this.getRoutes(routes)}
                        <Redirect to="/area/index"/>
                    </Switch>
                    {console.log()}
                </div>
            </>
        );
    }
}

export default Area;
