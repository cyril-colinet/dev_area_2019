import React from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import { Container, Row, Col } from "reactstrap";

import routes from "../../config/routes.js";

/**
 * Class Auth default layout
 */
class Auth extends React.Component {

    constructor(props) {
        super(props);

        // Configure state
        this.state = {
            logged: false
        };
    }

    /**
     * When component currently monting
     */
    componentDidMount() {
        document.body.classList.add("bg-default");

        // Check if user already logged in
        if (localStorage.getItem("token") != null)
            this.setState({logged: true});
    }

    /**
     * When component will unmount
     */
    componentWillUnmount() {
        document.body.classList.remove("bg-default");
    }

    /**
     * Get routes for this layout
     */
    getRoutes(routes) {
        // Redirect if user already logged in
        if (this.state.logged)
            return (<Redirect from="/" to="/area/index"/>);

        return routes.map((prop, key) => {
            if (prop.layout === "/auth") {
                return (
                    <Route
                        path={prop.layout + prop.path}
                        component={prop.component}
                        key={key}
                    />
                );
            } else {
                return null;
            }
        });
    };

    /**
     * Render current layout
     */
    render() {
        return (
            <>
                <div className="main-content">
                    <div className="header bg-gradient-blue py-7 py-lg-8">
                        <Container>
                            <div className="header-body text-center mb-7">
                                <Row className="justify-content-center">
                                    <Col lg="5" md="6">
                                        <img src={require('assets/img/logo_dark.png')} alt="Logo" style={{
                                            width: '50%'
                                        }} />
                                    </Col>
                                </Row>
                            </div>
                        </Container>
                        <div className="separator separator-bottom separator-skew zindex-100">
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                preserveAspectRatio="none"
                                version="1.1"
                                viewBox="0 0 2560 100"
                                x="0"
                                y="0">
                                <polygon
                                    className="fill-default"
                                    points="2560 0 2560 100 0 100"
                                />
                            </svg>
                        </div>
                    </div>

                    {/* Page content */}
                    <Container className="mt--9 pb-5">
                        <Row className="justify-content-center">
                            <Switch>
                                {this.getRoutes(routes)}
                                <Redirect to="/auth/login"/>
                            </Switch>
                        </Row>
                    </Container>
                </div>
            </>
        );
    }
}

export default Auth;
