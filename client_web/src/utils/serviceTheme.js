import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faClock, faEnvelope, faEnvelopeOpenText, faNewspaper, faPhone} from "@fortawesome/free-solid-svg-icons";

export default {
                timer: {
                    icon: (<FontAwesomeIcon icon={faClock} color="black"/>),
                    iconLight: (<FontAwesomeIcon icon={faClock} color="white"/>),
                    backgroundColor: "#c0392b",
                    gradient: false
                },
                facebook: {
                    icon: (<FontAwesomeIcon icon={['fab', 'facebook']} color="black"/>),
                    iconLight: (<FontAwesomeIcon icon={['fab', 'facebook']} color="white"/>),
                    backgroundColor: "#3b5998",
                    gradient: false
                },
                github: {
                    icon: (<FontAwesomeIcon icon={['fab', 'github']} color="black"/>),
                    iconLight: (<FontAwesomeIcon icon={['fab', 'github']} color="white"/>),
                    backgroundColor: "#333333",
                    gradient: false
                },
                google: {
                    icon: (<FontAwesomeIcon icon={['fab', 'google']} color="black"/>),
                    iconLight: (<FontAwesomeIcon icon={['fab', 'google']} color="white"/>),
                    backgroundColor: "#4285f4",
                    gradient: false
                },
                sms: {
                    icon: (<FontAwesomeIcon icon={faPhone} color="black"/>),
                    iconLight: (<FontAwesomeIcon icon={faPhone} color="white"/>),
                    backgroundColor: "#2dce89",
                    gradient: false
                },
                dropbox: {
                    icon: (<FontAwesomeIcon icon={['fab', 'dropbox']} color="black"/>),
                    iconLight: (<FontAwesomeIcon icon={['fab', 'dropbox']} color="white"/>),
                    backgroundColor: "#007ee5",
                    gradient: false
                },
                twitter: {
                    icon: (<FontAwesomeIcon icon={['fab', 'twitter']} color="black"/>),
                    iconLight: (<FontAwesomeIcon icon={['fab', 'twitter']} color="white"/>),
                    backgroundColor: "#00acee",
                    gradient: false
                },
                discord: {
                    icon: (<FontAwesomeIcon icon={['fab', 'discord']} color="black"/>),
                    iconLight: (<FontAwesomeIcon icon={['fab', 'discord']} color="white"/>),
                    backgroundColor: "#7289DA",
                    gradient: false
                },
                spotify: {
                    icon: (<FontAwesomeIcon icon={['fab', 'spotify']} color="black"/>),
                    iconLight: (<FontAwesomeIcon icon={['fab', 'spotify']} color="white"/>),
                    backgroundColor: "#1DB954",
                    gradient: false
                },
                news: {
                    icon: (<FontAwesomeIcon icon={faNewspaper} color="black"/>),
                    iconLight: (<FontAwesomeIcon icon={faNewspaper} color="white"/>),
                    backgroundColor: "#d35400",
                    gradient: true,
                    /*                    'gradient': LinearGradient(
                                                    begin: Alignment.topLeft,
                                                    end: Alignment.bottomRight,
                                                    colors: [
                                                        Color(0xFF1e3c72),
                                                        Color(0xFF2a5298),
                                                    ]
                                        )*/
                },
                instagram: {
                    icon: (<FontAwesomeIcon icon={['fab', 'instagram']} color="black"/>),
                    iconLight: (<FontAwesomeIcon icon={['fab', 'instagram']} color="white"/>),
                    backgroundColor: "#4285f4",
                    gradient: true,
                    /*                    'gradient': LinearGradient(
                                                    begin: Alignment.topLeft,
                                                    end: Alignment.bottomRight,
                                                    colors: [
                                                        Color(0xFFfeda75),
                                                        Color(0xFFfa7e1e),
                                                        Color(0xFFd62976),
                                                        Color(0xFF962fbf),
                                                        Color(0xFF4f5bd5),
                                                    ]
                                        )*/
                },
                email: {
                    icon: (<FontAwesomeIcon icon={faEnvelope} color="black"/>),
                    iconLight: (<FontAwesomeIcon icon={faEnvelope} color="white"/>),
                    backgroundColor: "#0072C6",
                    gradient: false
                },
            };
