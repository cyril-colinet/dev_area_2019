import axios from "axios";

export default axios.create({
    baseURL: localStorage.getItem("api_baseurl") || "https://area.epitech-nice.fr",
    responseType: "json",
});