import Index from "../views/area/main.jsx";
import Profile from "../views/area/profile.jsx";
import Register from "../views/auth/register.jsx";
import Login from "../views/auth/login.jsx";
import ForgotPassword from "../views/auth/forgotPassword.jsx";
import ChangePassword from "../views/auth/changePassword.jsx";
import ActionsList from "../views/area/actionsList";
import ReactionsList from "../views/area/reactionsList";
import ConfigureArea from "../views/area/configureAREA";

let routes = [
    {
        path: "/index",
        name: "Dashboard",
        icon: "ni ni-tv-2 text-primary",
        component: Index,
        layout: "/area",
        navigable: true
    },
    {
        path: "/profile",
        name: "Profile",
        icon: "ni ni-single-02 text-yellow",
        component: Profile,
        layout: "/area",
        navigable: true
    },
    {
        path: "/actions",
        name: "Actions",
        icon: "ni ni-active-40 text-red",
        component: ActionsList,
        layout: "/area",
        navigable: true
    },
    {
        path: "/reactions",
        name: "Reactions",
        icon: "ni ni-user-run text-indigo",
        component: ReactionsList,
        layout: "/area",
        navigable: true
    },
    {
        path: "/configure",
        name: "Create new AREA",
        icon: "ni ni-circle-08 text-danger",
        component: ConfigureArea,
        layout: "/area",
        navigable: true
    },
    {
        path: "/login",
        name: "Connexion",
        icon: "ni ni-key-25 text-info",
        component: Login,
        layout: "/auth",
        navigable: false
    },
    {
        path: "/register",
        name: "Create new account",
        icon: "ni ni-circle-08 text-pink",
        component: Register,
        layout: "/auth",
        navigable: false
    },
    {
        path: "/forgotPassword",
        name: "Send a email to access to the reset password form",
        icon: "ni ni-key-25 text-info",
        component: ForgotPassword,
        layout: "/auth",
        navigable: false
    },
    {
        path: "/changePassword",
        name: "Change password",
        icon: "ni ni-key-25 text-info",
        component: ChangePassword,
        layout: "/auth",
        navigable: false
    }
];

export default routes;
