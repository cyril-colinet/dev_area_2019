import React from "react";

import {Container, Row, Col} from "reactstrap";

/**
 * User header
 * Extended from React.Component
 */
class UserHeader extends React.Component {
    render() {
        return (
            <>
                <div
                    className="header pt-5 pt-lg-8 d-flex align-items-center"
                    style={{
                        minHeight: "400px",
                        backgroundImage: "url(" + require("../../assets/img/theme/profile-cover.jpg") + ")",
                        backgroundSize: "cover",
                        backgroundPosition: "center top"
                    }}>
                    {/* Mask */}
                    <span className="mask bg-gradient-default opacity-8"/>
                    {/* Header container */}
                    <Container className="d-flex align-items-center" fluid>
                        <Row>
                            <Col lg="7" md="10">
                                <h1 className="display-2 text-white">{localStorage.getItem("email")}</h1>
                            </Col>
                        </Row>
                    </Container>
                </div>
            </>
        );
    }
}

export default UserHeader;
