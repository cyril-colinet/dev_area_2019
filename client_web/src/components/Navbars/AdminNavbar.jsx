import React from "react";
import {Link} from "react-router-dom";
import {
    DropdownMenu,
    DropdownItem,
    UncontrolledDropdown,
    DropdownToggle,
    Navbar,
    Nav,
    Container,
    Media,
    Button,
} from "reactstrap";

/**
 * area navigation bar
 * Extended from area layout
 */
class AdminNavbar extends React.Component {

    /**
     * Render component default
     * @returns {*}
     */
    render() {
        return (
            <>
                <Navbar className="navbar-top navbar-dark" expand="md" id="navbar-main">
                    <Container fluid>
                        <p className="h2 mb-0 text-white text-uppercase d-none d-lg-inline-block">
                            {this.props.brandText}
                        </p>
                        <Nav className="align-items-center d-none d-md-flex" navbar>
                            <Button
                                color="success"
                                href="/area/configure"
                                size="lg">
                                Configure new AREA
                            </Button>
                            <UncontrolledDropdown nav>
                                <DropdownToggle className="pr-0" nav>
                                    <Media className="align-items-center">
                                        <span className="avatar avatar-sm rounded-circle">
                                            <img
                                                alt="..."
                                                src={require("../../assets/img/theme/team-3-800x800.jpg")}
                                            />
                                        </span>
                                        <Media className="ml-2 d-none d-lg-block">
                                        <span className="mb-0 text-sm font-weight-bold">
                                            {localStorage.getItem("email")}
                                        </span>
                                        </Media>
                                    </Media>
                                </DropdownToggle>
                                <DropdownMenu className="dropdown-menu-arrow" right>
                                    <DropdownItem className="noti-title" header tag="div">
                                        <h6 className="text-overflow m-0">Welcome back!</h6>
                                    </DropdownItem>
                                    <DropdownItem to="/area/profile" tag={Link}>
                                        <i className="ni ni-single-02" />
                                        <span>Profile</span>
                                    </DropdownItem>
                                    <DropdownItem divider />
                                    <DropdownItem href="#pablo" onClick={() => {window.location.reload();localStorage.clear(); }}>
                                        <i className="ni ni-user-run" />
                                        <span>Logout</span>
                                    </DropdownItem>
                                </DropdownMenu>
                            </UncontrolledDropdown>
                        </Nav>
                    </Container>
                </Navbar>
            </>
        );
    }
}

export default AdminNavbar;
