import React from "react";
import ReactDOM from "react-dom";
import {BrowserRouter, Route, Switch, Redirect} from "react-router-dom";
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'

import "assets/vendor/nucleo/css/nucleo.css";
import "assets/vendor/@fortawesome/fontawesome-free/css/all.min.css";
import "assets/scss/argon-dashboard-react.scss";

import AdminLayout from "views/layouts/area.jsx";
import AuthLayout from "views/layouts/auth.jsx";

// Adding brands
library.add(fab);

ReactDOM.render(
    <BrowserRouter>
        <Switch>
            <Route path="/area" render={props => <AdminLayout {...props} />}/>
            <Route path="/auth" render={props => <AuthLayout {...props} />}/>
            <Redirect from="/" to="/auth/login"/>
        </Switch>
    </BrowserRouter>,
    document.getElementById("root")
);
