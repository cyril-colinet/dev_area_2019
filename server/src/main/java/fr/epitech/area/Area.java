package fr.epitech.area;

import fr.epitech.area.action.AreaActions;
import fr.epitech.area.database.Database;
import fr.epitech.area.route.RouteExecutor;
import fr.epitech.area.route.account.AccountAreaListRoute;
import fr.epitech.area.route.account.AccountInfoRoute;
import fr.epitech.area.route.actions.ActionsAvailableRoute;
import fr.epitech.area.route.administration.AdminUsersRoute;
import fr.epitech.area.route.area.AreaCreateRoute;
import fr.epitech.area.route.area.AreaDeleteRoute;
import fr.epitech.area.route.authentification.*;
import fr.epitech.area.route.others.AboutRoute;
import fr.epitech.area.route.others.ErrorRoute;
import fr.epitech.area.route.others.HelpRoute;
import fr.epitech.area.route.reactions.ReactionsAvailableRoute;
import fr.epitech.area.route.services.ServicesCallbackRoute;
import fr.epitech.area.route.services.ServicesRoute;
import fr.epitech.area.route.services.ServicesSubscribeRoute;
import fr.epitech.area.route.services.ServicesUserRoute;
import fr.epitech.area.service.AreaServices;
import io.github.cdimascio.dotenv.Dotenv;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.core.net.PemKeyCertOptions;
import io.vertx.ext.auth.KeyStoreOptions;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.jwt.JWTAuthOptions;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.SQLClient;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Logger;

public class Area extends AbstractVerticle {

	public static final double API_VERSION = 1.18;

	public static String getHostApi() {
		return getDotenv().get("HOST_API", "https://area.epitech-nice.fr");
	}

	/**
	 * Singleton.
	 */
	@Getter
	private static Area instance;

	/**
	 * Logger.
	 */
	@Getter
	private static Logger logger = Logger.getLogger("Area");

	/**
	 * Database manager with table and object managment.
	 */
	@Getter
	private static Database database;

	/**
	 * JWT Auth for Database connection.
	 */
	@Getter
	private static JWTAuth jwtAuth;

	/**
	 * Services manager.
	 */
	@Getter
	private static AreaServices servicesManager;

	@Getter
	private static Dotenv dotenv = Dotenv.load();

	@Getter
	private static ArrayList<RouteExecutor> registeredRoutes;

	/**
	 * Main function when launching verticle.
	 *
	 * @param startPromise
	 */
	@Override
	public void start(Promise<Void> startPromise) {
		instance = this;

		// Init DB Connection
		if (!this.initDatabase()) {
			System.exit(0);
			return;
		}

		// Setup JWT Auth Options
		JWTAuthOptions config = new JWTAuthOptions()
			.setKeyStore(new KeyStoreOptions()
				.setPath("keystore.jceks")
				.setPassword("secret"));
		jwtAuth = JWTAuth.create(vertx, config);

		// Setup main router and init all routes
		Router router = Router.router(vertx);
		router.route("/*").handler(BodyHandler.create());
		router.route("/*").handler(CorsHandler.create(".*.")
			.allowedHeader("Origin")
			.allowedHeader("Content-Type")
			.allowedHeader("Accept")
			.allowedHeader("Authorization"));

		this.registerRoutes(router);

		// Init services -> actions and reactions class
		servicesManager = new AreaServices();

		// Run Actions tasks
		AreaActions.initTasksActions();

		// Run server
		HttpServerOptions serverOptions = new HttpServerOptions();
		if (dotenv.get("SSL_ENABLED") != null && dotenv.get("SSL_ENABLED").equals("true")) {
			serverOptions
				.setSsl(true)
				.setPemKeyCertOptions(new PemKeyCertOptions()
					.addKeyPath("./privkey.pem").addCertPath("./fullchain.pem"));
		}
		vertx.createHttpServer(serverOptions).requestHandler(router).listen(8080);
	}

	/**
	 * Initializing database connection.
	 */
	private boolean initDatabase() {
		if (dotenv.get("DATABASE_URL") == null
			|| dotenv.get("DATABASE_NAME") == null
			|| dotenv.get("DATABASE_USER") == null
			|| dotenv.get("DATABASE_PASSWORD") == null) {
			getLogger().warning("Missing env variables!");
			return false;
		}
		JsonObject config = new JsonObject()
			.put("url", "jdbc:mysql://" + dotenv.get("DATABASE_URL") + "/" + dotenv.get("DATABASE_NAME"))
			.put("user", dotenv.get("DATABASE_USER"))
			.put("password", dotenv.get("DATABASE_PASSWORD"))
			.put("driver_class", "com.mysql.jdbc.Driver")
			.put("max_pool_size", 30);
		SQLClient client = JDBCClient.createShared(vertx, config);
		database = new Database(client);
		database.query("SELECT 1;", (res) -> {
			if (res.succeeded()) {
				getLogger().info("Successful connected to SQL");
			} else {
				getLogger().severe("Error with SQL Connection!");
			}
		});
		return true;
	}

	/**
	 * Register all routes of the api.
	 *
	 * @param router
	 */
	private void registerRoutes(Router router) {
		String prefixRoute = "/api";

		registeredRoutes = new ArrayList<>(Arrays.asList(
			new AboutRoute(),
			new HelpRoute(),

			new AccountInfoRoute(),
			new AccountAreaListRoute(),

			new AdminUsersRoute(),

			new AuthRegisterRoute(),
			new AuthRegisterConfirmRoute(),
			new AuthLoginRoute(),
			new AuthLoginOauthRoute(),
			new AuthLoginCallbackRoute(),
			new AuthForgotPasswordRoute(),
			new AuthResetPasswordRoute(),

			new ActionsAvailableRoute(),

			new ReactionsAvailableRoute(),

			new AreaCreateRoute(),
			new AreaDeleteRoute(),

			new ServicesSubscribeRoute(),
			new ServicesCallbackRoute(),
			new ServicesUserRoute(),
			new ServicesRoute(),

			new ErrorRoute()
		));

		router.route().handler(BodyHandler.create());

		for (RouteExecutor route : registeredRoutes) {
			if (!(route instanceof AboutRoute)
				&& !(route instanceof ErrorRoute)) {
				route.setPath(prefixRoute + route.getPath());
			}

			getLogger().info("Register route: " + route.getRouteType().name() + " " + route.getPath());

			switch (route.getRouteType()) {
				case ALL:
					router.route(route.getPath()).handler(route::preHandling);
					break;
				case GET:
					router.get(route.getPath()).handler(route::preHandling);
					break;
				case POST:
					router.post(route.getPath()).handler(route::preHandling);
					break;
				case DELETE:
					router.delete(route.getPath()).handler(route::preHandling);
					break;
				case PATCH:
					router.patch(route.getPath()).handler(route::preHandling);
					break;
			}
		}
	}

}
