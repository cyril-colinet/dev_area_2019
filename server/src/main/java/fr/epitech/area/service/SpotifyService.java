package fr.epitech.area.service;

import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.oauth.OAuth20Service;
import com.github.scribejava.core.oauth.OAuthService;
import fr.epitech.area.Area;
import fr.epitech.area.action.ActionExecutor;
import fr.epitech.area.action.spotify.SpotifyCurrentPlaying;
import fr.epitech.area.action.spotify.SpotifyNewFollower;
import fr.epitech.area.action.spotify.SpotifyNewSongAddedToPlaylist;
import fr.epitech.area.reaction.ReactionExecutor;
import fr.epitech.area.reaction.spotify.SpotifyNextTrack;
import fr.epitech.area.reaction.spotify.SpotifyPreviousTrack;
import fr.epitech.area.service.api.SpotifyApi;
import fr.epitech.area.utils.UserService;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.WebClient;

import java.util.ArrayList;
import java.util.Arrays;

public class SpotifyService extends ServiceExecutor {

	private final String CLIENT_ID = "9f65f677b8234e369ab69cf2d9be7f81";
	private final String CLIENT_SECRET = "d241c80f71dd4fe3b4e2e0db4d8269a7";

	public SpotifyService(int id) {
		super(id, "spotify", true);
	}

	@Override
	public ArrayList<ActionExecutor> loadActions() {
		return new ArrayList<>(Arrays.asList(
			new SpotifyCurrentPlaying(this, 16),
			new SpotifyNewSongAddedToPlaylist(this, 17),
			new SpotifyNewFollower(this, 18)
		));
	}

	@Override
	public ArrayList<ReactionExecutor> loadReactions() {
		return new ArrayList<>(Arrays.asList(
			new SpotifyNextTrack(this, 8),
			new SpotifyPreviousTrack(this, 9)
		));
	}

	@Override
	public OAuthService getService(String callbackUrl, String secretState) {
		if (!this.getOauthServices().containsKey(secretState)) {
			OAuthService service = new ServiceBuilder(CLIENT_ID)
				.apiSecret(CLIENT_SECRET)
				.callback(callbackUrl)
				.defaultScope("user-read-playback-state user-follow-read user-modify-playback-state")
				.debug()
				.build(SpotifyApi.instance());
			this.getOauthServices().put(secretState, service);
			return service;
		}
		return this.getOauthServices().get(secretState);
	}

	@Override
	public String getOAuthUrl(RoutingContext context, String callbackUrl, String secretState) {
		OAuth20Service service = ((OAuth20Service) this.getService(callbackUrl, secretState));
		return service.createAuthorizationUrlBuilder().state(secretState).build();
	}

	@Override
	public UserService getCallbackOauthData(RoutingContext context, String secretState) {
		OAuth20Service service = ((OAuth20Service) this.getService(context.request().absoluteURI().split("\\?")[0],
			secretState));

		// Get oauth access token from github api
		try {
			OAuth2AccessToken accessToken = service.getAccessToken(context.request().getParam("code"));
			return new UserService(new JsonObject().put("token", accessToken.getAccessToken()), secretState);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public void getLoginIdentity(UserService userService, Handler<String> identity) {
		if (!userService.getTokenObject().containsKey("token")) {
			identity.handle(null);
			return;
		}

		// Get discord user id
		WebClient client = WebClient.create(Area.getInstance().getVertx());
		client.get(443, "api.spotify.com", "/v1/me")
			.ssl(true)
			.bearerTokenAuthentication(userService.getTokenObject().getString("token"))
			.send(ar -> {
				if (ar.succeeded()) {
					JsonObject body = ar.result().bodyAsJsonObject();
					identity.handle("" + body.getString("id"));
				} else {
					identity.handle(null);
				}
			});
	}

}
