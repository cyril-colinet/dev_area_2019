package fr.epitech.area.service;

import fr.epitech.area.action.ActionExecutor;
import fr.epitech.area.action.news.NewsNewCategorieTopHeadlineAction;
import fr.epitech.area.action.news.NewsNewCountryTopHeadlineAction;
import fr.epitech.area.action.news.NewsNewSubjectTopHeadlineAction;
import fr.epitech.area.reaction.ReactionExecutor;

import java.util.ArrayList;
import java.util.Arrays;

public class NewsService extends ServiceExecutor {

	private final String API_KEY = "5e5c45843f8c40c9a083bed725a5d08e";

	public NewsService(int id) {
		super(id, "news", false);
	}

	@Override
	public ArrayList<ActionExecutor> loadActions() {
		return new ArrayList<>(Arrays.asList(
			new NewsNewCountryTopHeadlineAction(this, 4),
			new NewsNewCategorieTopHeadlineAction(this, 5),
			new NewsNewSubjectTopHeadlineAction(this, 6)
		));
	}

	@Override
	public ArrayList<ReactionExecutor> loadReactions() {
		return new ArrayList<>(Arrays.asList(
		));
	}

	@Override
	public String getApiKey() {
		return this.API_KEY;
	}

}
