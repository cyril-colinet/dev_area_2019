package fr.epitech.area.service;

import fr.epitech.area.action.ActionExecutor;
import fr.epitech.area.reaction.ReactionExecutor;
import fr.epitech.area.reaction.email.EmailSend;

import java.util.ArrayList;
import java.util.Arrays;

public class EmailService extends ServiceExecutor {

	public EmailService(int id) {
		super(id, "email", false);
	}

	@Override
	public ArrayList<ActionExecutor> loadActions() {
		return new ArrayList<>(Arrays.asList(
		));
	}

	@Override
	public ArrayList<ReactionExecutor> loadReactions() {
		return new ArrayList<>(Arrays.asList(
			new EmailSend(this, 6)
		));
	}

}
