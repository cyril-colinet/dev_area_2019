package fr.epitech.area.service;

import com.github.scribejava.core.oauth.OAuthService;
import fr.epitech.area.Area;
import fr.epitech.area.action.ActionExecutor;
import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.reaction.ReactionExecutor;
import fr.epitech.area.utils.UserService;
import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;
import lombok.Getter;

import java.util.ArrayList;
import java.util.HashMap;

public abstract class ServiceExecutor {

	/**
	 * Service id.
	 */
	@Getter
	private int id;

	/**
	 * Service name.
	 */
	@Getter
	private String name;

	/**
	 * TRUE if oauth is enable on this service.
	 */
	@Getter
	private boolean needOAuth;

	/**
	 * List of oauth services by secret
	 */
	@Getter
	private HashMap<String, OAuthService> oauthServices = new HashMap<>();

	/**
	 * List of all actions.
	 */
	@Getter
	private ArrayList<ActionExecutor> actions;

	/**
	 * List of all reactions.
	 */
	@Getter
	private ArrayList<ReactionExecutor> reactions;


	/**
	 * Abstract DashboardService object.
	 *
	 * @param id
	 * @param name
	 * @param needOAuth
	 */
	public ServiceExecutor(int id, String name, boolean needOAuth) {
		this.id = id;
		this.name = name;
		this.needOAuth = needOAuth;
		this.actions = this.loadActions();
		this.reactions = this.loadReactions();
	}

	/**
	 * Get registered service or default
	 * @param callbackUrl
	 * @param secretState
	 * @return
	 */
	public OAuthService getService(String callbackUrl, String secretState) {
		return null;
	}

	/**
	 * Get user oauth JsonObject (for token and other set when user confirm oauth)
	 *
	 * @param user
	 * @param data
	 */
	public void getOauthData(UserModel user, Handler<UserService> data) {
		Area.getDatabase().getServicesTable().getService(user.getId(), this.getId(), (service) -> {
			if (service == null) {
				data.handle(null);
				return;
			}
			data.handle(new UserService(service.getData(), service.getUniqueState()));
		});
	}

	/**
	 * Get oauth url when requested.
	 *
	 * @param context
	 * @param secretState
	 * @return URL String of oauth.
	 */
	public String getOAuthUrl(RoutingContext context, String callbackUrl, String secretState) {
		return null;
	}

	/**
	 * Get result of oauth.
	 *
	 * @param context
	 * @return Service data stocked.
	 */
	public UserService getCallbackOauthData(RoutingContext context, String secretState) {
		return null;
	}

	/**
	 * Get login identifier from api
	 * @param userService
	 * @param identity
	 */
	public void getLoginIdentity(UserService userService, Handler<String> identity) {

	}

	/**
	 * Initializing all actions of an services.
	 *
	 * @return List of all actions.
	 */
	public abstract ArrayList<ActionExecutor> loadActions();

	/**
	 * Initializing all reactions of an services.
	 *
	 * @return List of all reactions.
	 */
	public abstract ArrayList<ReactionExecutor> loadReactions();

	/**
	 *
	 * @return Client ID
	 */
	public String getClientId() {
		return null;
	}

	/**
	 *
	 * @return Client secret
	 */
	public String getClientSecret() {
		return null;
	}

	/**
	 *
	 * @return API Key
	 */
	public String getApiKey() {
		return null;
	}
}
