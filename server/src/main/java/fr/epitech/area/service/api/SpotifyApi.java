package fr.epitech.area.service.api;

import com.github.scribejava.core.builder.api.DefaultApi20;

public class SpotifyApi extends DefaultApi20 {

	protected SpotifyApi() {
	}

	private static class InstanceHolder {
		private static final SpotifyApi INSTANCE = new SpotifyApi();
	}

	public static SpotifyApi instance() {
		return SpotifyApi.InstanceHolder.INSTANCE;
	}

	@Override
	public String getAccessTokenEndpoint(){
		return "https://accounts.spotify.com/api/token";
	}

	@Override
	protected String getAuthorizationBaseUrl() {
		return "https://accounts.spotify.com/authorize";
	}
}
