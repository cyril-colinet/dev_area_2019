package fr.epitech.area.service;

import com.github.scribejava.apis.GoogleApi20;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.oauth.OAuth20Service;
import com.github.scribejava.core.oauth.OAuthService;
import fr.epitech.area.Area;
import fr.epitech.area.action.ActionExecutor;
import fr.epitech.area.action.gmail.GmailNewMailAction;
import fr.epitech.area.reaction.ReactionExecutor;
import fr.epitech.area.reaction.gmail.GmailEmptyTrash;
import fr.epitech.area.reaction.gmail.GmailSendMailReaction;
import fr.epitech.area.utils.UserService;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.WebClient;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class GoogleService extends ServiceExecutor {

	private final String CLIENT_ID = "702776428036-iiohif0fc4pib2tgjqd40cnjg5a5thh3.apps.googleusercontent.com";
	private final String CLIENT_SECRET = "EUp3oHtbgzIuyT7obwLGjThK";

	public GoogleService(int id) {
		super(id, "google", true);
	}

	@Override
	public ArrayList<ActionExecutor> loadActions() {
		return new ArrayList<>(Arrays.asList(
			new GmailNewMailAction(this, 2)
		));
	}

	@Override
	public ArrayList<ReactionExecutor> loadReactions() {
		return new ArrayList<>(Arrays.asList(
			new GmailSendMailReaction(this, 3),
			new GmailEmptyTrash(this, 7)
		));
	}

	@Override
	public OAuthService getService(String callbackUrl, String secretState) {
		if (!this.getOauthServices().containsKey(secretState)) {
			OAuthService service = new ServiceBuilder(CLIENT_ID)
				.apiSecret(CLIENT_SECRET)
				.defaultScope("profile")
				.callback(callbackUrl)
				.build(GoogleApi20.instance());
			this.getOauthServices().put(secretState, service);
			return service;
		}
		return this.getOauthServices().get(secretState);
	}

	@Override
	public String getOAuthUrl(RoutingContext context, String callbackUrl, String secretState) {
		OAuth20Service service = ((OAuth20Service) this.getService(callbackUrl, secretState));
		Map<String, String> additionalParams = new HashMap<>();
		additionalParams.put("access_type", "offline");
		additionalParams.put("prompt", "consent");

		return service.createAuthorizationUrlBuilder().state(secretState)
			.additionalParams(additionalParams).build();
	}

	@Override
	public UserService getCallbackOauthData(RoutingContext context, String secretState) {
		OAuth20Service service = ((OAuth20Service) this.getService(context.request().absoluteURI().split("\\?")[0],
			secretState));

		// Get oauth access token from discord api
		try {
			OAuth2AccessToken accessToken = service.getAccessToken(context.request().getParam("code"));
			return new UserService(new JsonObject()
				.put("token", accessToken.getAccessToken()), secretState);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public void getLoginIdentity(UserService userService, Handler<String> identity) {
		if (!userService.getTokenObject().containsKey("token")) {
			identity.handle(null);
			return;
		}

		// Get unique id
		WebClient client = WebClient.create(Area.getInstance().getVertx());
		client.get(443, "www.googleapis.com", "/oauth2/v3/userinfo")
			.ssl(true)
			.bearerTokenAuthentication(userService.getTokenObject().getString("token"))
			.send(ar -> {
				if (ar.succeeded()) {
					JsonObject body = ar.result().bodyAsJsonObject();
					identity.handle("" + body.getString("id"));
				} else {
					identity.handle(null);
				}
			});
	}
}
