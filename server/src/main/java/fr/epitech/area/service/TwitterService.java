package fr.epitech.area.service;

import com.github.scribejava.apis.TwitterApi;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.*;
import com.github.scribejava.core.oauth.OAuth10aService;
import com.github.scribejava.core.oauth.OAuthService;
import fr.epitech.area.Area;
import fr.epitech.area.action.ActionExecutor;
import fr.epitech.area.action.twitter.TwitterNewDirectMessage;
import fr.epitech.area.action.twitter.TwitterNewFollower;
import fr.epitech.area.action.twitter.TwitterNewTweetFromUser;
import fr.epitech.area.reaction.ReactionExecutor;
import fr.epitech.area.reaction.twitter.TwitterPostNewPrivateMessage;
import fr.epitech.area.reaction.twitter.TwitterPostStatus;
import fr.epitech.area.utils.UserService;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class TwitterService extends ServiceExecutor {

	private final String CLIENT_ID = "NTf71SnrGZI0W9BlYtzkFo6Yz";
	private final String CLIENT_SECRET = "iSpfxjX21AQufrLXtQnv5HAcL3nSVR6m7EjWH7l3uQDoyOleeD";

	private HashMap<String, OAuth1RequestToken> oauthRequestsTokens = new HashMap<>();

	public TwitterService(int id) {
		super(id, "twitter", true);
	}

	@Override
	public ArrayList<ActionExecutor> loadActions() {
		return new ArrayList<>(Arrays.asList(
			new TwitterNewDirectMessage(this, 11),
			new TwitterNewTweetFromUser(this, 12),
			new TwitterNewFollower(this, 13)
		));
	}

	@Override
	public ArrayList<ReactionExecutor> loadReactions() {
		return new ArrayList<>(Arrays.asList(
			new TwitterPostStatus(this, 5),
			new TwitterPostNewPrivateMessage(this, 7)
		));
	}

	@Override
	public OAuthService getService(String callbackUrl, String secretState) {
		if (!this.getOauthServices().containsKey(secretState)) {
			OAuthService service = new ServiceBuilder(CLIENT_ID)
				.apiSecret(CLIENT_SECRET)
				.callback(callbackUrl + "?state=" + secretState)
				//.debug()
				.build(TwitterApi.instance());
			this.getOauthServices().put(secretState, service);
		}
		return this.getOauthServices().get(secretState);
	}

	@Override
	public String getOAuthUrl(RoutingContext context, String callbackUrl, String secretState) {
		OAuth10aService service = ((OAuth10aService) this.getService(callbackUrl, secretState));

		// Get request token and return authorize url
		try {
			final OAuth1RequestToken requestToken = service.getRequestToken();
			oauthRequestsTokens.put(secretState, requestToken);
			return service.getAuthorizationUrl(requestToken);
		} catch (Exception err) {
			err.printStackTrace();
		}
		return null;
	}

	@Override
	public UserService getCallbackOauthData(RoutingContext context, String secretState) {
		OAuth10aService service = ((OAuth10aService) this.getService(context.request().absoluteURI(),
			secretState));

		try {
			OAuth1AccessToken accessToken = service.getAccessToken(
				oauthRequestsTokens.get(secretState),
				context.request().getParam("oauth_verifier"));

			// Generate token object
			JsonObject tokenObj = new JsonObject()
				.put("token", accessToken.getToken())
				.put("token_secret", accessToken.getTokenSecret());

			return new UserService(tokenObj, secretState);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void getLoginIdentity(UserService userService, Handler<String> identity) {
		if (!userService.getTokenObject().containsKey("token")) {
			identity.handle(null);
			return;
		}

		// Get service
		OAuth10aService service = ((OAuth10aService) this.getOauthServices().get(userService.getUniqueState()));
		if (service == null) {
			identity.handle(null);
			return;
		}

		// Create request
		JsonObject tokenObj = userService.getTokenObject();
		OAuthRequest request = new OAuthRequest(Verb.GET, "https://api.twitter.com/1.1/account/verify_credentials.json");
		OAuth1AccessToken accessToken = new OAuth1AccessToken(tokenObj.getString("token"), tokenObj.getString("token_secret"));

		// Sign request and execute it
		service.signRequest(accessToken, request);
		try (Response response = service.execute(request)) {
			Area.getLogger().info(response.getBody());
		} catch (Exception err) {
			err.printStackTrace();
		}
	}
}
