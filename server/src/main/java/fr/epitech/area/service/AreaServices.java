package fr.epitech.area.service;

import fr.epitech.area.action.ActionExecutor;
import fr.epitech.area.reaction.ReactionExecutor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Arrays;

public class AreaServices {

	@Getter
	private ArrayList<ServiceExecutor> services;

	/**
	 * Dashboard service manager, initializing child service.
	 */
	public AreaServices() {
		services = new ArrayList<>(Arrays.asList(
			new FacebookService(1),
			new TimerService(2),
			new GithubService(3),
			new GoogleService(4),
			new InstagramService(5),
			new SmsService(6),
			new NewsService(7),
			new DropboxService(8),
			new TwitterService(9),
			new DiscordService(10),
			new SpotifyService(11),
			new EmailService(12)
		));
	}

	/**
	 * Get service object with id on param.
	 *
	 * @param serviceId Id of searched service.
	 * @return Service object.
	 */
	public ServiceExecutor getService(int serviceId) {
		for (ServiceExecutor service : this.getServices()) {
			if (service.getId() == serviceId) {
				return service;
			}
		}
		return null;
	}

	public ActionExecutor getAction(int actionId) {
		for (ServiceExecutor service : this.getServices()) {
			for (ActionExecutor action : service.getActions()) {
				if (action.getId() == actionId) {
					return action;
				}
			}
		}
		return null;
	}

	public ReactionExecutor getReaction(int reactionId) {
		for (ServiceExecutor service : this.getServices()) {
			for (ReactionExecutor reaction : service.getReactions()) {
				if (reaction.getId() == reactionId) {
					return reaction;
				}
			}
		}
		return null;
	}

}
