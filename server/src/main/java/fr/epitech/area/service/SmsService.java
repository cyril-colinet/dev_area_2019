package fr.epitech.area.service;

import fr.epitech.area.action.ActionExecutor;
import fr.epitech.area.reaction.ReactionExecutor;
import fr.epitech.area.reaction.sms.SmsSendMessageReaction;

import java.util.ArrayList;
import java.util.Arrays;

public class SmsService extends ServiceExecutor {

    private final String CLIENT_ID = "4b7864b8";
    private final String CLIENT_SECRET = "rteCG957K4qPZoeH";

    public SmsService(int id) {
        super(id, "sms", false);
    }

    @Override
    public ArrayList<ActionExecutor> loadActions() {
        return new ArrayList<>(Arrays.asList(
        ));
    }

    @Override
    public ArrayList<ReactionExecutor> loadReactions() {
        return new ArrayList<>(Arrays.asList(
                new SmsSendMessageReaction(this, 4)
        ));
    }

    @Override
    public String getClientId() {
        return this.CLIENT_ID;
    }

    @Override
    public String getClientSecret() {
        return this.CLIENT_SECRET;
    }
}
