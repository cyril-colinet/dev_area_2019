package fr.epitech.area.service;

import com.github.scribejava.apis.DiscordApi;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.oauth.OAuth20Service;
import com.github.scribejava.core.oauth.OAuthService;
import fr.epitech.area.Area;
import fr.epitech.area.action.ActionExecutor;
import fr.epitech.area.action.discord.DiscordNewFriendRequest;
import fr.epitech.area.action.discord.DiscordNewPrivateMessage;
import fr.epitech.area.reaction.ReactionExecutor;
import fr.epitech.area.utils.UserService;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.WebClient;

import java.util.ArrayList;
import java.util.Arrays;

public class DiscordService extends ServiceExecutor {

	private final String CLIENT_ID = "683127553447165973";
	private final String CLIENT_SECRET = "-wD9pBNzZa18HjIFZUrgwIRiV7p4TUlrJ";

	public DiscordService(int id) {
		super(id, "discord", true);
	}

	@Override
	public ArrayList<ActionExecutor> loadActions() {
		return new ArrayList<>(Arrays.asList(
			new DiscordNewPrivateMessage(this, 14),
			new DiscordNewFriendRequest(this, 15)
		));
	}

	@Override
	public ArrayList<ReactionExecutor> loadReactions() {
		return new ArrayList<>(Arrays.asList(
		));
	}

	@Override
	public OAuthService getService(String callbackUrl, String secretState) {
		if (!this.getOauthServices().containsKey(secretState)) {
			OAuthService service = new ServiceBuilder(CLIENT_ID)
				.apiSecret(CLIENT_SECRET)
				.defaultScope("identify relationships.read activities.read")
				.callback(callbackUrl)
				.userAgent("ScribeJava")
				.debug()
				.build(DiscordApi.instance());
			this.getOauthServices().put(secretState, service);
			return service;
		}
		return this.getOauthServices().get(secretState);
	}

	@Override
	public String getOAuthUrl(RoutingContext context, String callbackUrl, String secretState) {
		OAuth20Service service = ((OAuth20Service) this.getService(callbackUrl, secretState));
		return service.createAuthorizationUrlBuilder().state(secretState).build();
	}

	@Override
	public UserService getCallbackOauthData(RoutingContext context, String secretState) {
		OAuth20Service service = ((OAuth20Service) this.getService(context.request().absoluteURI().split("\\?")[0],
			secretState));

		// Get oauth access token from discord api
		try {
			OAuth2AccessToken accessToken = service.getAccessToken(context.request().getParam("code"));
			return new UserService(new JsonObject().put("token", accessToken.getAccessToken()), secretState);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public void getLoginIdentity(UserService userService, Handler<String> identity) {
		if (!userService.getTokenObject().containsKey("token")) {
			identity.handle(null);
			return;
		}

		WebClient client = WebClient.create(Area.getInstance().getVertx());
		client.get(443, "discordapp.com", "/api/users/@me")
			.ssl(true)
			.bearerTokenAuthentication(userService.getTokenObject().getString("token"))
			.send(ar -> {
				if (ar.succeeded()) {
					JsonObject body = ar.result().bodyAsJsonObject();
					identity.handle("" + body.getString("id"));
				} else {
					identity.handle(null);
				}
			});
	}

}
