package fr.epitech.area.service;

import com.github.scribejava.apis.GitHubApi;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.oauth.OAuth20Service;
import com.github.scribejava.core.oauth.OAuthService;
import fr.epitech.area.Area;
import fr.epitech.area.action.ActionExecutor;
import fr.epitech.area.action.github.GithubCheckNewReleaseAction;
import fr.epitech.area.action.github.GithubCheckNewRepositoryAction;
import fr.epitech.area.reaction.ReactionExecutor;
import fr.epitech.area.utils.UserService;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.WebClient;

import java.util.ArrayList;
import java.util.Arrays;

public class GithubService extends ServiceExecutor {

	private final String CLIENT_ID = "8cd32f02e4471dce87d8";
	private final String CLIENT_SECRET = "cbb9e08a8dbbbcf70af002203b9e15b179c71b7e";

	public GithubService(int id) {
		super(id, "github", true);
	}

	@Override
	public ArrayList<ActionExecutor> loadActions() {
		return new ArrayList<>(Arrays.asList(
			new GithubCheckNewRepositoryAction(this, 0),
			new GithubCheckNewReleaseAction(this, 1)
		));
	}

	@Override
	public ArrayList<ReactionExecutor> loadReactions() {
		return new ArrayList<>(Arrays.asList(

		));
	}

	@Override
	public OAuthService getService(String callbackUrl, String secretState) {
		if (!this.getOauthServices().containsKey(secretState)) {
			OAuthService service = new ServiceBuilder(CLIENT_ID)
				.apiSecret(CLIENT_SECRET)
				.callback(callbackUrl)
				.build(GitHubApi.instance());
			this.getOauthServices().put(secretState, service);
			return service;
		}
		return this.getOauthServices().get(secretState);
	}

	@Override
	public String getOAuthUrl(RoutingContext context, String callbackUrl, String secretState) {
		OAuth20Service service = ((OAuth20Service) this.getService(callbackUrl, secretState));
		return service.createAuthorizationUrlBuilder().state(secretState).build();
	}

	@Override
	public UserService getCallbackOauthData(RoutingContext context, String secretState) {
		OAuth20Service service = ((OAuth20Service) this.getService(context.request().absoluteURI().split("\\?")[0],
			secretState));

		// Get oauth access token from github api
		try {
			OAuth2AccessToken accessToken = service.getAccessToken(context.request().getParam("code"));
			return new UserService(new JsonObject().put("token", accessToken.getAccessToken()), secretState);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public void getLoginIdentity(UserService userService, Handler<String> identity) {
		if (!userService.getTokenObject().containsKey("token")) {
			identity.handle(null);
			return;
		}

		// Get github auth user_id
		WebClient client = WebClient.create(Area.getInstance().getVertx());
		client.get(443, "api.github.com", "/user")
			.ssl(true)
			.bearerTokenAuthentication(userService.getTokenObject().getString("token"))
			.send(ar -> {
				Area.getLogger().info("debug 3");
				if (ar.succeeded()) {
					JsonObject body = ar.result().bodyAsJsonObject();
					identity.handle( body.getInteger("id").toString());
				} else {
					identity.handle(null);
				}
			});
	}
}
