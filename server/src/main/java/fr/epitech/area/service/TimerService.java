package fr.epitech.area.service;

import fr.epitech.area.action.ActionExecutor;
import fr.epitech.area.action.timer.TimerDayAction;
import fr.epitech.area.action.timer.TimerEveryMinute;
import fr.epitech.area.action.timer.TimerHourAction;
import fr.epitech.area.action.timer.TimerXBeforeAction;
import fr.epitech.area.reaction.ReactionExecutor;

import java.util.ArrayList;
import java.util.Arrays;

public class TimerService extends ServiceExecutor {

	public TimerService(int id) {
		super(id, "timer", false);
	}

	@Override
	public ArrayList<ActionExecutor> loadActions() {
		return new ArrayList<>(Arrays.asList(
			new TimerDayAction(this, 7),
			new TimerHourAction(this, 8),
			new TimerXBeforeAction(this, 9),
			new TimerEveryMinute(this, 10)
		));
	}

	@Override
	public ArrayList<ReactionExecutor> loadReactions() {
		return new ArrayList<>();
	}
}
