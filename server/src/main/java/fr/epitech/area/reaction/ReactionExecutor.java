package fr.epitech.area.reaction;

import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.service.ServiceExecutor;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import lombok.Getter;

import java.util.HashMap;

public abstract class ReactionExecutor {

	@Getter
	private int id;

	@Getter
	private ServiceExecutor parentService;

	@Getter
	private String name;

	@Getter
	private String description;

	@Getter
	private HashMap<String, String> requiredConfig = new HashMap<>();

	public ReactionExecutor(ServiceExecutor parentService, int id, String name, String description) {
		this.parentService = parentService;
		this.id = id;
		this.name =  name;
		this.description = description;
	}

	public abstract void runReaction(UserModel user, JsonObject config, Handler<Boolean> result);

	public JsonObject getRequireDataJson() {
		JsonObject json = new JsonObject();
		this.requiredConfig.forEach(json::put);
		return json;
	}

}
