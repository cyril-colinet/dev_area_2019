package fr.epitech.area.reaction.facebook;

import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.reaction.ReactionExecutor;
import fr.epitech.area.service.ServiceExecutor;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;

public class FacebookReaction extends ReactionExecutor {

	public FacebookReaction(ServiceExecutor parentService, int id) {
		super(parentService, id,
			"post_facebook",
			"Post facebook with user account"
		);
	}

	@Override
	public void runReaction(UserModel user, JsonObject config, Handler<Boolean> result) {
		// Config of reaction: config.getString("time_hour")
		// Token of Service linked by user:
		/*
		this.getParentService().getOauthData(user, json -> {
			String token = json.getString("token");
		});
		 */
	}
}
