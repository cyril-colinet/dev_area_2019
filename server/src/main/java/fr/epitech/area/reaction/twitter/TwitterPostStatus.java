package fr.epitech.area.reaction.twitter;

import com.github.scribejava.core.model.OAuth1AccessToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth10aService;
import com.github.scribejava.core.oauth.OAuthService;
import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.reaction.ReactionExecutor;
import fr.epitech.area.service.ServiceExecutor;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;

public class TwitterPostStatus extends ReactionExecutor {

	public TwitterPostStatus(ServiceExecutor parentService, int id) {
		super(parentService, id,
			"twitter_new_tweet",
			"Post new tweet into your account"
		);
		this.getRequiredConfig().put("message", "Content of the new post");
	}

	@Override
	public void runReaction(UserModel user, JsonObject config, Handler<Boolean> result) {
		ServiceExecutor twitterService = this.getParentService();
		twitterService.getOauthData(user, userService -> {
			OAuthService oauthService = twitterService.getService(null, userService.getUniqueState());
			if (!(oauthService instanceof OAuth10aService)) {
				result.handle(false);
				return;
			}

			// Cast service to current
			OAuth10aService service = ((OAuth10aService) oauthService);

			// Create request
			JsonObject tokenObj = userService.getTokenObject();
			OAuthRequest request = new OAuthRequest(Verb.POST, "https://api.twitter.com/1.1/statuses/update.json");
			request.addParameter("status", config.getString("message"));
			OAuth1AccessToken accessToken = new OAuth1AccessToken(tokenObj.getString("token"), tokenObj.getString("token_secret"));

			// Sign request and execute it
			service.signRequest(accessToken, request);
			try (Response response = service.execute(request)) {
				result.handle(response.isSuccessful());
			} catch (Exception err) {
				err.printStackTrace();
				result.handle(false);
			}
		});
	}
}
