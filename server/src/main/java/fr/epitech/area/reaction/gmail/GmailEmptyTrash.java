package fr.epitech.area.reaction.gmail;

import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.reaction.ReactionExecutor;
import fr.epitech.area.service.ServiceExecutor;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;

public class GmailEmptyTrash extends ReactionExecutor {

    public GmailEmptyTrash(ServiceExecutor parentService, int id) {
        super(parentService, id,
			"gmail_empty_trash",
			"Send a mail using Gmail"
        );
    }

    @Override
    public void runReaction(UserModel user, JsonObject config, Handler<Boolean> result) {
    	result.handle(true);
    }
}
