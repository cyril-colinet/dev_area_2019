package fr.epitech.area.reaction.email;

import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.reaction.ReactionExecutor;
import fr.epitech.area.service.ServiceExecutor;
import fr.epitech.area.utils.Email;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;

public class EmailSend extends ReactionExecutor {

	public EmailSend(ServiceExecutor parentService, int id) {
		super(parentService, id,
			"send_email",
			"Send email to user with custom message"
		);
		this.getRequiredConfig().put("to_email", "Email receiver");
		this.getRequiredConfig().put("subject", "Subject of the email");
		this.getRequiredConfig().put("content", "Content of the email");
	}

	@Override
	public void runReaction(UserModel user, JsonObject config, Handler<Boolean> result) {
		result.handle(new Email()
			.setTo(config.getString("to_email"))
			.setContent(config.getString("content"))
			.setSubject("[AREA] " + config.getString("subject"))
			.send());
	}
}
