package fr.epitech.area.reaction.gmail;

import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.reaction.ReactionExecutor;
import fr.epitech.area.service.ServiceExecutor;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;

public class GmailSendMailReaction extends ReactionExecutor {

    public GmailSendMailReaction(ServiceExecutor parentService, int id) {
        super(parentService, id,
                "gmail_send_mail",
                "Send a mail using Gmail"
        );
        this.getRequiredConfig().put("subject", "Subject of the email");
        this.getRequiredConfig().put("text", "Content of the email");
        this.getRequiredConfig().put("from", "From email address");
        this.getRequiredConfig().put("to", "Email receiver");
    }

    @Override
    public void runReaction(UserModel user, JsonObject config, Handler<Boolean> result) {
    	/*try {
			Properties props = new Properties();
			Session session = Session.getDefaultInstance(props, null);
			MimeMessage email = new MimeMessage(session);

			email.setFrom(new InternetAddress(config.getString("from")));
			email.addRecipient(javax.mail.Message.RecipientType.TO, new InternetAddress(config.getString("to")));
			email.setSubject(config.getString("subject"));
			email.setText(config.getString("text"));

			ByteArrayOutputStream buffer = new ByteArrayOutputStream();
			email.writeTo(buffer);
			byte[] bytes = buffer.toByteArray();
			String encodedEmail = Base64.encodeBase64URLSafeString(bytes);
			String url = "https://www.googleapis.com/gmail/v1/users/" + config.getString("gmail_user_id") + "/messages/send";
			WebClient webClient = WebClient.create(Vertx.vertx());
			MultipartForm multipartForm = MultipartForm.create();

			multipartForm.attribute("raw", encodedEmail);
			webClient.post(url)
				.sendMultipartForm(multipartForm, ar -> {
					if (ar.succeeded()) {
						result.handle(true);
					} else {
						result.handle(false);
					}
				});
		} catch (Exception exception) {
			exception.printStackTrace();
			result.handle(false);
		}*/
    }
}
