package fr.epitech.area.reaction.twitter;

import com.github.scribejava.core.model.OAuth1AccessToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth10aService;
import com.github.scribejava.core.oauth.OAuthService;
import fr.epitech.area.Area;
import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.reaction.ReactionExecutor;
import fr.epitech.area.service.ServiceExecutor;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;

public class TwitterPostNewPrivateMessage extends ReactionExecutor {

	public TwitterPostNewPrivateMessage(ServiceExecutor parentService, int id) {
		super(parentService, id,
			"send_new_dm",
			"Send private message to user"
		);
		this.getRequiredConfig().put("username", "Username of the twitter account");
		this.getRequiredConfig().put("message", "Message content");
	}

	@Override
	public void runReaction(UserModel user, JsonObject config, Handler<Boolean> result) {
		ServiceExecutor twitterService = this.getParentService();
		twitterService.getOauthData(user, userService -> {
			OAuthService oauthService = twitterService.getService(null, userService.getUniqueState());
			if (!(oauthService instanceof OAuth10aService)) {
				result.handle(false);
				return;
			}

			// Cast service to current and access token creation
			OAuth10aService service = ((OAuth10aService) oauthService);
			JsonObject tokenObj = userService.getTokenObject();
			OAuth1AccessToken accessToken = new OAuth1AccessToken(tokenObj.getString("token"), tokenObj.getString("token_secret"));

			// Get recipient_id from username
			OAuthRequest recipientRequest = new OAuthRequest(Verb.GET, "https://api.twitter.com/1.1/users/show.json?screen_name=" + config.getString("username"));
			service.signRequest(accessToken, recipientRequest);
			try (Response recipientResponse = service.execute(recipientRequest)) {
				JsonObject userObject = new JsonObject(recipientResponse.getBody());
				recipientResponse.close();
				if (userObject.getString("id_str") == null) {
					result.handle(false);
					return;
				}

				// Event object
				JsonObject eventObj = new JsonObject()
					.put("event", new JsonObject()
						.put("type", "message_create")
						.put("message_create", new JsonObject()
							.put("target", new JsonObject().put("recipient_id", userObject.getString("id_str")))
							.put("message_data", new JsonObject().put("text", config.getString("message")))
						)
					);

				// Create request POST for new message
				OAuthRequest request = new OAuthRequest(Verb.POST, "https://api.twitter.com/1.1/direct_messages/events/new.json");
				service.signRequest(accessToken, request);
				request.setPayload(eventObj.encode());

				// Sign request and execute it
				try (Response response = service.execute(request)) {
					Area.getLogger().info(response.getBody());
					result.handle(response.isSuccessful());
				} catch (Exception err) {
					err.printStackTrace();
					result.handle(false);
				}
			} catch (Exception err) {
				err.printStackTrace();
				result.handle(false);
			}
		});
	}
}
