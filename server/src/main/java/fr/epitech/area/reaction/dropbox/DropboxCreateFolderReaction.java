package fr.epitech.area.reaction.dropbox;

import fr.epitech.area.Area;
import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.reaction.ReactionExecutor;
import fr.epitech.area.service.ServiceExecutor;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;

public class DropboxCreateFolderReaction extends ReactionExecutor {

	public DropboxCreateFolderReaction(ServiceExecutor parentService, int id) {
		super(parentService, id,
			"dropbox_create_folder",
			"Create folder on dropbox"
		);
		this.getRequiredConfig().put("path", "Folder path");
	}

	@Override
	public void runReaction(UserModel user, JsonObject config, Handler<Boolean> result) {
		this.getParentService().getOauthData(user, userService -> {
			String url = "https://api.dropboxapi.com/2/files/create_folder_v2";
			WebClient webClient = WebClient.create(Area.getInstance().getVertx());
			JsonObject object = new JsonObject();

			object.put("path", config.getString("path"));
			object.put("autorename", "true");
			webClient.post(url)
				.putHeader("Authorization", "Bearer " + userService.getTokenObject().getString("token"))
				.putHeader("Content-Type", "application/json")
				.sendJson(object, ar -> {
					if (ar.succeeded()) {
						result.handle(true);
					} else {
						result.handle(false);
					}
				});
		});
	}
}
