package fr.epitech.area.reaction.spotify;

import fr.epitech.area.Area;
import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.reaction.ReactionExecutor;
import fr.epitech.area.service.ServiceExecutor;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;

public class SpotifyNextTrack extends ReactionExecutor {

    public SpotifyNextTrack(ServiceExecutor parentService, int id) {
        super(parentService, id,
			"spotify_next_track",
			"Change current track for the next in waiting list");
    }

    @Override
    public void runReaction(UserModel user, JsonObject config, Handler<Boolean> result) {
        this.getParentService().getOauthData(user, userService -> {
			WebClient webClient = WebClient.create(Area.getInstance().getVertx());
			webClient.post("https://api.spotify.com/v1/me/player/next")
				.ssl(true)
				.bearerTokenAuthentication(userService.getTokenObject().getString("token"))
				.send(ar -> result.handle(ar.succeeded()));
		});
    }
}
