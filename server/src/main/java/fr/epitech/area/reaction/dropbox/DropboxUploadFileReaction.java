package fr.epitech.area.reaction.dropbox;

import fr.epitech.area.Area;
import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.reaction.ReactionExecutor;
import fr.epitech.area.service.ServiceExecutor;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.multipart.MultipartForm;

public class DropboxUploadFileReaction extends ReactionExecutor {

	public DropboxUploadFileReaction(ServiceExecutor parentService, int id) {
		super(parentService, id,
			"dropbox_upload_file",
			"Upload jpeg file on dropbox"
		);
		this.getRequiredConfig().put("name", "Name of the file");
		this.getRequiredConfig().put("description", "Description of the file");
		this.getRequiredConfig().put("path", "Path of the file");
	}

	@Override
	public void runReaction(UserModel user, JsonObject config, Handler<Boolean> result) {
		this.getParentService().getOauthData(user, userService -> {
			String url = "https://content.dropboxapi.com/2/files/upload";
			WebClient webClient = WebClient.create(Area.getInstance().getVertx());
			MultipartForm multipartForm = MultipartForm.create();
			multipartForm.binaryFileUpload(config.getString("name"), config.getString("description"),
					"", "image/jpeg");

			JsonObject object = new JsonObject();
			object.put("path", config.getString("path"));
			object.put("mode", "add");
			object.put("autorename", "true");
			object.put("mute", "false");
			object.put("strict_conflict", "false");

			webClient.post(url)
				.putHeader("Authorization", "Bearer " + userService.getTokenObject().getString("token"))
				.putHeader("Dropbox-API-Arg", object.toString())
				.putHeader("Content-Type", "application/octet-stream")
				.sendMultipartForm(multipartForm, ar -> {
					if (ar.succeeded()) {
						result.handle(true);
					} else {
						result.handle(false);
					}
				});
		});
	}
}
