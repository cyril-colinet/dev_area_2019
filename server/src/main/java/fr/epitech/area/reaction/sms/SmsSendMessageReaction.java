package fr.epitech.area.reaction.sms;

import fr.epitech.area.Area;
import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.reaction.ReactionExecutor;
import fr.epitech.area.service.ServiceExecutor;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;

public class SmsSendMessageReaction extends ReactionExecutor {


    public SmsSendMessageReaction(ServiceExecutor parentService, int id) {
        super(parentService, id,
			"sms_send",
			"Send sms to a specific number with custom message");
        this.getRequiredConfig().put("phone_number", "Tel number with country code (+33...)");
        this.getRequiredConfig().put("message", "Message of the text message");
    }

    @Override
    public void runReaction(UserModel user, JsonObject config, Handler<Boolean> result) {
        String url = "https://rest.nexmo.com/sms/json";
        WebClient webClient = WebClient.create(Area.getInstance().getVertx());
        JsonObject form = new JsonObject()
			.put("api_key", this.getParentService().getClientId())
        	.put("api_secret", this.getParentService().getClientSecret())
        	.put("to", config.getString("phone_number"))
        	.put("from", "AREA")
        	.put("text", "[AREA Reaction] : " + config.getString("message"));
        webClient.post(url).sendJsonObject(form, ar -> {
        	Area.getLogger().info(ar.toString());
        	result.handle(ar.succeeded());
		});

    }
}
