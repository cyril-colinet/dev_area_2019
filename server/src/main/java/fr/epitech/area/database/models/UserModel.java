package fr.epitech.area.database.models;

import lombok.Getter;

public class UserModel {

	@Getter
	private int id;

	@Getter
	private String email;

	@Getter
	private String password;

	@Getter
	private String recoveryToken;

	@Getter
	private boolean verified;

	public UserModel(int id, String email, String password, String recoveryToken, int isVerified) {
		this.id = id;
		this.email = email;
		this.password = password;
		this.recoveryToken = recoveryToken;
		this.verified = isVerified == 1;
	}

	public UserModel(String email, String password) {
		this(-1, email, password, null, 0);
	}

}
