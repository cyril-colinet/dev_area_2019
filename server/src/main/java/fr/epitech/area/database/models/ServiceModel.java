package fr.epitech.area.database.models;

import fr.epitech.area.utils.UserService;
import io.vertx.core.json.JsonObject;
import lombok.Getter;

public class ServiceModel {

	@Getter
	private int entryId;

	@Getter
	private int userId;

	@Getter
	private int id;

	@Getter
	private JsonObject data;

	@Getter
	private String uniqueState;

	public ServiceModel(int entryId, int userId, int id, UserService userService) {
		this.entryId = entryId;
		this.userId = userId;
		this.id = id;
		this.data = userService.getTokenObject();
		this.uniqueState = userService.getUniqueState();
	}

	public ServiceModel(int id, int userId, UserService userService) {
		this(-1, userId, id, userService);
	}
}
