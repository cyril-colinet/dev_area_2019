package fr.epitech.area.database;

import fr.epitech.area.database.tables.AreaTable;
import fr.epitech.area.database.tables.ServicesTable;
import fr.epitech.area.database.tables.UsersTable;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.sql.ResultSet;
import io.vertx.ext.sql.SQLClient;
import io.vertx.ext.sql.SQLConnection;
import io.vertx.ext.sql.UpdateResult;
import lombok.Getter;

public class Database {

	/**
	 * SQL Client of request to SQL.
	 */
	@Getter
	private SQLClient sqlClient;

	/**
	 * Users table manager.
	 */
	@Getter
	private UsersTable usersTable;

	/**
	 * Services table manager.
	 */
	@Getter
	private ServicesTable servicesTable;

	/**
	 * Actions table manager;
	 */
	@Getter
	private AreaTable areaTable;

	/**
	 * Main Database manager for project.
	 *
	 * @param sqlClient
	 */
	public Database(SQLClient sqlClient) {
		this.sqlClient = sqlClient;
		this.usersTable = new UsersTable(this);
		this.servicesTable = new ServicesTable(this);
		this.areaTable = new AreaTable(this);
	}

	/**
	 * Query Sql without params.
	 *
	 * @param request
	 * @param handler
	 */
	public void query(String request, Handler<AsyncResult<ResultSet>> handler) {
		sqlClient.getConnection(res -> {
			if (res.succeeded()) {
				SQLConnection connection = res.result();
				connection.query(request, handler);
				connection.close();
			}
		});
	}

	/**
	 * Query sql with params.
	 *
	 * @param request
	 * @param params
	 * @param handler
	 */
	public void query(String request, JsonArray params, Handler<AsyncResult<ResultSet>> handler) {
		sqlClient.getConnection(res -> {
			if (res.succeeded()) {
				SQLConnection connection = res.result();
				connection.queryWithParams(request, params, handler);
				connection.close();
			}
		});
	}

	/**
	 * Update sql request with params.
	 *
	 * @param request
	 * @param params
	 * @param handler
	 */
	public void execute(String request, JsonArray params, Handler<AsyncResult<UpdateResult>> handler) {
		sqlClient.getConnection(res -> {
			if (res.succeeded()) {
				SQLConnection connection = res.result();
				connection.updateWithParams(request, params, handler);
				connection.close();
			}
		});
	}

	/**
	 * Update sql request without params.
	 *
	 * @param request
	 * @param handler
	 */
	public void execute(String request, Handler<AsyncResult<UpdateResult>> handler) {
		sqlClient.getConnection(res -> {
			if (res.succeeded()) {
				SQLConnection connection = res.result();
				connection.update(request, handler);
				connection.close();
			}
		});
	}

}
