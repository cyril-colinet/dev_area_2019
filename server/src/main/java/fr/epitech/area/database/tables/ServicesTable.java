package fr.epitech.area.database.tables;

import fr.epitech.area.database.Database;
import fr.epitech.area.database.models.ServiceModel;
import fr.epitech.area.utils.UserService;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import lombok.Getter;

public class ServicesTable {

	@Getter
	private Database database;

	public ServicesTable(Database database) {
		this.database = database;
		this.getDatabase().execute("CREATE TABLE `users_services` (\n" +
			"  `id` int(11) NOT NULL AUTO_INCREMENT,\n" +
			"  `user_id` int(11) NOT NULL,\n" +
			"  `service_id` int(11) NOT NULL,\n" +
			"  `service_data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,\n" +
			"  `unique_state` varchar(255) DEFAULT NULL,\n" +
			"  PRIMARY KEY (`id`)\n" +
			") ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1\n" +
			"\n", res -> {
		});
	}

	public void getService(int userId, int serviceId, Handler<ServiceModel> callback) {
		this.getDatabase().query("SELECT * FROM users_services WHERE user_id = ? AND service_id = ?",
			new JsonArray()
				.add(userId)
				.add(serviceId),
			result -> {
				if (result.result() == null || result.result().getRows().size() == 0) {
					callback.handle(null);
					return;
				}
				for (JsonObject row : result.result().getRows()) {
					callback.handle(new ServiceModel(
						row.getInteger("id"),
						row.getInteger("user_id"),
						row.getInteger("service_id"),
						new UserService(
							new JsonObject(row.getString("service_data")),
							row.getString("unique_state")
						)
					));
				}
			}
		);
	}

	public void insertNewService(ServiceModel service, Handler<Boolean> callback) {
		this.getDatabase().execute("INSERT INTO users_services SET user_id = ?, service_id = ?, service_data = ?, unique_state = ?",
			new JsonArray()
				.add(service.getUserId())
				.add(service.getId())
				.add(service.getData().encode())
				.add(service.getUniqueState()),
			res -> callback.handle(res.succeeded())
		);
	}

}
