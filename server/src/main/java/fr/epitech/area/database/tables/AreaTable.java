package fr.epitech.area.database.tables;

import fr.epitech.area.database.Database;
import fr.epitech.area.database.models.AreaModel;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public class AreaTable {

	@Getter
	private Database database;

	public AreaTable(Database database) {
		this.database = database;
		this.getDatabase().execute("CREATE TABLE `users_area` (\n" +
			"  `id` int(11) NOT NULL AUTO_INCREMENT,\n" +
			"  `user_id` int(11) NOT NULL,\n" +
			"  `action_id` int(11) NOT NULL,\n" +
			"  `action_data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,\n" +
			"  `reaction_id` int(11) NOT NULL,\n" +
			"  `reaction_data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,\n" +
			"  PRIMARY KEY (`id`)\n" +
			") ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1\n" +
			"\n", res -> {
		});
	}

	public void getAllAreas(int actionId, Handler<List<AreaModel>> handler) {
		this.getDatabase().query("SELECT * FROM users_area WHERE action_id = ?",
			new JsonArray().add(actionId),
			result -> {
				if (result.result() == null || result.result().getNumRows() == 0) {
					handler.handle(new ArrayList<>());
					return;
				}
				List<JsonObject> rows = result.result().getRows();
				ArrayList<AreaModel> users = new ArrayList<>();
				for (JsonObject row : rows) {
					users.add(new AreaModel(row));
				}
				handler.handle(users);
			}
		);
	}

	public void getUserAreas(int userId, Handler<List<AreaModel>> handler) {
		this.getDatabase().query("SELECT * FROM users_area WHERE user_id = ?",
			new JsonArray().add(userId),
			result -> {
				if (result.result() == null || result.result().getNumRows() == 0) {
					handler.handle(new ArrayList<>());
					return;
				}

				List<JsonObject> rows = result.result().getRows();
				ArrayList<AreaModel> users = new ArrayList<>();
				for (JsonObject row : rows) {
					users.add(new AreaModel(row));
				}
				handler.handle(users);
			}
		);
	}

	public void insertNewArea(int userId, AreaModel area, Handler<Boolean> callback) {
		this.getDatabase().execute("INSERT INTO users_area SET user_id = ?, action_id = ?, action_data = ?, reaction_id = ?, reaction_data = ?",
			new JsonArray()
				.add(userId)
				.add(area.getActionId())
				.add(area.getActionData().encode())
				.add(area.getReactionId())
				.add(area.getReactionData().encode()),
			res -> callback.handle(res.succeeded())
		);
	}

}
