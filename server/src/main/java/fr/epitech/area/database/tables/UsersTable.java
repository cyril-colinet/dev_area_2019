package fr.epitech.area.database.tables;

import fr.epitech.area.database.Database;
import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.utils.BCrypt;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public class UsersTable {

	@Getter
	private Database database;

	public UsersTable(Database database) {
		this.database = database;
		this.getDatabase().execute("CREATE TABLE `users` (\n"
			+ "  `user_id` int(11) NOT NULL,\n"
			+ "  `email` varchar(255) NOT NULL,\n"
			+ "  `password` varchar(255) DEFAULT NULL,\n"
			+ "  `recovery_token` varchar(255) DEFAULT NULL,\n"
			+ "  `is_verified` tinyint(3) UNSIGNED NOT NULL DEFAULT 0\n"
			+ ") ENGINE=InnoDB DEFAULT CHARSET=latin1;", res -> {
		});
	}

	public void getUser(String email, Handler<UserModel> user) {
		this.getDatabase().query("SELECT * FROM users WHERE email = ?", new JsonArray().add(email), res -> {
			if (res.result() == null || res.result().getNumRows() == 0) {
				user.handle(null);
				return;
			}
			for (JsonObject row : res.result().getRows()) {
				user.handle(new UserModel(
					row.getInteger("user_id"),
					row.getString("email"),
					row.getString("password"),
					row.getString("recovery_token"),
					row.getInteger("is_verified")
				));
			}
		});
	}

	public void getUser(int id, Handler<UserModel> user) {
		this.getDatabase().query("SELECT * FROM users WHERE user_id = ?", new JsonArray().add(id), res -> {
			if (res.result() == null || res.result().getNumRows() == 0) {
				user.handle(null);
				return;
			}
			for (JsonObject row : res.result().getRows()) {
				user.handle(new UserModel(
					row.getInteger("user_id"),
					row.getString("email"),
					row.getString("password"),
					row.getString("recovery_token"),
					row.getInteger("is_verified")
				));
			}
		});
	}

	public void getAllUsers(Handler<List<UserModel>> handler) {
		this.getDatabase().query("SELECT * FROM users", res -> {
			List<JsonObject> rs = res.result().getRows();
			ArrayList<UserModel> users = new ArrayList<>();
			for (JsonObject entries : rs) {
				users.add(new UserModel(
					entries.getInteger("user_id"),
					entries.getString("email"),
					entries.getString("password"),
					entries.getString("recovery_token"),
					entries.getInteger("is_verified")
				));
			}
			handler.handle(users);
		});
	}

	public void setRecoveryToken(UserModel user, String recoveryToken, Handler<Boolean> handler) {
		this.getDatabase().execute("UPDATE users SET recovery_token = ? WHERE user_id = ?",
			new JsonArray()
				.add(recoveryToken)
				.add(user.getId()),
			res -> handler.handle(res.succeeded())
		);
	}

	public void resetPassword(String recoveryToken, String newPassword, Handler<Boolean> handler) {
		this.getDatabase().execute("UPDATE users SET password = ?, recovery_token = NULL WHERE recovery_token = ?",
			new JsonArray()
				.add(BCrypt.hashpw(newPassword, BCrypt.gensalt()))
				.add(recoveryToken),
			res -> handler.handle(res.succeeded())
		);
	}

	public void insertNewUser(UserModel newUser, Handler<Boolean> confirm) {
		this.getDatabase().execute("INSERT INTO users SET email = ?, password = ?",
			new JsonArray()
				.add(newUser.getEmail())
				.add(newUser.getPassword()),
			res -> confirm.handle(res.succeeded())
		);
	}

}
