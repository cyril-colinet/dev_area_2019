package fr.epitech.area.database.models;

import io.vertx.core.json.JsonObject;
import lombok.Getter;

public class AreaModel {

	@Getter
	public int id;

	@Getter
	public int userId;

	@Getter
	private int actionId;

	@Getter
	private JsonObject actionData;

	@Getter
	private int reactionId;

	@Getter
	private JsonObject reactionData;

	public AreaModel(int id, int userId, int actionId, JsonObject actionData,
					 int reactionId, JsonObject reactionData) {
		this.id = id;
		this.userId = userId;
		this.actionId = actionId;
		this.actionData = actionData;
		this.reactionId = reactionId;
		this.reactionData = reactionData;
	}

	public AreaModel(int userId, int actionId, JsonObject actionData,
					 int reactionId, JsonObject reactionData) {
		this(-1, userId, actionId, actionData, reactionId, reactionData);
	}

	public AreaModel(JsonObject row) {
		this(row.getInteger("id"),
			row.getInteger("user_id"),
			row.getInteger("action_id"),
			new JsonObject(row.getString("action_data")),
			row.getInteger("reaction_id"),
			new JsonObject(row.getString("reaction_data"))
		);
	}
}
