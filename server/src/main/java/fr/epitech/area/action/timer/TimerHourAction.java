package fr.epitech.area.action.timer;

import fr.epitech.area.action.ActionExecutor;
import fr.epitech.area.action.ActionType;
import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.service.ServiceExecutor;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;

import java.time.LocalDateTime;
import java.time.ZoneId;

public class TimerHourAction extends ActionExecutor {

	public TimerHourAction(ServiceExecutor parentService, int id) {
		super(parentService, id,
			"at_exact_hour",
			"When time is equal to given hours and minutes",
			ActionType.TASK, 1
		);
		this.getRequiredConfig().put("hours", "Hours of the day (as number)");
		this.getRequiredConfig().put("minutes", "Minutes of the day (as number)");
	}

	@Override
	public void onCheckAction(UserModel user, JsonObject config, Handler<Boolean> result) {
		int hours = Integer.parseInt(config.getString("hours"));
		int minutes = Integer.parseInt(config.getString("minutes"));
		LocalDateTime now = LocalDateTime.now(ZoneId.of("Europe/Paris"));

		result.handle(now.getHour() == hours && now.getMinute() == minutes);
	}
}
