package fr.epitech.area.action.spotify;

import fr.epitech.area.Area;
import fr.epitech.area.action.ActionExecutor;
import fr.epitech.area.action.ActionType;
import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.service.ServiceExecutor;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;

import java.util.HashMap;
import java.util.Objects;

public class SpotifyNewFollower extends ActionExecutor {

	public HashMap<Integer, Integer> userLastFollowersCount = new HashMap<>();

	public SpotifyNewFollower(ServiceExecutor parentService, int id) {
		super(parentService, id,
			"spotify_new_follower",
			"Catch new follower count in your spotify account",
			ActionType.TASK, 1
		);
	}

	@Override
	public void onCheckAction(UserModel user, JsonObject config, Handler<Boolean> result) {
		this.getParentService().getOauthData(user, userService -> {
			WebClient client = WebClient.create(Area.getInstance().getVertx());
			client.get("https://api.spotify.com/v1/me")
				.ssl(true)
				.bearerTokenAuthentication(userService.getTokenObject().getString("token"))
				.send(ar -> {
					if (ar.succeeded()) {
						try {
							JsonObject body = ar.result().bodyAsJsonObject();
							Integer followers = Integer.parseInt(body.getJsonObject("followers").getString("total"));
							if (this.userLastFollowersCount.containsKey(user.getId())) {
								this.userLastFollowersCount.put(user.getId(), followers);
								result.handle(false);
								return;
							}
							if (!Objects.equals(this.userLastFollowersCount.get(user.getId()), followers))
								result.handle(true);
							result.handle(false);
							return;
						} catch (Exception ignored) {}
						result.handle(false);
					} else {
						result.handle(null);
					}
				});
		});
	}
}
