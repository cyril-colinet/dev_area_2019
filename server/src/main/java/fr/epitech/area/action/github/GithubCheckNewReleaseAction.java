package fr.epitech.area.action.github;

import fr.epitech.area.Area;
import fr.epitech.area.action.ActionExecutor;
import fr.epitech.area.action.ActionType;
import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.service.ServiceExecutor;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.codec.BodyCodec;

public class GithubCheckNewReleaseAction extends ActionExecutor {

    public GithubCheckNewReleaseAction(ServiceExecutor parentService, int id) {
        super(parentService, id,
                "gtihub_check_new_repository_release_action",
                "Github check for news repositories release action",
                ActionType.TASK, 1);
        this.getRequiredConfig().put("last_release_tag_name", "IDK");
        this.getRequiredConfig().put("repo_name", "Repository name");
        this.getRequiredConfig().put("repo_owner", "Repository owner");
    }

    @Override
    public void onCheckAction(UserModel user, JsonObject config, Handler<Boolean> result) {
        this.getParentService().getOauthData(user, userService -> {
            String url = "https://api.github.com/repos/" + config.getString("repo_owner") + "/"
                    + config.getString("repo_name") + "/releases";
            WebClient webClient = WebClient.create(Area.getInstance().getVertx());

            webClient.get(url)
                    .as(BodyCodec.jsonArray())
                    .putHeader("Authorization", userService.getTokenObject().getString("token"))
                    .send(ar -> {
                        if (ar.succeeded()) {
                            HttpResponse<JsonArray> response = ar.result();
                            JsonArray jsonObject = response.body();

                            if (jsonObject.size() == 0)
                                result.handle(false);

                            if (!jsonObject.getJsonObject(0).getString("tag_name").equals(config.getString("last_release_tag_name")))
                                result.handle(true);
                            else
                                result.handle(false);

                        } else {
                            result.handle(false);
                        }
                    });
        });
    }
}
