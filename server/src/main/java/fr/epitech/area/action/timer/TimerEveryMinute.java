package fr.epitech.area.action.timer;

import fr.epitech.area.action.ActionExecutor;
import fr.epitech.area.action.ActionType;
import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.service.ServiceExecutor;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;

public class TimerEveryMinute extends ActionExecutor {

	public TimerEveryMinute(ServiceExecutor parentService, int id) {
		super(parentService, id,
			"at_avery_minutes",
			"Every minutes of the current hour",
			ActionType.TASK, 1
		);
	}

	@Override
	public void onCheckAction(UserModel user, JsonObject config, Handler<Boolean> result) {
		result.handle(true);
	}
}
