package fr.epitech.area.action.github;

import fr.epitech.area.Area;
import fr.epitech.area.action.ActionExecutor;
import fr.epitech.area.action.ActionType;
import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.service.ServiceExecutor;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.codec.BodyCodec;

import java.util.HashMap;

public class GithubCheckNewRepositoryAction extends ActionExecutor {

	private final HashMap<Integer, Integer> lastRepoSizeById = new HashMap<>();

    public GithubCheckNewRepositoryAction(ServiceExecutor parentService, int id) {
        super(parentService, id,
			"gtihub_check_new_repository_action",
			"Check for new repository or a delete repository",
			ActionType.TASK, 1
		);
    }

    @Override
    public void onCheckAction(UserModel user, JsonObject config, Handler<Boolean> result) {
        this.getParentService().getOauthData(user, userService -> {
            WebClient webClient = WebClient.create(Area.getInstance().getVertx());

            webClient.get("https://api.github.com/user/repos")
                    .as(BodyCodec.jsonArray())
                    .putHeader("Authorization", userService.getTokenObject().getString("token"))
                    .send(ar -> {
                        if (ar.succeeded()) {
                            HttpResponse<JsonArray> response = ar.result();
                            JsonArray jsonObject = response.body();

                            // If exists, fill
							if (!this.lastRepoSizeById.containsKey(user.getId()))
								this.lastRepoSizeById.put(user.getId(), jsonObject.size());

							// Ya
                            result.handle(jsonObject.size() != this.lastRepoSizeById.get(user.getId()));
                        } else {
                            result.handle(false);
                        }
                    });
        });
    }
}
