package fr.epitech.area.action;

import fr.epitech.area.Area;
import fr.epitech.area.database.models.AreaModel;
import fr.epitech.area.reaction.ReactionExecutor;
import fr.epitech.area.service.ServiceExecutor;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class AreaActions {

	private static ScheduledExecutorService scheduledExecutorService;

	static {
		scheduledExecutorService = Executors.newScheduledThreadPool(Runtime.getRuntime().availableProcessors());
	}

	public static void initTasksActions() {

		for (ServiceExecutor service : Area.getServicesManager().getServices()) {
			for (ActionExecutor action : service.getActions()) {
				if (!action.getType().equals(ActionType.TASK)) {
					continue;
				}
				scheduledExecutorService.scheduleWithFixedDelay(() -> {
					Area.getDatabase().getAreaTable().getAllAreas(action.getId(), res -> {
						if (res == null) {
							return;
						}
						for (AreaModel area : res) {
							Area.getDatabase().getUsersTable().getUser(area.getUserId(), user -> {
								if (user == null)
									return;

								action.onCheckAction(user, area.getActionData(), checked -> {
									if (checked) {
										ReactionExecutor reaction = Area.getServicesManager().getReaction(area.getReactionId());
										reaction.runReaction(user, area.getReactionData(), result -> {
											if (result)
												Area.getLogger().info("Action " + action.getName() + " -> Reaction " + reaction.getName() + " : successful");
											else
												Area.getLogger().info("Action " + action.getName() + " -> Reaction " + reaction.getName() + " : failure");
										});
									}
								});
							});
						}
					});
				}, 0, action.getRefreshMinutesRate(), TimeUnit.MINUTES);
			}
		}
	}

}
