package fr.epitech.area.action.gmail;

import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth20Service;
import com.github.scribejava.core.oauth.OAuthService;
import fr.epitech.area.Area;
import fr.epitech.area.action.ActionExecutor;
import fr.epitech.area.action.ActionType;
import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.service.ServiceExecutor;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.codec.BodyCodec;

public class GmailNewMailAction extends ActionExecutor {

    public GmailNewMailAction(ServiceExecutor parentService, int id) {
        super(parentService, id,
                "gmail_check_new_mail_arrivals",
                "Check for new mail arrivals",
                ActionType.TASK, 1);
        this.getRequiredConfig().put("user_id", "UserId of the google account");
        this.getRequiredConfig().put("last_id", "Last mail indentifier");
    }

    @Override
    public void onCheckAction(UserModel user, JsonObject config, Handler<Boolean> result) {
		ServiceExecutor googleService = this.getParentService();
		googleService.getOauthData(user, userService -> {
			OAuthService oauthService = googleService.getService(null, userService.getUniqueState());
			if (!(oauthService instanceof OAuth20Service)) {
				result.handle(false);
				return;
			}

			// Cast service to current
			OAuth20Service service = ((OAuth20Service) oauthService);

			// Create request
			OAuthRequest request = new OAuthRequest(Verb.GET, "https://api.twitter.com/1.1/direct_messages/events/list.json?count=2");
			OAuth2AccessToken accessToken = new OAuth2AccessToken(userService.getTokenObject().getString("token"));

			// Sign request and execute it
			/*service.signRequest(accessToken, request);
			try (Response response = service.execute(request)) {
				JsonObject resultObj = new JsonObject(response.getBody());
				if (!resultObj.containsKey("events")) {
					result.handle(false);
					return;
				}

				JsonObject firstEvent = resultObj.getJsonArray("events").getJsonObject(0);
				result.handle(!this.userLastEventId.getOrDefault(user.getId(), "")
					.equalsIgnoreCase(firstEvent.getString("id")));
				this.userLastEventId.put(user.getId(), firstEvent.getString("id"));
			} catch (Exception err) {
				err.printStackTrace();
				result.handle(false);
			}*/
		});



        this.getParentService().getOauthData(user, json -> {
            String url = "https://www.googleapis.com/gmail/v1/users/" + config.getString("user_id") + "/messages";
            WebClient webClient = WebClient.create(Area.getInstance().getVertx());

            //TODO Auth token in headers ?

            webClient.get(url)
                    .as(BodyCodec.jsonObject())
                    .send(ar -> {
                        if (ar.succeeded()) {
                            HttpResponse<JsonObject> response = ar.result();
                            JsonObject jsonObject = response.body();
                            String id = jsonObject.getJsonArray("messages").getJsonObject(0).getString("id");

                            if (!id.equals(config.getString("last_id")))
                                result.handle(true);
                            else
                                result.handle(false);

                        } else {
                            result.handle(false);
                        }
                    });
        });
    }
}
