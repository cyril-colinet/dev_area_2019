package fr.epitech.area.action.instagram;

import fr.epitech.area.action.ActionExecutor;
import fr.epitech.area.action.ActionType;
import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.service.ServiceExecutor;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;

public class InstagramNewDM extends ActionExecutor {

	public InstagramNewDM(ServiceExecutor parentService, int id) {
		super(parentService, id,
			"instagram_new_dm",
			"A new direct message was received from your accountg",
			ActionType.TASK, 1
		);
	}

	@Override
	public void onCheckAction(UserModel user, JsonObject config, Handler<Boolean> result) {
		/*this.getParentService().getOauthData(user, userService -> {
			String token = userService.getTokenObject().getString("token");
			String hashtag = config.getString("instagram_hashtag");
			String userId = config.getString("instagram_user_id");
			String url = "https://graph.facebook.com/v6.0/ig_hashtag_search?user_id=" + userId
					+ "&q=" + hashtag + "&access_token=" + token;
			WebClient webClient = WebClient.create(Area.getInstance().getVertx());

			webClient.get(url)
					.as(BodyCodec.jsonObject())
					.send(ar -> {
						if (ar.succeeded()) {
							HttpResponse<JsonObject> response = ar.result();
							JsonObject jsonObject = response.body();
							String hashtagId = null;

							try {
								hashtagId = jsonObject.getJsonArray("data").getJsonObject(0).getString("id");
							} catch (Exception e) {
								System.out.println(e.getMessage());
								result.handle(false);
								return;
							}

							String apiUrl = "https://graph.facebook.com/v6.0/" + hashtagId + "/recent_media?user_id="
									+ userId + "&fields=id,media_type,comments_count,like_count,media_url,permalink";
							webClient.get(apiUrl)
									.as(BodyCodec.jsonObject())
									.send(res -> {
										if (res.succeeded()) {
											HttpResponse<JsonObject> responseApi = res.result();
											JsonObject jsonObjectApi = responseApi.body();

											result.handle(!jsonObjectApi.getJsonArray("data").getJsonObject(0).getString("id").equals(config.getString("instagram_hashtag_last_id")));
										} else {
											result.handle(false);
										}

									});
						} else {
							result.handle(false);
						}
					});
		});*/
		result.handle(false);
	}
}
