package fr.epitech.area.action.timer;

import fr.epitech.area.action.ActionType;
import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.service.ServiceExecutor;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import fr.epitech.area.action.ActionExecutor;

import java.time.LocalDateTime;
import java.time.ZoneId;

public class TimerDayAction extends ActionExecutor {

	public TimerDayAction(ServiceExecutor parentService, int id) {
		super(parentService, id,
			"at_exact_day",
			"When the day is equals to the value",
			ActionType.TASK, 1
		);
		this.getRequiredConfig().put("day", "Day of the month (as number)");
	}

	@Override
	public void onCheckAction(UserModel user, JsonObject config, Handler<Boolean> result) {
		int day = Integer.parseInt(config.getString("day"));
		LocalDateTime now = LocalDateTime.now(ZoneId.of("Europe/Paris"));

		result.handle(now.getDayOfWeek().getValue() == day);
	}
}
