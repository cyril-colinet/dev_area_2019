package fr.epitech.area.action;

import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.service.ServiceExecutor;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import lombok.Getter;

import java.util.HashMap;

public abstract class ActionExecutor {

	@Getter
	private int id;

	@Getter
	private ActionType type;

	@Getter
	private String name;

	@Getter
	private String description;

	@Getter
	private int refreshMinutesRate;

	@Getter
	private ServiceExecutor parentService;

	@Getter
 	private HashMap<String, String> requiredConfig = new HashMap<>();

	public ActionExecutor(ServiceExecutor parentService, int id, String name, String description, ActionType type, int refreshMinutesRate) {
		this.parentService = parentService;
		this.id = id;
		this.name = name;
		this.description = description;
		this.type = type;
		this.refreshMinutesRate = refreshMinutesRate;
	}

	public ActionExecutor(ServiceExecutor parentService, int id, String name, String description, ActionType type) {
		this(parentService, id, name, description, type, 1);
	}

	public JsonObject getRequireDataJson() {
		JsonObject json = new JsonObject();
		this.getRequiredConfig().forEach(json::put);
		return json;
	}

	public abstract void onCheckAction(UserModel user, JsonObject config, Handler<Boolean> result);

}
