package fr.epitech.area.action.news;

import fr.epitech.area.Area;
import fr.epitech.area.action.ActionExecutor;
import fr.epitech.area.action.ActionType;
import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.service.ServiceExecutor;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.codec.BodyCodec;

public class NewsNewCategorieTopHeadlineAction extends ActionExecutor {

	public NewsNewCategorieTopHeadlineAction(ServiceExecutor parentService, int id) {
		super(parentService, id,
			"news_new_top_headline_category_action",
			"Check for new top category headline",
			ActionType.TASK, 1
		);
        this.getRequiredConfig().put("categories", "Categories of the new (separated by spaces)");
	}

	@Override
	public void onCheckAction(UserModel user, JsonObject config, Handler<Boolean> result) {
		String url = "http://newsapi.org/v2/top-headlines?apiKey=" + this.getParentService().getApiKey();
		WebClient webClient = WebClient.create(Area.getInstance().getVertx());

		url += "&category=" + config.getString("categories");
		webClient.get(url)
				.as(BodyCodec.jsonObject())
				.send(ar -> {
					if (ar.succeeded()) {
						//HttpResponse<JsonObject> response = ar.result();
						//JsonObject jsonObject = response.body();

						result.handle(true);
					} else {
						result.handle(false);
					}
				});
	}
}
