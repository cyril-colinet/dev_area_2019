package fr.epitech.area.action.twitter;

import fr.epitech.area.action.ActionExecutor;
import fr.epitech.area.action.ActionType;
import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.service.ServiceExecutor;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;

public class TwitterNewFollower extends ActionExecutor {

	public TwitterNewFollower(ServiceExecutor parentService, int id) {
		super(parentService, id,
			"new_follower",
			"The followers amount of your account was increased",
			ActionType.TASK, 1
		);
	}

	@Override
	public void onCheckAction(UserModel user, JsonObject config, Handler<Boolean> result) {
		result.handle(true);
	}
}
