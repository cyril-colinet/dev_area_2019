package fr.epitech.area.action.news;

import fr.epitech.area.Area;
import fr.epitech.area.action.ActionExecutor;
import fr.epitech.area.action.ActionType;
import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.service.ServiceExecutor;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.codec.BodyCodec;

public class NewsNewCountryTopHeadlineAction extends ActionExecutor {

	public NewsNewCountryTopHeadlineAction(ServiceExecutor parentService, int id) {
		super(parentService, id,
			"news_new_top_headline_country_action",
			"Check for new top country headline",
			ActionType.TASK, 1
		);
        this.getRequiredConfig().put("country", "Country of the new");
	}

	@Override
	public void onCheckAction(UserModel user, JsonObject config, Handler<Boolean> result) {
		String url = "http://newsapi.org/v2/top-headlines?apiKey=" + this.getParentService().getApiKey();
		WebClient webClient = WebClient.create(Area.getInstance().getVertx());

		url += "&country=" + config.getString("country");
		webClient.get(url)
                .as(BodyCodec.jsonObject())
				.send(ar -> {
					if (ar.succeeded()) {
                        HttpResponse<JsonObject> response = ar.result();
                        JsonObject jsonObject = response.body();

                        if (!jsonObject.getJsonArray("articles").getJsonObject(0)
                                .getString("publishedAt").equals(config.getString("news_country_published_at"))) {
                            result.handle(true);
                        }
					} else {
						result.handle(false);
					}
				});
	}
}
