package fr.epitech.area.action.twitter;

import com.github.scribejava.core.model.OAuth1AccessToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth10aService;
import com.github.scribejava.core.oauth.OAuthService;
import fr.epitech.area.action.ActionExecutor;
import fr.epitech.area.action.ActionType;
import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.service.ServiceExecutor;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;

import java.util.HashMap;

public class TwitterNewDirectMessage extends ActionExecutor {

	public HashMap<Integer, String> userLastEventId = new HashMap<>();

	public TwitterNewDirectMessage(ServiceExecutor parentService, int id) {
		super(parentService, id,
			"new_dm",
			"New direct message in twitter account",
			ActionType.TASK, 1
		);
	}

	@Override
	public void onCheckAction(UserModel user, JsonObject config, Handler<Boolean> result) {
		ServiceExecutor twitterService = this.getParentService();
		twitterService.getOauthData(user, userService -> {
			OAuthService oauthService = twitterService.getService(null, userService.getUniqueState());
			if (!(oauthService instanceof OAuth10aService)) {
				result.handle(false);
				return;
			}

			// Cast service to current
			OAuth10aService service = ((OAuth10aService) oauthService);

			// Create request
			JsonObject tokenObj = userService.getTokenObject();
			OAuthRequest request = new OAuthRequest(Verb.GET, "https://api.twitter.com/1.1/direct_messages/events/list.json?count=2");
			OAuth1AccessToken accessToken = new OAuth1AccessToken(tokenObj.getString("token"), tokenObj.getString("token_secret"));

			// Sign request and execute it
			service.signRequest(accessToken, request);
			try (Response response = service.execute(request)) {
				JsonObject resultObj = new JsonObject(response.getBody());
				if (!resultObj.containsKey("events")) {
					result.handle(false);
					return;
				}

				JsonObject firstEvent = resultObj.getJsonArray("events").getJsonObject(0);
				result.handle(!this.userLastEventId.getOrDefault(user.getId(), "")
					.equalsIgnoreCase(firstEvent.getString("id")));
				this.userLastEventId.put(user.getId(), firstEvent.getString("id"));
			} catch (Exception err) {
				err.printStackTrace();
				result.handle(false);
			}
		});
	}
}
