package fr.epitech.area.action.timer;

import fr.epitech.area.action.ActionExecutor;
import fr.epitech.area.action.ActionType;
import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.service.ServiceExecutor;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;

import java.time.LocalDateTime;

public class TimerXBeforeAction extends ActionExecutor {

	public TimerXBeforeAction(ServiceExecutor parentService, int id) {
		super(parentService, id,
			"x_days_before_y",
			"Amount of days before the day in value",
			ActionType.TASK, 1
		);
		this.getRequiredConfig().put("day_x", "Amount of days");
		this.getRequiredConfig().put("day_y", "Prediction day in amount days");
	}

	@Override
	public void onCheckAction(UserModel user, JsonObject config, Handler<Boolean> result) {
		int dayToTarget = Integer.parseInt(config.getString("day_y"));
		int dayToGo = Integer.parseInt(config.getString("day_x"));
		LocalDateTime now = LocalDateTime.now();

		result.handle((now.getDayOfWeek().getValue() + dayToGo) == dayToTarget);
	}
}
