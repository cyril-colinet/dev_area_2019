package fr.epitech.area.action.spotify;

import fr.epitech.area.Area;
import fr.epitech.area.action.ActionExecutor;
import fr.epitech.area.action.ActionType;
import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.service.ServiceExecutor;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;

public class SpotifyCurrentPlaying extends ActionExecutor {

	public SpotifyCurrentPlaying(ServiceExecutor parentService, int id) {
		super(parentService, id,
			"current_playing",
			"Current playing music contains value in the title",
			ActionType.TASK, 1
		);
		this.getRequiredConfig().put("title_contains", "Title contains the value");
	}

	@Override
	public void onCheckAction(UserModel user, JsonObject config, Handler<Boolean> result) {
		this.getParentService().getOauthData(user, userService -> {
			WebClient client = WebClient.create(Area.getInstance().getVertx());
			client.get("https://api.spotify.com/v1/me/player")
				.ssl(true)
				.bearerTokenAuthentication(userService.getTokenObject().getString("token"))
				.send(ar -> {
					if (ar.succeeded()) {
						try {
							JsonObject body = ar.result().bodyAsJsonObject();
							if (body.getJsonObject("item").getJsonObject("album")
								.getString("name").toLowerCase().contains(config.getString("title_contains").toLowerCase())) {
								result.handle(true);
							}
							result.handle(false);
							return;
						} catch (Exception ignored) {}
					}
					result.handle(false);
				});
		});
	}
}
