package fr.epitech.area.action.spotify;

import fr.epitech.area.action.ActionExecutor;
import fr.epitech.area.action.ActionType;
import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.service.ServiceExecutor;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;

public class SpotifyNewSongAddedToPlaylist extends ActionExecutor {

	public SpotifyNewSongAddedToPlaylist(ServiceExecutor parentService, int id) {
		super(parentService, id,
			"new_song_added_to_playlist",
			"New song added to playlist",
			ActionType.TASK, 1
		);
		this.getRequiredConfig().put("playlist_name", "Name of the spotify playlist to lookup");
	}

	@Override
	public void onCheckAction(UserModel user, JsonObject config, Handler<Boolean> result) {
		result.handle(true);
	}
}
