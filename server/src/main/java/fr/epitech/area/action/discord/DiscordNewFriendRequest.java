package fr.epitech.area.action.discord;

import fr.epitech.area.action.ActionExecutor;
import fr.epitech.area.action.ActionType;
import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.service.ServiceExecutor;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;

import java.time.LocalDateTime;

public class DiscordNewFriendRequest extends ActionExecutor {

	public DiscordNewFriendRequest(ServiceExecutor parentService, int id) {
		super(parentService, id,
			"new_friend_request",
			"Get new friend request by another user",
			ActionType.TASK, 1
		);
	}

	@Override
	public void onCheckAction(UserModel user, JsonObject config, Handler<Boolean> result) {
		int day = config.getInteger("timer_day");
		LocalDateTime now = LocalDateTime.now();

		result.handle(now.getDayOfWeek().getValue() == day);
	}
}
