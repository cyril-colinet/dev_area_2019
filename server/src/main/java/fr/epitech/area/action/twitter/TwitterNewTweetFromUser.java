package fr.epitech.area.action.twitter;

import fr.epitech.area.action.ActionExecutor;
import fr.epitech.area.action.ActionType;
import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.service.ServiceExecutor;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;

public class TwitterNewTweetFromUser extends ActionExecutor {

	public TwitterNewTweetFromUser(ServiceExecutor parentService, int id) {
		super(parentService, id,
			"new_tweet_from_user",
			"New tweet published from user",
			ActionType.TASK, 1
		);
		this.getRequiredConfig().put("user_at", "Username of the Twitter account (@)");
	}

	@Override
	public void onCheckAction(UserModel user, JsonObject config, Handler<Boolean> result) {
		result.handle(true);
	}
}
