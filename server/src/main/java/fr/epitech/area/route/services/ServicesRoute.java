package fr.epitech.area.route.services;

import fr.epitech.area.Area;
import fr.epitech.area.route.RouteExecutor;
import fr.epitech.area.route.RouteType;
import fr.epitech.area.service.ServiceExecutor;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

public class ServicesRoute extends RouteExecutor {

	public ServicesRoute() {
		super(RouteType.GET, "/services", false);
		this.setDescription("List all services");
	}

	@Override
	public void handling(RoutingContext context) {
		JsonArray response = new JsonArray();

		for (ServiceExecutor service : Area.getServicesManager().getServices()) {
			if (!service.isNeedOAuth()) {
				continue;
			}
			response.add(new JsonObject()
				.put("service_id", service.getId())
				.put("service_name", service.getName())
				.put("need_oauth", service.isNeedOAuth())
			);
		}

		response(context, new JsonObject()
			.put("result", "ok")
			.put("data", response));
	}
}
