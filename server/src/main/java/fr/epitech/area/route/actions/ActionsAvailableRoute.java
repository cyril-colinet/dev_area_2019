package fr.epitech.area.route.actions;

import fr.epitech.area.Area;
import fr.epitech.area.action.ActionExecutor;
import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.route.RouteExecutor;
import fr.epitech.area.route.RouteType;
import fr.epitech.area.service.ServiceExecutor;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

import java.util.ArrayList;
import java.util.Arrays;

public class ActionsAvailableRoute extends RouteExecutor {

	public ActionsAvailableRoute() {
		super(RouteType.GET, "/actions/available", true);
		this.setDescription("List all actions available");
	}

	@Override
	public void handling(RoutingContext context, UserModel user) {
		ArrayList<Integer> remainingServices = new ArrayList<>();
		JsonArray actionsJson = new JsonArray();

		for (ServiceExecutor service : Area.getServicesManager().getServices()) {
			JsonArray serviceJson = new JsonArray();
			if (service.isNeedOAuth()) {
				remainingServices.add(service.getId());
				Area.getDatabase().getServicesTable().getService(user.getId(), service.getId(), res -> {
					for (ActionExecutor action : service.getActions()) {
						serviceJson.add(new JsonObject()
							.put("id", action.getId())
							.put("name", action.getName())
							.put("description", action.getDescription())
							.put("need_oauth", true)
							.put("is_oauth", res != null)
							.put("required_config", action.getRequireDataJson())
						);
					}
					actionsJson.add(new JsonObject()
						.put("service_id", service.getId())
						.put("service_name", service.getName())
						.put("actions", serviceJson)
					);
					remainingServices.removeAll(Arrays.asList(service.getId()));
					if (remainingServices.size() == 0) {
						response(context, actionsJson);
					}
				});
				continue;
			}
			for (ActionExecutor action : service.getActions()) {
				serviceJson.add(new JsonObject()
					.put("id", action.getId())
					.put("name", action.getName())
					.put("description", action.getDescription())
					.put("need_oauth", false)
					.put("is_oauth", false)
					.put("required_config", action.getRequireDataJson())
				);
			}
			actionsJson.add(new JsonObject()
				.put("service_id", service.getId())
				.put("service_name", service.getName())
				.put("actions", serviceJson)
			);
			if (remainingServices.size() == 0) {
				response(context, actionsJson);
			}
		}
	}
}
