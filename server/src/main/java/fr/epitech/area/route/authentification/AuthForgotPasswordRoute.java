package fr.epitech.area.route.authentification;

import fr.epitech.area.Area;
import fr.epitech.area.route.ResponseError;
import fr.epitech.area.route.RouteExecutor;
import fr.epitech.area.route.RouteType;
import fr.epitech.area.utils.Email;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

import java.nio.charset.StandardCharsets;
import java.util.Random;

public class AuthForgotPasswordRoute extends RouteExecutor {

	public AuthForgotPasswordRoute() {
		super(RouteType.POST, "/password/forgot", false);
		this.addRequiredParam("email", String.class);
		this.setDescription("Send a mail with a password reset link");
	}

	@Override
	public void handling(RoutingContext context) {
		String email = context.getBodyAsJson().getString("email");

		Area.getDatabase().getUsersTable().getUser(email, user -> {
			if (user == null) {
				response(context, new ResponseError("Cannot found user in database"));
				return;
			}

			// Generate random token
			byte[] array = new byte[15];
			new Random().nextBytes(array);
			String recoveryToken = new String(array, StandardCharsets.UTF_8);

			// Insert recovery token to database
			Area.getDatabase().getUsersTable().setRecoveryToken(user, recoveryToken, success -> {
				if (!success) {
					response(context, new ResponseError("Unable to generate new password"));
					return;
				}

				// Send mail with link and token
				if (new Email()
					.setTo(email)
					.setSubject("Password Recovery - Epitech AREA")
					.setContent("Please reset your password by visiting <a href=\"https://area.epitech-nice.fr/api/password/reset?token=" + recoveryToken + "\">this link</a>.")
					.send()) {
					response(context, new JsonObject()
						.put("result", "success"));
					return;
				}

				response(context, new ResponseError("Unable to send mail"));
			});
		});
	}
}
