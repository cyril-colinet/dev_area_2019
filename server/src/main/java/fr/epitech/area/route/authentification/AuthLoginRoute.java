package fr.epitech.area.route.authentification;

import fr.epitech.area.Area;
import fr.epitech.area.route.ResponseError;
import fr.epitech.area.route.RouteExecutor;
import fr.epitech.area.route.RouteType;
import fr.epitech.area.utils.BCrypt;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

public class AuthLoginRoute extends RouteExecutor {

	public AuthLoginRoute() {
		super(RouteType.POST, "/login", false);
		this.addRequiredParam("email", String.class);
		this.addRequiredParam("password", String.class);
		this.setDescription("Login with user info and get JWT token");
	}

	@Override
	public void handling(RoutingContext context) {
		String email = context.getBodyAsJson().getString("email");
		String password = context.getBodyAsJson().getString("password");

		Area.getDatabase().getUsersTable().getUser(email, user -> {
			if (user == null) {
				response(context, new ResponseError("Cannot found user in database"));
				return;
			}

			if (!BCrypt.checkpw(password, user.getPassword())) {
				response(context, new ResponseError("Invalid credentials"));
				return;
			}

			if (!user.isVerified()) {
				response(context, new ResponseError("Account not verified! Check your emails !"));
				return;
			}

			String jwtToken = Area.getJwtAuth().generateToken(new JsonObject().put("email", email));

			response(context, new JsonObject()
				.put("result", "success")
				.put("token", jwtToken));
		});
	}

}
