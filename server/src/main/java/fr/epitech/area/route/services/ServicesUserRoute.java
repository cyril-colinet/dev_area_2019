package fr.epitech.area.route.services;

import fr.epitech.area.Area;
import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.route.RouteExecutor;
import fr.epitech.area.route.RouteType;
import fr.epitech.area.service.ServiceExecutor;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

import java.util.ArrayList;

public class ServicesUserRoute extends RouteExecutor {

	public ServicesUserRoute() {
		super(RouteType.GET, "/services/user", true);
		this.setDescription("List services available and check status");
	}

	@Override
	public void handling(RoutingContext context, UserModel user) {
		JsonArray response = new JsonArray();
		ArrayList<ServiceExecutor> remainingServices = new ArrayList<>();

		for (ServiceExecutor service : Area.getServicesManager().getServices()) {
			remainingServices.add(service);
			Area.getDatabase().getServicesTable().getService(user.getId(), service.getId(), res -> {
				response.add(new JsonObject()
					.put("service_id", service.getId())
					.put("service_name", service.getName())
					.put("need_oauth", service.isNeedOAuth())
					.put("is_oauth", res != null)
				);
				remainingServices.remove(service);
				if (remainingServices.size() == 0) {
					response(context, new JsonObject()
						.put("result", "ok")
						.put("data", response)
					);
				}
			});
		}
	}
}
