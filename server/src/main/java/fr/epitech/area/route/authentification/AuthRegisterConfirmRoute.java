package fr.epitech.area.route.authentification;

import fr.epitech.area.Area;
import fr.epitech.area.route.ResponseError;
import fr.epitech.area.route.RouteExecutor;
import fr.epitech.area.route.RouteType;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

public class AuthRegisterConfirmRoute extends RouteExecutor {

	public AuthRegisterConfirmRoute() {
		super(RouteType.GET, "/register/confirm", false);
		this.setDescription("Confirm account after register");
	}

	@Override
	public void handling(RoutingContext context) {
		String userEmail = context.request().getParam("user_email");

		Area.getDatabase().getUsersTable().getUser(userEmail, user -> {
			if (user == null) {
				response(context, new ResponseError("Error occurred #1"));
				return;
			}

			Area.getDatabase().execute("UPDATE users SET is_verified = 1 WHERE email = ?",
				new JsonArray()
					.add(userEmail),
				res -> {
					if (res.succeeded()) {
						response(context, new JsonObject()
							.put("result", "success"));
					} else {
						response(context, new ResponseError("Error occurred #2"));
					}
				});
		});
	}

}
