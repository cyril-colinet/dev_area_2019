package fr.epitech.area.route;

/**
 * Enum of all routes types.
 */
public enum RouteType {
    ALL,
    GET,
    POST,
    DELETE,
    PATCH;
}
