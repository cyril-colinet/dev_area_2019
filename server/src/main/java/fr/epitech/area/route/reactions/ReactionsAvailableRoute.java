package fr.epitech.area.route.reactions;

import fr.epitech.area.Area;
import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.reaction.ReactionExecutor;
import fr.epitech.area.route.RouteExecutor;
import fr.epitech.area.route.RouteType;
import fr.epitech.area.service.ServiceExecutor;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

import java.util.ArrayList;
import java.util.Arrays;

public class ReactionsAvailableRoute extends RouteExecutor {

	public ReactionsAvailableRoute() {
		super(RouteType.GET, "/reactions/available", true);
		this.setDescription("List all reactions available");
	}

	@Override
	public void handling(RoutingContext context, UserModel user) {
		ArrayList<Integer> remainingServices = new ArrayList<>();
		JsonArray reactionsJson = new JsonArray();

		for (ServiceExecutor service : Area.getServicesManager().getServices()) {
			JsonArray serviceJson = new JsonArray();
			if (service.isNeedOAuth()) {
				remainingServices.add(service.getId());
				Area.getDatabase().getServicesTable().getService(user.getId(), service.getId(), res -> {
					for (ReactionExecutor reaction : service.getReactions()) {
						serviceJson.add(new JsonObject()
								.put("id", reaction.getId())
								.put("name", reaction.getName())
								.put("description", reaction.getDescription())
								.put("need_oauth", true)
								.put("is_oauth", res != null)
								.put("required_config", reaction.getRequireDataJson())
						);
					}
					reactionsJson.add(new JsonObject()
						.put("service_id", service.getId())
						.put("service_name", service.getName())
						.put("reactions", serviceJson)
					);
					remainingServices.removeAll(Arrays.asList(service.getId()));
					if (remainingServices.size() == 0) {
						response(context, reactionsJson);
					}
				});
				continue;
			}
			for (ReactionExecutor reaction : service.getReactions()) {
				serviceJson.add(new JsonObject()
						.put("id", reaction.getId())
						.put("name", reaction.getName())
						.put("description", reaction.getDescription())
						.put("need_oauth", false)
						.put("is_oauth", false)
						.put("required_config", reaction.getRequireDataJson())
				);
			}
			reactionsJson.add(new JsonObject()
				.put("service_id", service.getId())
				.put("service_name", service.getName())
				.put("reactions", serviceJson)
			);
			if (remainingServices.size() == 0) {
				response(context, reactionsJson);
			}
		}
	}
}
