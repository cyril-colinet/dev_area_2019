package fr.epitech.area.route;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class ResponseError {

	private String message;

	private JsonArray messages;

	public ResponseError() {
	}

	/**
	 * Response error when error occurred.
	 *
	 * @param message
	 */
	public ResponseError(String message) {
		this.message = message;
	}

	/**
	 * Response error when error occurred.
	 *
	 * @param messages
	 */
	public ResponseError(JsonArray messages) {
		this.messages = messages;
	}

	public JsonObject toJson() {
		JsonObject jsonObject = new JsonObject()
			.put("result", "error")
			.put("help_route", "/api/help")
		;
		if (message != null) {
			jsonObject.put("message", this.message);
		}
		if (messages != null) {
			jsonObject.put("messages", this.messages);
		}
		return jsonObject;
	}

}
