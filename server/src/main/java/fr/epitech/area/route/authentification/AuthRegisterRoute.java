package fr.epitech.area.route.authentification;

import fr.epitech.area.Area;
import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.route.ResponseError;
import fr.epitech.area.route.RouteExecutor;
import fr.epitech.area.route.RouteType;
import fr.epitech.area.utils.BCrypt;
import fr.epitech.area.utils.Email;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

public class AuthRegisterRoute extends RouteExecutor {

	public AuthRegisterRoute() {
		super(RouteType.POST, "/register", false);
		this.addRequiredParam("email", String.class);
		this.addRequiredParam("password", String.class);
		this.setDescription("Register new user and get JWT token of session");
	}

	@Override
	public void handling(RoutingContext context) {
		String email = context.getBodyAsJson().getString("email");
		String password = BCrypt.hashpw(context.getBodyAsJson().getString("password"), BCrypt.gensalt());

		Area.getDatabase().getUsersTable().insertNewUser(new UserModel(email, password), confirm -> {
			if (confirm) {
				new Email()
					.setTo(email)
					.setSubject("Confirm account - Epitech AREA")
					.setContent("Confirm your email for login: <a href=\"https://area.epitech-nice.fr/api/register/confirm?user_email=" + email + "\">click here!</a>")
					.send();
				response(context, new JsonObject()
					.put("result", "success"));
			} else {
				response(context, new ResponseError("Unable to create new user"));
			}
		});
	}

}
