package fr.epitech.area.route.authentification;

import fr.epitech.area.Area;
import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.route.ResponseError;
import fr.epitech.area.route.RouteExecutor;
import fr.epitech.area.route.RouteType;
import fr.epitech.area.service.ServiceExecutor;
import fr.epitech.area.utils.UserService;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

public class AuthLoginCallbackRoute extends RouteExecutor {

	public AuthLoginCallbackRoute() {
		super(RouteType.GET, "/login/oauth/callback/:service_id/", false);
	}

	@Override
	public void handling(RoutingContext context) {
		try {
			int serviceId = Integer.parseInt(context.request().getParam("service_id"));
			ServiceExecutor service = Area.getServicesManager().getService(serviceId);
			if (service == null) {
				response(context, new ResponseError("service id not found"));
				return;
			}
			if (!service.isNeedOAuth()) {
				response(context, new ResponseError("no oauth on this service!"));
				return;
			}
			String secretState = context.request().getParam("state");
			UserService data = service.getCallbackOauthData(context, secretState);
			if (data == null) {
				response(context, new ResponseError("Unable to found user service."));
				return;
			}

			service.getLoginIdentity(data, identity -> {
				if (identity == null) {
					response(context, new ResponseError("identity not found"));
					return;
				}
				Area.getLogger().info("identity: " + identity);
				Area.getDatabase().getUsersTable().getUser(identity, user -> {
					if (user == null) {
						Area.getLogger().info("create user: " + identity);
						Area.getDatabase().getUsersTable().insertNewUser(new UserModel(identity, null), res -> {
							if (!res) {
								response(context, new ResponseError("new user error"));
								return;
							}
							String jwtToken = Area.getJwtAuth().generateToken(new JsonObject().put("email", identity));
							response(context, new JsonObject()
								.put("result", "success")
								.put("token", jwtToken));
						});
						return;
					}
					Area.getLogger().info("get jwt token user: " + identity);
					String jwtToken = Area.getJwtAuth().generateToken(new JsonObject().put("email", identity));
					response(context, new JsonObject()
						.put("result", "success")
						.put("token", jwtToken));
				});

			});
		} catch (NumberFormatException exception) {
			response(context, new ResponseError("error with service_id number"));
		}
	}

}
