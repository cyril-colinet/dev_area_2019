package fr.epitech.area.route.administration;

import fr.epitech.area.Area;
import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.route.RouteExecutor;
import fr.epitech.area.route.RouteType;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

public class AdminUsersRoute extends RouteExecutor {

	public AdminUsersRoute() {
		super(RouteType.GET, "/admin/users", true);
		this.setDescription("List all users registered");
	}

	@Override
	public void handling(RoutingContext context, UserModel user) {
		Area.getDatabase().getUsersTable().getAllUsers(userModels -> {
			JsonArray users = new JsonArray();
			for (UserModel userJson : userModels) {
				users.add(new JsonObject()
					.put("id", userJson.getId())
					.put("email", userJson.getEmail())
				);
			}
			response(context, users);
		});
	}

}
