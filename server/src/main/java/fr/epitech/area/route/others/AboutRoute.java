package fr.epitech.area.route.others;

import fr.epitech.area.Area;
import fr.epitech.area.action.ActionExecutor;
import fr.epitech.area.reaction.ReactionExecutor;
import fr.epitech.area.route.RouteExecutor;
import fr.epitech.area.route.RouteType;
import fr.epitech.area.service.ServiceExecutor;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

public class AboutRoute extends RouteExecutor {

	public AboutRoute() {
		super(RouteType.ALL, "/about.json", false);
		this.setDescription("Useless route (just for mandatory)");
	}

	@Override
	public void handling(RoutingContext context) {
		try {
			JsonArray services = new JsonArray();
			for (ServiceExecutor service : Area.getServicesManager().getServices()) {
				JsonArray actions = new JsonArray();
				JsonArray reactions = new JsonArray();

				for (ActionExecutor action : service.getActions()) {
					actions.add(new JsonObject()
						.put("name", action.getName())
						.put("description", action.getDescription()));
				}

				for (ReactionExecutor reaction : service.getReactions()) {
					reactions.add(new JsonObject()
						.put("name", reaction.getName())
						.put("description", reaction.getDescription()));
				}

				services.add(new JsonObject()
					.put("name", service.getName())
					.put("actions", actions)
					.put("reactions", reactions)
				);
			}
			response(context, new JsonObject()
				.put("client", new JsonObject()
					.put("host", context.request().remoteAddress().host()))
				.put("server", new JsonObject()
					.put("current_time", System.currentTimeMillis() / 1000)
					.put("services", services))
			);
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

}
