package fr.epitech.area.route.area;

import fr.epitech.area.Area;
import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.route.ResponseError;
import fr.epitech.area.route.RouteExecutor;
import fr.epitech.area.route.RouteType;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

public class AreaDeleteRoute extends RouteExecutor {

	public AreaDeleteRoute() {
		super(RouteType.DELETE, "/area/:id", true);
		this.setDescription("Delete AREA element");
	}

	@Override
	public void handling(RoutingContext context, UserModel user) {
		Area.getDatabase().execute("DELETE FROM users_area WHERE id = ?", new JsonArray().add(context.request().getParam("id")), res -> {
			if (res.succeeded()) {
				response(context, new JsonObject().put("result", "success"));
			} else {
				response(context, new ResponseError("Error occurred"));
			}
		});
	}
}
