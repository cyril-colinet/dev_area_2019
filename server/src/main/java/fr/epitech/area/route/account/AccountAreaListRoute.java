package fr.epitech.area.route.account;

import fr.epitech.area.Area;
import fr.epitech.area.action.ActionExecutor;
import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.reaction.ReactionExecutor;
import fr.epitech.area.route.ResponseError;
import fr.epitech.area.route.RouteExecutor;
import fr.epitech.area.route.RouteType;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

public class AccountAreaListRoute extends RouteExecutor {

	public AccountAreaListRoute() {
		super(RouteType.GET, "/account/areas", true);
		this.setDescription("Get the list of registered area of user");
	}

	@Override
	public void handling(RoutingContext context, UserModel user) {
		Area.getDatabase().getAreaTable().getUserAreas(user.getId(),
			res -> {
				if (res != null) {
					JsonArray result = new JsonArray();
					res.forEach(areaModel -> {
						JsonObject area = new JsonObject();
						area.put("id", areaModel.getId());

						// Get action information
						ActionExecutor action = Area.getServicesManager()
							.getAction(areaModel.getActionId());
						area.put("action", new JsonObject()
							.put("id", action.getId())
							.put("belongs_to_service", action.getParentService().getName())
							.put("name", action.getName())
							.put("description", action.getDescription())
							.put("config", areaModel.getActionData()));

						// Get reaction information
						ReactionExecutor reaction = Area.getServicesManager()
							.getReaction(areaModel.getReactionId());
						area.put("reaction", new JsonObject()
							.put("id", reaction.getId())
							.put("belongs_to_service", reaction.getParentService().getName())
							.put("name", reaction.getName())
							.put("description", reaction.getDescription())
							.put("config", areaModel.getReactionData()));

						// Add area json to result final array
						result.add(area);
					});

					response(context, new JsonObject()
						.put("result", "success")
						.put("data", result));
				} else {
					response(context, new ResponseError("Error occurred"));
				}
			}
		);
	}
}
