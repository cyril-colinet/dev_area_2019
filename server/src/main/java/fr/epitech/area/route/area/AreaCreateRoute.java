package fr.epitech.area.route.area;

import fr.epitech.area.Area;
import fr.epitech.area.action.ActionExecutor;
import fr.epitech.area.database.models.AreaModel;
import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.reaction.ReactionExecutor;
import fr.epitech.area.route.ResponseError;
import fr.epitech.area.route.RouteExecutor;
import fr.epitech.area.route.RouteType;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

public class AreaCreateRoute extends RouteExecutor {

	public AreaCreateRoute() {
		super(RouteType.POST, "/area", true);
		this.addRequiredParam("action_id", Integer.class);
		this.addRequiredParam("action_config", String.class);
		this.addRequiredParam("reaction_id", Integer.class);
		this.addRequiredParam("reaction_config", String.class);
		this.setDescription("Create AREA element");
	}

	@Override
	public void handling(RoutingContext context, UserModel user) {
		int actionId = context.getBodyAsJson().getInteger("action_id");
		int reactionId = context.getBodyAsJson().getInteger("reaction_id");

		ReactionExecutor reactionExecutor = Area.getServicesManager().getReaction(reactionId);
		ActionExecutor actionExecutor = Area.getServicesManager().getAction(actionId);

		if (reactionExecutor == null) {
			response(context, new ResponseError("Unknown Reaction ID"));
			return;
		}

		if (actionExecutor == null) {
			response(context, new ResponseError("Unknown Action ID"));
			return;
		}

		try {
			Area.getDatabase().getAreaTable().insertNewArea(user.getId(), new AreaModel(
				user.getId(),
				actionId, new JsonObject(context.getBodyAsJson().getString("action_config")),
				reactionId, new JsonObject(context.getBodyAsJson().getString("reaction_config"))
			), res -> {
				if (res) {
					response(context, new JsonObject()
						.put("result", "success"));
				} else {
					response(context, new ResponseError("Error occurred"));
				}
			});
		} catch (Exception ignored) {
			response(context, new ResponseError("Error occurred"));
		}
	}
}
