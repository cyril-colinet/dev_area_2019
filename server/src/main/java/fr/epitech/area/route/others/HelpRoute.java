package fr.epitech.area.route.others;

import fr.epitech.area.Area;
import fr.epitech.area.route.RouteExecutor;
import fr.epitech.area.route.RouteType;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

public class HelpRoute extends RouteExecutor {

	public HelpRoute() {
		super(RouteType.ALL, "/help", false);
		this.setDescription("List all routes and params");
	}

	@Override
	public void handling(RoutingContext context) {
		JsonArray routes = new JsonArray();

		for (RouteExecutor route : Area.getRegisteredRoutes()) {
			if (route instanceof ErrorRoute) {
				continue;
			}
			JsonArray params = new JsonArray();
			for (RouteParam param : route.getParams()) {
				params.add(new JsonObject()
					.put("name", param.getName())
					.put("type", param.getType().getSimpleName())
				);
			}
			JsonObject routeJson = new JsonObject()
				.put("type", route.getRouteType())
				.put("route", route.getPath());
			if (route.getDescription() != null) {
				routeJson.put("description", route.getDescription());
			}
			routeJson.put("require_jwt_token", route.isAuthPage());
			routeJson.put("params", params);
			routes.add(routeJson);
		}

		response(context, new JsonObject()
			.put("version", Area.API_VERSION)
			.put("routes", routes));
	}
}
