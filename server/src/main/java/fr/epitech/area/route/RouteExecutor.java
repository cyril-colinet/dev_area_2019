package fr.epitech.area.route;

import fr.epitech.area.Area;
import fr.epitech.area.database.models.UserModel;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.User;
import io.vertx.ext.web.RoutingContext;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

public abstract class RouteExecutor {

	/**
	 * Type of route.
	 */
	@Getter
	private RouteType routeType;

	/**
	 * Path of route.
	 */
	@Getter
	@Setter
	private String path;

	/**
	 * TRUE if is auth page.
	 */
	@Getter
	@Setter
	private boolean authPage;

	/**
	 * List of all params.
	 */
	@Getter
	private ArrayList<RouteParam> params = new ArrayList<>();

	@Getter
	@Setter
	private String description;

	/**
	 * Abstract class of routes.
	 *
	 * @param routeType
	 * @param path
	 * @param authPage
	 */
	public RouteExecutor(RouteType routeType, String path, boolean authPage) {
		this.routeType = routeType;
		this.path = path;
		this.authPage = authPage;
	}

	/**
	 * Pre handler of route for checking arguments and oauth.
	 *
	 * @param context
	 */
	public void preHandling(RoutingContext context) {
		// Check missing params
		JsonArray errors = new JsonArray();
		try {
			for (RouteParam param : params) {
				if (!context.getBodyAsJson().containsKey(param.getName())) {
					errors.add("missing argument " + param.getName());
					continue;
				}
				if (!(context.getBodyAsJson().getValue(param.getName()).getClass().equals(param.getType()))) {
					errors.add("incorrect value type of " + param.getName()
						+ " (given: " + context.getBodyAsJson().getValue(param.getName()).getClass().getSimpleName()
						+ ", required: " + param.getType().getSimpleName() + ")"
					);
				}
			}
		} catch (Exception exception) {
			response(context, new ResponseError("Internal error occurred."));
			return;
		}

		if (errors.size() > 0) {
			response(context, new ResponseError(errors));
			return;
		}

		// Check user is auth
		if (authPage) {
			String jwtToken = getJwtToken(context);

			if (jwtToken == null) {
				response(context, new ResponseError("missing jwt token"));
				return;
			}

			// Get user data
			Area.getJwtAuth().authenticate(new JsonObject().put("jwt", jwtToken), res -> {
				if (res.succeeded()) {
					User theUser = res.result();
					String email = theUser.principal().getString("email");
					if (email != null) {
						Area.getDatabase().getUsersTable().getUser(email, user -> {
							if (user == null) {
								response(context, new ResponseError("error user data, user not exist!"));
								return;
							}
							theUser.principal().put("user_id", user.getId());
							context.setUser(theUser);
							handling(context);
							handling(context, user);
						});
					}
				} else {
					response(context, new ResponseError("jsp"));
				}
			});
		} else {
			// Check is not login
			if (getJwtToken(context) != null
				&& !getPath().equals("/about.json")
				&& !getPath().equals("/*")) {
				response(context, new ResponseError("must logout first"));
				return;
			}
			handling(context);
		}
	}

	/**
	 * Add requirement param for request.
	 *
	 * @param name
	 */
	protected void addRequiredParam(String name, Class type) {
		this.params.add(new RouteParam(name, type));
	}

	/**
	 * Send response to request.
	 *
	 * @param context
	 * @param object
	 */
	protected void response(RoutingContext context, JsonObject object) {
		context.response()
			.putHeader("content-type", "application/json; charset=utf-8")
			.end(Json.encode(object));
	}

	/**
	 * Send response to request.
	 *
	 * @param context
	 * @param objects
	 */
	protected void response(RoutingContext context, List<JsonObject> objects) {
		context.response()
			.putHeader("content-type", "application/json; charset=utf-8")
			.end(Json.encode(objects));
	}

	/**
	 * Send response to request.
	 *
	 * @param context
	 * @param array
	 */
	protected void response(RoutingContext context, JsonArray array) {
		context.response()
			.putHeader("content-type", "application/json; charset=utf-8")
			.end(Json.encode(array));
	}

	/**
	 * Send response to request.
	 *
	 * @param context
	 * @param error
	 */
	protected void response(RoutingContext context, ResponseError error) {
		context.response()
			.putHeader("content-type", "application/json; charset=utf-8")
			.end(Json.encode(error.toJson()));
	}

	/**
	 * Get Jwt token of header of request.
	 *
	 * @param context
	 * @return
	 */
	private String getJwtToken(RoutingContext context) {
		String token = context.request().getHeader("Authorization");
		if (token == null) {
			return null;
		}
		if (!token.contains("Bearer ")) {
			return null;
		}
		return token.split("Bearer ")[1];
	}

	/**
	 * Route param class for future details.
	 */
	public static class RouteParam {

		@Getter
		private String name;

		@Getter
		private Class type;

		public RouteParam(String name, Class type) {
			this.name = name;
			this.type = type;
		}

	}

	/**
	 * Main handler of route.
	 *
	 * @param context
	 */
	public void handling(RoutingContext context) {

	}

	/**
	 * Main handler of route with user.
	 *
	 * @param context
	 * @param user
	 */
	public void handling(RoutingContext context, UserModel user) {

	}

}
