package fr.epitech.area.route.others;

import fr.epitech.area.route.ResponseError;
import fr.epitech.area.route.RouteExecutor;
import fr.epitech.area.route.RouteType;
import io.vertx.ext.web.RoutingContext;

public class ErrorRoute extends RouteExecutor {

	public ErrorRoute() {
		super(RouteType.ALL, "/*", false);
		this.setDescription("Last route when no route found");
	}

	@Override
	public void handling(RoutingContext context) {
		response(context, new ResponseError());
	}

}
