package fr.epitech.area.route.account;

import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.route.RouteExecutor;
import fr.epitech.area.route.RouteType;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

public class AccountInfoRoute extends RouteExecutor {

	public AccountInfoRoute() {
		super(RouteType.GET, "/account/info", true);
		this.setDescription("Get account information of user");
	}

	@Override
	public void handling(RoutingContext context, UserModel user) {
		response(context, new JsonObject()
			.put("user_id", user.getId())
			.put("email", user.getEmail()));
	}

}
