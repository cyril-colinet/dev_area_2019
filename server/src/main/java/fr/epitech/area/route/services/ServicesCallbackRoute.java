package fr.epitech.area.route.services;

import fr.epitech.area.Area;
import fr.epitech.area.database.models.ServiceModel;
import fr.epitech.area.route.ResponseError;
import fr.epitech.area.route.RouteExecutor;
import fr.epitech.area.route.RouteType;
import fr.epitech.area.service.ServiceExecutor;
import fr.epitech.area.utils.UserService;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

public class ServicesCallbackRoute extends RouteExecutor {

	public ServicesCallbackRoute() {
		super(RouteType.GET, "/services/callback/:service_id/", false);
	}

	@Override
	public void handling(RoutingContext context) {
		try {
			int serviceId = Integer.parseInt(context.request().getParam("service_id"));
			ServiceExecutor service = Area.getServicesManager().getService(serviceId);
			if (service == null) {
				response(context, new ResponseError("service id not found"));
				return;
			}
			String secretState = context.request().getParam("state");
			if (!ServicesSubscribeRoute.getSecretCache().containsKey(secretState)) {
				response(context, new ResponseError("error with state"));
				return;
			}
			int userId = ServicesSubscribeRoute.getSecretCache().get(secretState);
			UserService data = service.getCallbackOauthData(context, secretState);
			if (data == null) {
				response(context, new ResponseError("unable to found service."));
				return;
			}
			Area.getDatabase().getServicesTable().insertNewService(new ServiceModel(serviceId, userId, data), res -> {
				response(context, new JsonObject().put("result", "ok"));
				//context.response().putHeader("location", "/services.html").setStatusCode(302).end();
			});
		} catch (NumberFormatException exception) {
			response(context, new ResponseError("error with service_id number"));
		}
	}

}
