package fr.epitech.area.route.authentification;

import fr.epitech.area.Area;
import fr.epitech.area.route.ResponseError;
import fr.epitech.area.route.RouteExecutor;
import fr.epitech.area.route.RouteType;
import fr.epitech.area.service.ServiceExecutor;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

import java.util.Random;

public class AuthLoginOauthRoute extends RouteExecutor {

	public AuthLoginOauthRoute() {
		super(RouteType.POST, "/login/oauth", false);
		this.addRequiredParam("service_id", Integer.class);
		this.setDescription("Login with oauth service");
	}

	@Override
	public void handling(RoutingContext context) {
		try {
			int serviceId = context.getBodyAsJson().getInteger("service_id");
			ServiceExecutor service = Area.getServicesManager().getService(serviceId);
			if (service == null) {
				response(context, new ResponseError("service id not found"));
				return;
			}
			if (!service.isNeedOAuth()) {
				response(context, new ResponseError("no oauth on this service!"));
				return;
			}
			String secretState = "secret" + new Random().nextInt(999_999);
			response(context, new JsonObject()
				.put("result", "ok")
				.put("link_oauth", service.getOAuthUrl(context, Area.getHostApi() + "/api/login/oauth/callback/" + serviceId + "/", secretState)));
		} catch (NumberFormatException exception) {
			response(context, new ResponseError("error with service_id number"));
		}
	}

}
