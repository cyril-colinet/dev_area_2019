package fr.epitech.area.route.services;

import fr.epitech.area.Area;
import fr.epitech.area.database.models.UserModel;
import fr.epitech.area.route.ResponseError;
import fr.epitech.area.route.RouteExecutor;
import fr.epitech.area.route.RouteType;
import fr.epitech.area.service.ServiceExecutor;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import lombok.Getter;

import java.util.HashMap;
import java.util.Random;

public class ServicesSubscribeRoute extends RouteExecutor {

	@Getter
	private static HashMap<String, Integer> secretCache = new HashMap<>();

	public ServicesSubscribeRoute() {
		super(RouteType.POST, "/services/subscribe", true);
		this.addRequiredParam("service_id", Integer.class);
	}

	@Override
	public void handling(RoutingContext context, UserModel user) {
		try {
			int serviceId = context.getBodyAsJson().getInteger("service_id");
			ServiceExecutor service = Area.getServicesManager().getService(serviceId);
			if (service == null) {
				response(context, new ResponseError("service id not found"));
				return;
			}
			if (!service.isNeedOAuth()) {
				response(context, new JsonObject()
					.put("result", "ok")
					.put("message", "no needed oauth")
				);
				return;
			}
			Area.getDatabase().getServicesTable().getService(user.getId(), serviceId, res -> {
				if (res != null) {
					response(context, new ResponseError("already subscribe"));
					return;
				}
				String secretState = "secret" + new Random().nextInt(999_999);
				secretCache.put(secretState, user.getId());
				response(context, new JsonObject()
					.put("result", "ok")
					.put("link_oauth", service.getOAuthUrl(context, Area.getHostApi() + "/api/services/callback/" + serviceId + "/", secretState)));
			});
		} catch (NumberFormatException exception) {
			response(context, new ResponseError("error with service_id number"));
		}
	}

}
