package fr.epitech.area.route.authentification;

import fr.epitech.area.Area;
import fr.epitech.area.route.ResponseError;
import fr.epitech.area.route.RouteExecutor;
import fr.epitech.area.route.RouteType;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

public class AuthResetPasswordRoute extends RouteExecutor {

	public AuthResetPasswordRoute() {
		super(RouteType.POST, "/password/reset", false);
		this.addRequiredParam("password", String.class);
		this.setDescription("Reset your password by a new");
	}

	@Override
	public void handling(RoutingContext context) {
		String token = context.request().getParam("token");
		String password = context.getBodyAsJson().getString("password");
		if (token == null || password == null) {
			response(context, new ResponseError("Missing recovery token or password"));
			return;
		}

		// Change password by recovery token
		Area.getDatabase().getUsersTable().resetPassword(token, password, success -> {
			if (!success) {
				response(context, new ResponseError("Unable to change your password"));
				return;
			}

			response(context, new JsonObject()
				.put("result", "success"));
		});
	}
}
