package fr.epitech.area.utils;

import io.vertx.core.json.JsonObject;
import lombok.Getter;

public class UserService {

	/**
	 * Token object
	 */
	@Getter
	private JsonObject tokenObject;

	/**
	 * Unique state string
	 */
	@Getter
	private String uniqueState;

	/**
	 * New user service class
	 * @param tokenObject
	 * @param uniqueState
	 */
	public UserService(JsonObject tokenObject, String uniqueState) {
		this.tokenObject = tokenObject;
		this.uniqueState = uniqueState;
	}
}
