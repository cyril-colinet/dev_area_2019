package fr.epitech.area.utils;

import com.sendgrid.*;
import fr.epitech.area.Area;

import java.io.IOException;

public class Email {

	private String to;
	private String subject;
	private String content;

	public Email setTo(String to) {
		this.to = to;
		return this;
	}

	public Email setSubject(String subject) {
		this.subject = subject;
		return this;
	}

	public Email setContent(String content) {
		this.content = content;
		return this;
	}

	public boolean send() {
		if (this.subject == null
			|| this.to == null
			|| this.content == null) {
			return false;
		}
		Scheduler.runTask(() -> {
			com.sendgrid.Email from = new com.sendgrid.Email("no-reply@area.epitech-nice.fr");
			com.sendgrid.Email to = new com.sendgrid.Email(this.to);
			Content content = new Content("text/html", this.content);
			Mail mail = new Mail(from, subject, to, content);

			SendGrid sg = new SendGrid(Area.getDotenv().get("SENDGRID_API_KEY"));
			Request request = new Request();
			try {
				request.setMethod(Method.POST);
				request.setEndpoint("mail/send");
				request.setBody(mail.build());
				Response response = sg.api(request);
			} catch (IOException ex) {
				try {
					throw ex;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		return true;
	}
}
