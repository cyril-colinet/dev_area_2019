package fr.epitech.area.utils;

import java.util.concurrent.*;

public abstract class Scheduler implements Runnable {

	private static ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(Runtime.getRuntime().availableProcessors());
	private static ExecutorService executorService = Executors.newCachedThreadPool();

	public static void runTask(Runnable run) {
		executorService.submit(run);
	}

	public static void cancelTasks() {
		executorService.shutdown();
		scheduledExecutorService.shutdown();
	}

	public ScheduledFuture scheduleAsyncDelayedTask(long delay, TimeUnit timeUnit) {
		return scheduledExecutorService.schedule(this, delay, timeUnit);
	}

	public ScheduledFuture scheduleAsyncRepeatingTask(long start, long period, TimeUnit timeUnit) {
		return scheduledExecutorService.scheduleAtFixedRate(this, start, period, timeUnit);
	}

	public void runTask() {
		Scheduler.runTask(this);
	}

}

