import 'package:flutter/widgets.dart';

class FacebookIcon {
  FacebookIcon._();

  static const _kFontFam = 'FacebookIcon';

  static const IconData ifacebook = const IconData(0xe800, fontFamily: _kFontFam);
}
