import 'package:area/components/drawerTab.dart';
import 'package:area/components/painterBackground.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DashboardLayout extends StatelessWidget {

    final String title;
    final Widget child;
    final Widget floatingActionButton;
    final GlobalKey<ScaffoldState> scaffoldKey;

    DashboardLayout({@required this.title, @required this.child,
        @required this.scaffoldKey, this.floatingActionButton});

    Widget displayButtonBackOrMenu(BuildContext context) {
        if (Navigator.of(context).canPop()) {
            return Container(
                margin: const EdgeInsets.only(right: 10),
                child: InkWell(
                    child: Icon(Icons.arrow_back_ios,
                        color: Colors.white,
                        size: 25,
                    ),
                    onTap: () => Navigator.of(context).maybePop(),
                ),
            );
        }

        return Container(
            margin: const EdgeInsets.only(right: 10),
            child: InkWell(
                child: Icon(Icons.menu,
                    color: Colors.white,
                    size: 30,
                ),
                onTap: () => this.scaffoldKey.currentState.openDrawer(),
            ),
        );
    }

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            key: this.scaffoldKey,
            floatingActionButton: this.floatingActionButton,
            floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
            resizeToAvoidBottomPadding: false,
            body: GestureDetector(
                onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
                child: Container(
                    color: Theme.of(context).scaffoldBackgroundColor,
                    child: CustomPaint(
                        painter: PainterBackground(
                            linear: true,
                            size: MediaQuery.of(context).size.height / 3.7
                        ),
                        child: SafeArea(
                            bottom: false,
                            child: Container(
                                child: Column(
                                    children: <Widget>[

                                        // Header
                                        Container(
                                            margin: const EdgeInsets.only(top: 20, left: 20, right: 20),
                                            child: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: <Widget>[
                                                    Row(
                                                        children: <Widget>[
                                                            this.displayButtonBackOrMenu(context),
                                                            Text(this.title,
                                                                style: TextStyle(
                                                                    color: Colors.white,
                                                                    fontWeight: FontWeight.bold,
                                                                    fontSize: 25
                                                                ),
                                                            ),
                                                        ],
                                                    ),

                                                    InkWell(
                                                        onTap: () {
                                                            SharedPreferences.getInstance().then((prefs) {
                                                                prefs.clear();

                                                                // Redirect to login page
                                                                Navigator.of(context).pushReplacementNamed('/login');
                                                            });
                                                        },
                                                        child: Icon(Icons.power_settings_new,
                                                            color: Colors.white,
                                                            size: 27,
                                                        ),
                                                    )
                                                ],
                                            ),
                                        ),

                                        this.child
                                    ],
                                )
                            ),
                        )
                    ),
                ),
            ),
            drawer: Drawer(
                child: Container(
                    decoration: BoxDecoration(
                        color: Theme.of(context).scaffoldBackgroundColor
                    ),
                    child: ListView(
                        physics: NeverScrollableScrollPhysics(),
                        children: <Widget>[

                            // Header
                            Container(
                                margin: const EdgeInsets.only(bottom: 30, top: 20),
                                child: SizedBox(
                                    width: MediaQuery.of(context).size.width / 2.5,
                                    height: MediaQuery.of(context).size.width / 2.5,
                                    child: Image.asset("assets/images/logo_light.png")
                                ),
                            ),

                            DrawerTab(
                                title: "Dashboard",
                                iconData: Icons.home,
                                routeName: '/dashboard',
                            ),

                            DrawerTab(
                                title: "Settings",
                                iconData: Icons.touch_app,
                                routeName: '/settings',
                            ),

                            DrawerTab(
                                title: "Profile",
                                iconData: Icons.person,
                                routeName: '/subscribe',
                            ),

                        ],
                    ),
                )
            ),
        );
    }
}