import 'package:area/components/painterBackground.dart';
import 'package:flutter/material.dart';

class AuthLayout extends StatelessWidget {

    final Widget child;
    final GlobalKey<ScaffoldState> scaffoldKey;

    AuthLayout({@required this.child, this.scaffoldKey});

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            key: this.scaffoldKey != null ? this.scaffoldKey : Key("authScaffold"),
            resizeToAvoidBottomPadding: false,
            body: GestureDetector(
                onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
                child: Container(
                    color: Theme.of(context).scaffoldBackgroundColor,
                    child: CustomPaint(
                        painter: PainterBackground(
                            size: MediaQuery.of(context).size.height / 2,
                            linear: false
                        ),
                        child: SafeArea(
                            child: this.child,
                        ),
                    )
                ),
            )
        );
    }
}