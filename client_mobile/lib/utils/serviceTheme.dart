import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

/// This class get all theme information about the service
/// For example, facebook have a blue background and facebook icon, and need
/// to be authenticated by an account
class ServiceTheme {

    /// Make this class singleton
    static ServiceTheme _instance = ServiceTheme.internal();
    ServiceTheme.internal();
    factory ServiceTheme() => _instance;

    /// Theme information by service
    final Map<String, Map<String, dynamic>> serviceTheme = {
        'timer': {
            'icon': Icons.access_time,
            'backgroundColor': Color(0xFFc0392b),
            'color': Colors.white,
            'gradient': null
        },
        'facebook': {
            'icon': FontAwesomeIcons.facebook,
            'backgroundColor': Color(0xFF3b5998),
            'color': Colors.white,
            'gradient': null
        },
        'github': {
            'icon': FontAwesomeIcons.github,
            'backgroundColor': Color(0xFF333333),
            'color': Colors.white,
            'gradient': null
        },
        'google': {
            'icon': FontAwesomeIcons.google,
            'backgroundColor': Color(0xFF4285f4),
            'color': Colors.white,
            'gradient': null
        },
        'sms': {
            'icon': Icons.textsms,
            'backgroundColor': Color(0xFF2dce89),
            'color': Colors.white,
            'gradient': null
        },
        'dropbox': {
            'icon': FontAwesomeIcons.dropbox,
            'backgroundColor': Color(0xFF007ee5),
            'color': Colors.white,
            'gradient': null
        },
        'twitter': {
            'icon': FontAwesomeIcons.twitter,
            'backgroundColor': Color(0xFF00acee),
            'color': Colors.white,
            'gradient': null
        },
        'discord': {
            'icon': FontAwesomeIcons.discord,
            'backgroundColor': Color(0xFF7289DA),
            'color': Colors.white,
            'gradient': null
        },
        'spotify': {
            'icon': FontAwesomeIcons.spotify,
            'backgroundColor': Color(0xFF1DB954),
            'color': Colors.white,
            'gradient': null
        },
        'email': {
            'icon': Icons.email,
            'backgroundColor': Color(0xFF1A82E2),
            'color': Colors.white,
            'gradient': null
        },
        'news': {
            'icon': FontAwesomeIcons.newspaper,
            'backgroundColor': Color(0xFFd35400),
            'color': Colors.white,
            'gradient': LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [
                    Color(0xFF1e3c72),
                    Color(0xFF2a5298),
                ]
            )
        },
        'instagram': {
            'icon': FontAwesomeIcons.instagram,
            'backgroundColor': Color(0xFF4285f4),
            'color': Colors.white,
            'gradient': LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [
                    Color(0xFFfeda75),
                    Color(0xFFfa7e1e),
                    Color(0xFFd62976),
                    Color(0xFF962fbf),
                    Color(0xFF4f5bd5),
                ]
            )
        }
    };

    /// Get basic theme info about the service
    ThemeInformation getThemeInformation(String serviceName) {
        if (!this.serviceTheme.containsKey(serviceName.toLowerCase())) {
            return ThemeInformation(
                title: serviceName[0].toUpperCase() + serviceName.substring(1),
                icon: Icons.close,
                backgroundColor: Colors.black,
                color: Colors.white,
                gradient: null
            );
        }

        return ThemeInformation(
            title: serviceName[0].toUpperCase() + serviceName.substring(1),
            //faIcon: this.serviceTheme[serviceName]['faIcon'],
            icon: this.serviceTheme[serviceName]['icon'],
            backgroundColor: this.serviceTheme[serviceName]['backgroundColor'],
            color: this.serviceTheme[serviceName]['color'],
            gradient: this.serviceTheme[serviceName]['gradient']
        );
    }
}

/// Class that return all the basic information about the
/// service theme (color, icon, etc...)
class ThemeInformation {

    // Class fields
    final IconData icon;
    final String title;
    final Color backgroundColor;
    final Color color;
    final Gradient gradient;

    /// Constructor
    ThemeInformation({
        @required this.icon,
        @required this.title,
        @required this.backgroundColor,
        @required this.color,
        @required this.gradient
    });

}