import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

String baseUrl = "https://area.epitech-nice.fr";

/// Get request from url
/// Return future of data
Future<dynamic> getRequest(String url, {bool isAnonymousRequest = false}) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    // Call request with corresponded headers
    if (!isAnonymousRequest) {
        final response = await http.get(baseUrl + url,
            headers: {
                "Authorization": "Bearer " + prefs.getString("jwt_token"),
                "Accept": "application/json",
            },
        );

        if (response.statusCode == 200)
            return json.decode(response.body);

        return {
            'message': "Internal error. Please try again later."
        };
    } else {
        final response = await http.get(baseUrl + url,
            headers: {
                "Accept": "application/json",
            }
        );

        if (response.statusCode == 200)
            return json.decode(response.body);

        return {
            'message': "Internal error. Please try again later."
        };
    }
}

Future<dynamic> postRequest(String url, {bool withoutCredentials = false, Map data}) async {
    try {
        final SharedPreferences prefs = await SharedPreferences.getInstance();

        if (!withoutCredentials) {
            final response = await http.post(baseUrl + url,
                headers: {
                    "Authorization": "Bearer " + prefs.getString("jwt_token"),
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                },
                body: json.encode(data)
            );

            if (response.statusCode == 200)
                return json.decode(response.body);

            return {
                'message': "Internal error. Please try again later."
            };
        } else {
            final response = await http.post(baseUrl + url,
                body: json.encode(data),
                headers: {
                    "Accept-Content": "application/json",
                    "Content-Type": "application/json",
                }
            );

            if (response.statusCode == 200)
                return json.decode(response.body);

            return {
                'message': "Internal error. Please try again later."
            };
        }
    } catch(err) {
        return {
            'message': "Internal error. Please try again later."
        };
    }
}

/*
Future<String> putRequest(String url, {bool isAnonymousRequest = false, String json = ""}) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    if (!isAnonymousRequest) {
        final response = await http.post(baseUrl + url, headers: {
            'Authorization': 'Bearer ' + prefs.getString("access_token")
        }, body: json
        );
        print("STATUS CODE : " + response.statusCode.toString());
        if (response.statusCode == 200)
            return response.body;
        return "request failed";
    } else {
        final response = await http.post(baseUrl + url, headers: {
            'Authorization': 'Client-ID 9f0153451f88a91'
        }, body: json
        );
        print("STATUS CODE : " + response.statusCode.toString());
        if (response.statusCode == 200)
            return response.body;
        return "request failed";
    }
}*/