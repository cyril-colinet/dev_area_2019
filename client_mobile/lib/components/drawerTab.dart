import 'package:flutter/material.dart';

class DrawerTab extends StatelessWidget {

    final IconData iconData;
    final String title;
    final String routeName;

    DrawerTab({@required this.iconData, @required this.title, @required this.routeName});

    @override
    Widget build(BuildContext context) {
        return ListTile(
            onTap: () {
                if (ModalRoute.of(context).settings.name != this.routeName)
                    Navigator.of(context).pushReplacementNamed(this.routeName);
            },
            title: Row(
                children: <Widget>[
                    SizedBox(
                        width: 25,
                        child: Icon(this.iconData,
                            color: Theme.of(context).buttonColor,
                            size: 22,
                        ),
                    ),

                    Container(
                        margin: const EdgeInsets.only(left: 10),
                        child: Text(this.title,
                            style: TextStyle(
                                color: Color(0xFF131313),
                                fontSize: 20
                            ),
                        ),
                    )
                ],
            )
        );
    }
}
