import 'dart:ui';

import 'package:flutter/material.dart';

class PainterBackground extends CustomPainter {

    final bool linear;
    final double size;

    PainterBackground({@required this.linear, @required this.size});

    @override
    void paint(Canvas canvas, Size size) {
        final paint = Paint();

        // Create gradient color
        Gradient gradient = LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [
                Color(0xFF5e72e4),
                Color(0xFF825ee4),
            ]
        );

        // Create rectangle
        paint.shader = gradient.createShader(Rect.fromCircle(
            center: new Offset(165.0, 55.0),
            radius: 180.0,
        ));


        // Shape path
        var path = Path();
        path.lineTo(size.width, 0);
        path.lineTo(size.width, (this.size - 100));
        path.lineTo(0, (this.size - 100) + (this.linear ? 0 : 20));

        path.close();

        // Draw rectangle
        canvas.drawPath(path, paint);
    }

    @override
    bool shouldRepaint(CustomPainter oldDelegate) => false;

}