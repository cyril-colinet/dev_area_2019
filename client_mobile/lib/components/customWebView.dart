//import 'package:flutter/material.dart';
//import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
//
///// LoginPage class
///// StatefulWidget class
//class CustomWebView extends StatefulWidget {
//  final String url;
//
//  const CustomWebView(this.url);
//
//  @override
//  State createState() => new _CustomWebView();
//}
//
///// LoginPageState class
///// Extended class state
//class _CustomWebView extends State<CustomWebView> with SingleTickerProviderStateMixin {
//  final flutterWebViewPlugin = FlutterWebviewPlugin();
//
//  /// On URL in webview is changed
//  void onURLChanged(url) async {
//        if (url != widget.url) {
//          flutterWebViewPlugin.close();
//          Navigator.of(context).pushReplacementNamed('/dashboard');
//        }
//  }
//
//  /// Configure listeners on webview creation
//  @override
//  void initState() {
//    super.initState();
//    flutterWebViewPlugin.onUrlChanged.listen(this.onURLChanged);
//  }
//
//  /// Destroy webview when widget is closed
//  @override
//  void dispose() {
//    super.dispose();
//    flutterWebViewPlugin.dispose();
//  }
//
//  /// Build widget
//  @override
//  Widget build(BuildContext context) {
//    print(widget.url);
//    return new SafeArea(
//        child: WebviewScaffold(
//          url: widget.url,
//          appCacheEnabled: false,
//          clearCookies: true,
//          clearCache: true,
//          withJavascript: true,
//        )
//    );
//  }
//}
