import 'package:area/components/configure/abstractConfigurator.dart';
import 'package:area/components/configure/actionButtonMini.dart';
import 'package:area/utils/request.dart';
import 'package:area/utils/serviceTheme.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

/// Create new event with action and reaction
/// This class in an authenticated page
class ConfigureReactions extends AConfigurator {
    @override
    _ConfigureReactions createState() => _ConfigureReactions();
}

/// State of the [ConfigurePage] class
/// Loading page before no information loaded
class _ConfigureReactions extends State<ConfigureReactions> {

    // Class fields
    Map<String, dynamic> _reactionsByService = {};
    String selectedReaction;

    /// Called before the render of the widget
    /// Just call the API to get the available services
    @override
    void initState() {
        super.initState();

        // Call API
        getRequest("/api/reactions/available")
            .then((data) {
                Map<String, dynamic> tmp = {};
                data.forEach((reaction) {
                    if ((reaction['reactions'] as List).isEmpty)
                        return;
                    tmp.putIfAbsent(reaction['service_name'], () {
                        return {
                            'reactions': reaction['reactions'],
                            'service_id': reaction['service_id']
                        };
                    });
                });

                // Add to content with the state
                this.setState(() => this._reactionsByService = tmp);
            });
    }

    /// Build widget and return [Widget] object
    Widget displayContent() {
        if (this._reactionsByService.isEmpty) {
            return Container(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                height: ((MediaQuery.of(context).size.height / 3) * 1.8) + 110,
                child: GridView.count(
                    crossAxisCount: 3,
                    children: List.generate(15, (int index) {
                        return Container(
                            decoration: BoxDecoration(
                                border: Border.all(
                                    color: Colors.transparent,
                                    width: 5
                                )
                            ),
                            child: Material(
                                elevation: 5,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0)
                                ),
                                child: Shimmer.fromColors(
                                    baseColor: Color(0xFFf7fafc),
                                    highlightColor: Color(0xFFEEEEEE),
                                    child: ClipRRect(
                                        borderRadius: BorderRadius.circular(10.0),
                                        child: Container(
                                            decoration: BoxDecoration(
                                                color: Colors.white,
                                            ),
                                            child: Container()
                                        ),
                                    ),
                                ),
                            ),
                        );
                    }),
                ),
            );
        }

        List<Widget> reactionsWidgets = [];
        this._reactionsByService.forEach((serviceName, serviceDetails) {
            ThemeInformation themeInfo = ServiceTheme.internal().getThemeInformation(serviceName);

            /// Add the widget to the list and display it
            serviceDetails['reactions'].forEach((reaction) {
                reactionsWidgets.add(
                    Container(
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: Colors.transparent,
                                width: 5
                            )
                        ),
                        child: InkWell(
                            onTap: () {
                                this.widget.selected.clear();
                                this.widget.selected.putIfAbsent('reaction', () {
                                    return {
                                        'service_name': serviceName,
                                        'service_id': serviceDetails['service_id'],
                                        'id': reaction['id'],
                                        'need_oauth': reaction['need_oauth'],
                                        'is_oauth': reaction['is_oauth'],
                                        'description': reaction['description'],
                                        'required_config': reaction['required_config']
                                    };
                                });

                                // Set selected box
                                this.setState(() => this.selectedReaction = reaction['name']);
                            },
                            child: ActionButtonMini(
                                themeInformation: themeInfo,
                                description: reaction['description'],
                                selected: this.selectedReaction == reaction['name'],
                            ),
                        ),
                    )
                );
            });
        });

        /// Display column of actions
        return Container(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            height: ((MediaQuery.of(context).size.height / 3) * 1.8) + 110,
            child: GridView.count(
                crossAxisCount: 3,
                children: reactionsWidgets,
            ),
        );
    }

    @override
    Widget build(BuildContext context) {
        return Column(
            children: <Widget>[
                Container(
                    margin: const EdgeInsets.only(bottom: 10),
                    child: Material(
                        elevation: 5,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0)
                        ),
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(10.0),
                            child: Container(
                                padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                                width: MediaQuery.of(context).size.width - 40,
                                child: Text("Select one of these reactions :",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Color(0xFF32325d),
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18
                                    ),
                                )
                            ),
                        ),
                    ),
                ),

                // Content
                this.displayContent()
            ],
        );
    }
}