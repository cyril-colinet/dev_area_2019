import 'package:area/utils/serviceTheme.dart';
import 'package:flutter/material.dart';

class ActionButtonMini extends StatelessWidget {

    final ThemeInformation themeInformation;
    final String description;
    final bool selected;

    /// Constructor
    ActionButtonMini({@required this.themeInformation, @required this.description,
        @required this.selected});

    @override
    Widget build(BuildContext context) {
        return Material(
            elevation: 5,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
            ),
            child: ClipRRect(
                borderRadius: BorderRadius.circular(10.0),
                child: Container(
                    decoration: BoxDecoration(
                        color: this.themeInformation.backgroundColor.withOpacity(this.selected ? 1 : 0.5),
                        gradient: this.themeInformation.gradient,
                    ),
                    width: MediaQuery.of(context).size.width / 3,
                    child: Container(
                        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                        child: Column(
                            children: <Widget>[

                                // Header
                                Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                        Icon(this.themeInformation.icon,
                                            color: this.themeInformation.color,
                                            size: 20,
                                        ),

                                        Text(this.themeInformation.title,
                                            style: TextStyle(
                                                color: this.themeInformation.color,
                                                fontWeight: FontWeight.bold
                                            ),
                                        )
                                    ],
                                ),

                                Container(
                                    margin: const EdgeInsets.only(top: 10),
                                    child: Text(this.description,
                                        maxLines: 4,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                            color: this.themeInformation.color,
                                            fontSize: 13.5
                                        ),
                                    ),
                                ),
                            ],
                        ),
                    )
                ),
            ),
        );
    }
}