import 'package:flutter/cupertino.dart';

/// This class just contains a map with configuration of
/// every pages in the configuration AREA
abstract class AConfigurator extends StatefulWidget {

    /// Configuration of the selected action
    final Map<String, dynamic> selected = {};

    /// Handle the next step button, used to add custom event handlers
    /// in the current page of the wizard
    VoidCallback handleNextStepButton() => () {};

}