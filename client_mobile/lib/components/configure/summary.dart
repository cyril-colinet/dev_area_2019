import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:area/components/animateDotsIndicator.dart';
import 'package:area/components/configure/abstractConfigurator.dart';
import 'package:area/components/configure/actionButtonMini.dart';
import 'package:area/utils/request.dart';
import 'package:area/utils/serviceTheme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class ConfigureSummary extends AConfigurator {

    final Map<String, dynamic> config;
    final _ConfigureSummary state = _ConfigureSummary();

    /// Constructor
    ConfigureSummary({@required this.config});

    @override
    VoidCallback handleNextStepButton() => () {
        return this.state.sendTheNewConfigurationArea();
    };

    @override
    _ConfigureSummary createState() => this.state;
}

class _ConfigureSummary extends State<ConfigureSummary> {

    // Controllers
    final PageController _controller = new PageController();

    // Class fields
    bool _actionOAuth = false;
    bool _reactionOAuth = false;
    Map<String, TextEditingController> _textControllers = {};
    String _errorMessage;
    String _successMessage;

    /// Handle the "next step" button and apply custom button action
    void sendTheNewConfigurationArea() {
        bool allRequiredFieldsFilled = () {
            bool filled = true;
            this._textControllers.forEach((field, controller) {
                if (controller.text.isEmpty)
                    filled = false;
            });

            return filled;
        }();

        // Check if all information filled before push to the server
        if (!this._actionOAuth || !this._reactionOAuth || !allRequiredFieldsFilled) {
            this.setState(() {
                this._successMessage = null;
                this._errorMessage = "Check if you are connect to the two services and if all the configuration is provided.";
            });
            return;
        }

        // Create object of configurations (actions and reactions)
        Map<String, String> actions_config = {};
        Map<String, String> reactions_config = {};
        this._textControllers.forEach((field, controller) {
            List<String> splitter = field.split('.');
            if (splitter[0] == "action")
                actions_config.putIfAbsent(splitter[1], () => controller.text);
            else if (splitter[0] == "reaction")
                reactions_config.putIfAbsent(splitter[1], () => controller.text);
        });

        // Create configuration
        Map<String, dynamic> config = {
            'action_id': this.widget.config['action']['id'],
            'action_config': json.encode(actions_config),
            'reaction_id': this.widget.config['reaction']['id'],
            'reaction_config': json.encode(reactions_config),
        };

        // Post configuration of the new AREA
        postRequest("/api/area", data: config).then((data) {
            if (data["result"] != null && data["result"] == "success") {
                this.setState(() {
                    this._successMessage = "The new AREA has successfully registered!";
                    this._errorMessage = null;
                });
                return;
            }

            this.setState(() {
                this._successMessage = null;
                this._errorMessage = "Internal error occurred. Please try again later.";
            });
        });
    }

    /// handle the login to service_name action button
    /// Just display a webview in bottom sheet
    VoidCallback handleOAuthLoginService(String type) {
        return () {
            postRequest("/api/services/subscribe", data: {'service_id': this.widget.config[type]['service_id']})
                .then((data) {
                    // Display webview
                    if (data["result"] == "ok" && data["link_oauth"] != null) {
                        FlutterWebviewPlugin webviewPlugin = FlutterWebviewPlugin();
                        StreamSubscription<WebViewStateChanged> listener;

                        // Listen to end of webview
                        void onStateChanged(WebViewStateChanged state) {
                            if (state.url.startsWith("https://area.epitech-nice.fr/")) {
                                if (state.type == WebViewState.shouldStart)
                                    webviewPlugin.hide();

                                if (state.type == WebViewState.finishLoad) {
                                    webviewPlugin.evalJavascript("document.documentElement.innerHTML").then((body) {
                                        RegExp exp = RegExp(
                                            r"<[^>]*>",
                                            multiLine: true,
                                            caseSensitive: true
                                        );
                                        return body.replaceAll(exp, '');
                                    }).then((resultJson) {
                                        Map<String, dynamic> result = json.decode(resultJson);

                                        // Check if result exists
                                        if (result["result"] == null || result["result"] != "ok") {
                                            Navigator.pop(context);
                                            this.setState(() {
                                                this._errorMessage = "We have some trouble to login your account. Please try again later.";
                                            });
                                            return;
                                        }

                                        // Close webview and destroy it
                                        webviewPlugin.close();
                                        listener.cancel();
                                        webviewPlugin.dispose();

                                        // Change values of variables
                                        this.setState(() {
                                            if (type == "action") {
                                                this._actionOAuth = true;
                                            } else if (type == "reaction") {
                                                this._reactionOAuth = true;
                                            }

                                            // Fake change of the request
                                            this.widget.config[type]['is_oauth'] = true;
                                            this._errorMessage = null;
                                            this._successMessage = "Successfully connected to " + this.widget.config[type]['service_name'] + "!";
                                            Navigator.pop(context);
                                        });
                                    });
                                }
                            }
                        }

                        // Listener
                        listener = webviewPlugin.onStateChanged.listen(onStateChanged);

                        // Show bottom modal
                        showModalBottomSheet(
                            isDismissible: false,
                            isScrollControlled: true,
                            context: context,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0)
                            ),
                            builder: (BuildContext context) {
                                return Container(
                                    height: MediaQuery.of(context).size.height - 100,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(20),
                                            topRight: Radius.circular(20),
                                        )
                                    ),
                                    child: Column(
                                        children: <Widget>[
                                            Container(
                                                height: 50,
                                                padding: const EdgeInsets.symmetric(horizontal: 10),
                                                child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                    children: <Widget>[
                                                        InkWell(
                                                            onTap: () {
                                                                webviewPlugin.close();
                                                                listener.cancel();
                                                                webviewPlugin.dispose();

                                                                // Close bottom sheet
                                                                Navigator.pop(context);
                                                            },
                                                            child: Icon(Icons.close,
                                                                size: 30,
                                                                color: Color(0xFF131313)
                                                            ),
                                                        ),

                                                        Text("Login to " + this.widget.config[type]['service_name'] + " service",
                                                            style: TextStyle(
                                                                color: Color(0xFF131313),
                                                                fontWeight: FontWeight.bold,
                                                                fontSize: 18
                                                            ),
                                                        ),
                                                    ],
                                                ),
                                            ),
                                            Expanded(
                                                child: WebviewScaffold(
                                                    userAgent: "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36",
                                                    clearCache: true,
                                                    clearCookies: true,
                                                    withZoom: false,
                                                    withJavascript: true,
                                                    url: data["link_oauth"],
                                                ),
                                            )
                                        ],
                                    )
                                );
                            }
                        );
                    }
                });
        };
    }

    /// Display button "login to service_name" when oauth is needed and
    /// if the user is not connected
    /// Return a [Column] with [MaterialButton] if oauth needed, and [Container] otherwise
    Widget displayNotAuthenticatedServices() {
        // Action login
        Widget actionLogin = () {
            if (this.widget.config['action']['need_oauth'] && !this.widget.config['action']['is_oauth']) {
                this._actionOAuth = false;

                return Container(
                    margin: const EdgeInsets.only(top: 10),
                    child: MaterialButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0)
                        ),
                        elevation: 5,
                        onPressed: this.handleOAuthLoginService("action"),
                        color: Color(0xFF3c4d69),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                                Container(
                                    margin: const EdgeInsets.only(right: 5),
                                    child: Icon(Icons.lock,
                                        color: Colors.white,
                                        size: 16,
                                    ),
                                ),
                                Text("Login to " + this.widget.config['action']['service_name'],
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold
                                    ),
                                ),
                            ],
                        )
                    ),
                );
            }

            this._actionOAuth = true;
            return Container();
        }();

        /// Reaction login button
        Widget reactionLogin = () {
            if (this.widget.config['reaction']['need_oauth'] && !this.widget.config['reaction']['is_oauth']) {
                this._reactionOAuth = false;

                return MaterialButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0)
                    ),
                    elevation: 5,
                    onPressed: this.handleOAuthLoginService("reaction"),
                    color: Color(0xFF3c4d69),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                            Container(
                                margin: const EdgeInsets.only(right: 5),
                                child: Icon(Icons.lock,
                                    color: Colors.white,
                                    size: 16,
                                ),
                            ),
                            Text("Login to " + this.widget.config['reaction']['service_name'],
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold
                                ),
                            ),
                        ],
                    )
                );
            }

            this._reactionOAuth = true;
            return Container();
        }();

        return Column(
            children: <Widget>[
                // Display buttons
                actionLogin,
                reactionLogin,
            ],
        );
    }

    /// Display the configuration form of the action
    Widget displayConfigurationForm() {
        // Display content with divider
        return Column(
            children: <Widget>[
                SizedBox(height: 10),
                SizedBox(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height / 3 - 85,
                    child: PageView(
                        controller: this._controller,
                        scrollDirection: Axis.horizontal,
                        physics: AlwaysScrollableScrollPhysics(),
                        children: <Widget>[

                            // Display actions required fields
                            () {
                                Map<String, dynamic> requiredFields = this.widget.config['action']['required_config'];
                                List<Widget> widgets = [
                                    Align(
                                        alignment: Alignment.centerLeft,
                                        child: Container(
                                            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                                            child: Text("Action configuration :",
                                                textAlign: TextAlign.start,
                                                style: TextStyle(
                                                    color: Color(0xFF32325d),
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 18
                                                ),
                                            )
                                        ),
                                    )
                                ];

                                if (requiredFields.isNotEmpty) {
                                    // Create textfield for all needed fields
                                    requiredFields.forEach((field, desc) {
                                        // Add text controller into list
                                        this._textControllers.putIfAbsent("action." + field.toString(), () => TextEditingController());

                                        // Create widget
                                        widgets.add(Container(
                                            margin: const EdgeInsets.only(bottom: 10),
                                            padding: const EdgeInsets.symmetric(horizontal: 20),
                                            child: Material(
                                                shape: RoundedRectangleBorder(
                                                    borderRadius: BorderRadius.circular(5.0)
                                                ),
                                                elevation: 2,
                                                clipBehavior: Clip.antiAlias,
                                                child: TextField(
                                                    autocorrect: false,
                                                    controller: this._textControllers["action." + field.toString()],
                                                    decoration: InputDecoration(
                                                        hintText: desc.toString(),
                                                    ),
                                                    keyboardAppearance: Brightness.dark,
                                                ),
                                            ),
                                        ));
                                    });
                                } else {
                                    widgets.add(Container(
                                        margin: const EdgeInsets.only(bottom: 10),
                                        child: Material(
                                            elevation: 5,
                                            shape: RoundedRectangleBorder(
                                                borderRadius: BorderRadius.circular(10.0)
                                            ),
                                            child: ClipRRect(
                                                borderRadius: BorderRadius.circular(10.0),
                                                child: Container(
                                                    padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                                                    width: MediaQuery.of(context).size.width - 40,
                                                    child: Text("No configuration needed for this part.",
                                                        textAlign: TextAlign.center,
                                                        style: TextStyle(
                                                            color: Color(0xFF32325d),
                                                            fontSize: 14
                                                        ),
                                                    )
                                                ),
                                            ),
                                        ),
                                    ));
                                }

                                return Column(children: widgets);
                            }(),

                            // Display reactions required fields
                            () {
                                Map<String, dynamic> requiredFields = this.widget.config['reaction']['required_config'];
                                List<Widget> widgets = [
                                    Align(
                                        alignment: Alignment.centerLeft,
                                        child: Container(
                                            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                                            child: Text("Reaction configuration :",
                                                textAlign: TextAlign.start,
                                                style: TextStyle(
                                                    color: Color(0xFF32325d),
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 18
                                                ),
                                            )
                                        ),
                                    )
                                ];

                                if (requiredFields.isNotEmpty) {
                                    // Create textfield for all needed fields
                                    requiredFields.forEach((field, desc) {
                                        // Add text controller into list
                                        this._textControllers.putIfAbsent("reaction." + field.toString(), () => TextEditingController());

                                        // Create widget
                                        widgets.add(Container(
                                            margin: const EdgeInsets.only(bottom: 10),
                                            padding: const EdgeInsets.symmetric(horizontal: 20),
                                            child: Material(
                                                shape: RoundedRectangleBorder(
                                                    borderRadius: BorderRadius.circular(5.0)
                                                ),
                                                elevation: 2,
                                                clipBehavior: Clip.antiAlias,
                                                child: TextField(
                                                    autocorrect: false,
                                                    controller: this._textControllers["reaction." + field.toString()],
                                                    decoration: InputDecoration(
                                                        hintText: desc.toString(),
                                                    ),
                                                    keyboardAppearance: Brightness.dark,
                                                ),
                                            ),
                                        ));
                                    });
                                } else {
                                    widgets.add(Container(
                                        margin: const EdgeInsets.only(bottom: 10),
                                        child: Material(
                                            elevation: 5,
                                            shape: RoundedRectangleBorder(
                                                borderRadius: BorderRadius.circular(10.0)
                                            ),
                                            child: ClipRRect(
                                                borderRadius: BorderRadius.circular(10.0),
                                                child: Container(
                                                    padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                                                    width: MediaQuery.of(context).size.width - 40,
                                                    child: Text("No configuration needed for this part.",
                                                        textAlign: TextAlign.center,
                                                        style: TextStyle(
                                                            color: Color(0xFF32325d),
                                                            fontSize: 15
                                                        ),
                                                    )
                                                ),
                                            ),
                                        ),
                                    ));
                                }

                                return Column(children: widgets);
                            }(),

                        ],
                    ),
                ),
                Container(
                    margin: EdgeInsets.only(top: 20),
                    child: AnimatedDotsIndicator(
                        color: Theme.of(context).buttonColor,
                        controller: this._controller,
                        itemCount: 2,
                        onPageSelected: (int page) {
                            this._controller.animateToPage(page,
                                duration: const Duration(milliseconds: 300),
                                curve: Curves.ease,
                            );
                        },
                    ),
                ),

            ],
        );
    }

    /// Build widget
    /// Return a [Widget] class
    @override
    Widget build(BuildContext context) {
        return Column(
            children: <Widget>[

                // Display error
                () {
                    if (this._errorMessage == null)
                        return Container();

                    return Container(
                        margin: const EdgeInsets.only(bottom: 10),
                        child: Material(
                            elevation: 5,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0)
                            ),
                            child: ClipRRect(
                                borderRadius: BorderRadius.circular(10.0),
                                child: Container(
                                    decoration: BoxDecoration(
                                        color: Colors.redAccent
                                    ),
                                    padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                                    width: MediaQuery.of(context).size.width - 40,
                                    child: Text(this._errorMessage,
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14
                                        ),
                                    )
                                ),
                            ),
                        ),
                    );
                }(),

                // Display success
                () {
                    if (this._successMessage == null)
                        return Container();

                    return Container(
                        margin: const EdgeInsets.only(bottom: 10),
                        child: Material(
                            elevation: 5,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0)
                            ),
                            child: ClipRRect(
                                borderRadius: BorderRadius.circular(10.0),
                                child: Container(
                                    decoration: BoxDecoration(
                                        color: Color(0xFF2dce89)
                                    ),
                                    padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                                    width: MediaQuery.of(context).size.width - 40,
                                    child: Text(this._successMessage,
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14
                                        ),
                                    )
                                ),
                            ),
                        ),
                    );
                }(),

                // Display message
                Container(
                    margin: const EdgeInsets.only(bottom: 10),
                    child: Material(
                        elevation: 5,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0)
                        ),
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(10.0),
                            child: Container(
                                padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                                width: MediaQuery.of(context).size.width - 40,
                                child: Text("Summary of your new AREA configuration :",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Color(0xFF32325d),
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18
                                    ),
                                )
                            ),
                        ),
                    ),
                ),

                // Action and reaction
                Container(
                    margin: const EdgeInsets.only(top: 10),
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                            Container(
                                height: 110,
                                child: ActionButtonMini(
                                    themeInformation: ServiceTheme.internal().getThemeInformation(this.widget.config['action']['service_name']),
                                    description: this.widget.config['action']['description'],
                                    selected: true,
                                ),
                            ),

                            Icon(Icons.arrow_forward,
                                size: 40,
                                color: Theme.of(context).buttonColor,
                            ),

                            Container(
                                height: 110,
                                child: ActionButtonMini(
                                    themeInformation: ServiceTheme.internal().getThemeInformation(this.widget.config['reaction']['service_name']),
                                    description: this.widget.config['reaction']['description'],
                                    selected: true,
                                ),
                            )
                        ],
                    ),
                ),

                Container(
                    margin: const EdgeInsets.only(top: 10),
                    //padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Column(
                        children: <Widget>[
                            Container(
                                padding: const EdgeInsets.symmetric(horizontal: 20),
                                child: this.displayNotAuthenticatedServices(),
                            ),
                            this.displayConfigurationForm()
                        ],
                    ),
                ),

            ],
        );
    }
}