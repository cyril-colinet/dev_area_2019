import 'package:flutter/material.dart';
import 'package:area/pages/login.dart';
import 'package:area/pages/register.dart';
import 'package:area/pages/configure.dart';
import 'package:area/pages/subscribe.dart';
import 'package:area/pages/dashboard.dart';
import 'package:area/pages/splashscreen.dart';
import 'package:area/pages/resetpassword.dart';
import 'package:area/pages/settings.dart';

class Area extends StatelessWidget {

    @override
    Widget build(BuildContext context) {
        return new MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'AREA',
            theme: new ThemeData(
                primaryColor: Colors.white,
                scaffoldBackgroundColor: Color(0xFFf7fafc),
                backgroundColor: Color(0xFFf7fafc),
                buttonColor: Color(0xFF5e72e4),
                inputDecorationTheme: InputDecorationTheme(
                    filled: true,
                    fillColor: Colors.white,
                    contentPadding: const EdgeInsets.symmetric(horizontal: 20),
                    focusColor: Colors.grey,
                    border: InputBorder.none
                ),
                textTheme: TextTheme(
                    title: TextStyle(
                        color: Colors.black,
                        fontFamily: "Calibre-Semibold",
                        letterSpacing: 1.0,
                        fontSize: 25
                    )
                )
            ),
            home: new SplashScreen(),
            routes: {
                // Auth layout (not authenticated routes)
                '/login': (_) => LoginPage(),
                '/register': (_) => RegisterPage(),

                // Dashboard layout, all authenticated routes)
                '/dashboard': (_) => DashboardPage(),
                '/subscribe': (_) => SubscribeView(),
                '/configure': (_) => ConfigurePage(),
                '/reset': (_) => ResetPasswordPage(),
                '/settings': (_) => SettingsPage()
            },
        );
    }
}

void main() => runApp(Area());