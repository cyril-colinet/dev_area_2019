class Areas {
    int     areaId;
    String  actionName;
    String  actionDesc;
    String  actionService;
    String  reActionName;
    String  reActionDesc;
    String  reActionService;
    Map<String, dynamic> reActionConfig;
    Map<String, dynamic> actionConfig;

    Areas({this.actionName, this.actionDesc, this.reActionName, this.reActionDesc, this.actionConfig, this.reActionConfig, this.areaId});

    Areas.fromJson(Map<String, dynamic> json) {
        this.areaId = json["id"];
        this.actionName = json["action"]["name"];
        this.actionDesc = json["action"]["description"];
        this.actionConfig = json["action"]["config"];
        this.actionService = json["action"]["belongs_to_service"];
        this.reActionName = json["reaction"]["name"];
        this.reActionDesc = json["reaction"]["description"];
        this.reActionConfig = json["reaction"]["config"];
        this.reActionService = json["reaction"]["belongs_to_service"];
    }

    int getAreaId() {
        return (this.areaId);
    }

    String getActionName() {
        return (this.actionName);
    }


    String getReActionName() {
        return (this.reActionName);
    }


    String getActionDesc() {
        return (this.actionDesc);
    }


    String getReActionDesc() {
        return (this.reActionDesc);
    }

    String getActionService() {
        return (this.actionService);
    }

    String getReActionService() {
        return (this.reActionService);
    }

    Map<String, dynamic> getActionConfig() {
        return (this.actionConfig);
    }


    Map<String, dynamic> getReActionConfig() {
        return (this.reActionConfig);
    }
}