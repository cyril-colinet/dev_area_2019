class Service {
  int serviceId;
  String serviceName;
  bool needOauth;
  bool isOauth;

  Service({this.serviceId, this.serviceName, this.needOauth, this.isOauth});

  Service.fromJson(Map<String, dynamic> json) {
    serviceId = json["service_id"];
    serviceName = json["service_name"];
    needOauth = json["need_oauth"];
    isOauth = json["is_oauth"];
  }

  String getName() {
    return (this.serviceName);
  }

  int getServiceId() {
    return (this.serviceId);
  }

  bool getOauthNeed(){
    return (this.needOauth);
  }

  bool getLogStatus(){
    return (this.isOauth);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    data["service_id"] = this.serviceId;
    data["service_name"] = this.serviceName;
    data["need_oauth"] = this.needOauth;
    data["is_oauth"] = this.isOauth;
    return (data);
  }
}