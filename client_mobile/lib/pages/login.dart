import 'dart:async';
import 'dart:convert';
import 'package:area/layouts/auth.dart';
import 'package:flutter/material.dart';
import 'package:area/utils/request.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class LoginPage extends StatefulWidget {
    @override
    _LoginPage createState() => _LoginPage();
}

class _LoginPage extends State<LoginPage> {

    // Class fields
    final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
    final TextEditingController email = TextEditingController();
    final TextEditingController password = TextEditingController();

    // State fields
    String _error;
    String _errorMessage;
    String _successMessage;

    VoidCallback handleOAuthLoginService(int ServiceId, String ServiceName) {
      return () {
        postRequest("/api/login/oauth", data: {'service_id': ServiceId}, withoutCredentials: true)
            .then((data) {
          // Display webview
          if (data["result"] == "ok" && data["link_oauth"] != null) {
            FlutterWebviewPlugin webviewPlugin = FlutterWebviewPlugin();
            StreamSubscription<WebViewStateChanged> listener;

            // Listen to end of webview
            void onStateChanged(WebViewStateChanged state) {
              if (state.url.startsWith("https://area.epitech-nice.fr/")) {
                if (state.type == WebViewState.shouldStart)
                  webviewPlugin.hide();

                if (state.type == WebViewState.finishLoad) {
                  webviewPlugin.evalJavascript("document.documentElement.innerHTML").then((body) {
                    RegExp exp = RegExp(
                        r"<[^>]*>",
                        multiLine: true,
                        caseSensitive: true
                    );
                    return body.replaceAll(exp, '');
                  }).then((resultJson) {
                    Map<String, dynamic> result = json.decode(resultJson);
                    print(result);

                    if (result["result"] == "success") {
                      SharedPreferences.getInstance().then((prefs) {
                        prefs.setString("jwt_token", data["token"]);

                        webviewPlugin.close();
                        listener.cancel();
                        webviewPlugin.dispose();
                        this.setState(() {
                          Navigator.pop(context);
                        });
                        // Display success message
                        this.scaffoldKey.currentState.showSnackBar(SnackBar(
                          backgroundColor: Colors.green,
                          duration: Duration(seconds: 5),
                          elevation: 10,
                          content: Text("Successfully logged in! You will be redirected in 3 seconds to the dashboard page.",
                            style: TextStyle(
                                fontSize: 16
                            ),
                          ),
                        ));

                        // Redirect to the dashboard page
                        var _duration = new Duration(seconds: 2);
                        return new Timer(_duration, () {
                          Navigator.of(context).pushReplacementNamed('/dashboard');
                        });
                      });
                    }

                    // Check if result exists
                    if (result["result"] == null || result["result"] != "ok") {
                      this.setState(() {
                        this._errorMessage = "We have som trouble to login your account. Please try again later.";
                      });
                      return;
                    }

                    // Close webview and destroy it
                    webviewPlugin.close();
                    listener.cancel();
                    webviewPlugin.dispose();

                    // Change values of variables
                    this.setState(() {

                      // Fake change of the request
                      this._errorMessage = null;
                      this._successMessage = "Successfully connected to " + ServiceName + "!";
                      Navigator.pop(context);

                    });
                  });
                }
              }
            }

            // Listener
            listener = webviewPlugin.onStateChanged.listen(onStateChanged);

            // Show bottom modal
            showModalBottomSheet(
                isDismissible: false,
                isScrollControlled: true,
                context: context,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0)
                ),
                builder: (BuildContext context) {
                  return Container(
                      height: MediaQuery.of(context).size.height - 100,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20),
                          )
                      ),
                      child: Column(
                        children: <Widget>[
                          Container(
                            height: 50,
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                InkWell(
                                  onTap: () {
                                    webviewPlugin.close();
                                    listener.cancel();
                                    webviewPlugin.dispose();

                                    // Close bottom sheet
                                    Navigator.pop(context);
                                  },
                                  child: Icon(Icons.close,
                                      size: 30,
                                      color: Color(0xFF131313)
                                  ),
                                ),

                                Text("Login to " + ServiceName + " service",
                                  style: TextStyle(
                                      color: Color(0xFF131313),
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            child: WebviewScaffold(
                              userAgent: "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36",
                              clearCache: true,
                              clearCookies: true,
                              withZoom: false,
                              withJavascript: true,
                              url: data["link_oauth"],
                            ),
                          )
                        ],
                      )
                  );
                }
            );
          }
        });
      };
    }

    /// Perform login of the user
    /// Before send data, verify if the email is valid and all fields
    /// was not empty
    void handleLoginButton() {
        if (this.email.text.isEmpty || this.password.text.isEmpty) {
            this.setState(() => this._error = "The email and the password cannot be empty.");
            return;
        } else if (!RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(this.email.text)) {
            this.setState(() => this._error = "Please enter a valid email address.");
            return;
        }

        // Perform request
        postRequest("/api/login",
            withoutCredentials: true,
            data: {
                "email": this.email.text,
                "password": this.password.text
            }
        ).then((data) {
            if (data["result"] == "success") {
                SharedPreferences.getInstance().then((prefs) {
                    prefs.setString("email", this.email.text);
                    prefs.setString("jwt_token", data["token"]);

                    // Display success message
                    this.scaffoldKey.currentState.showSnackBar(SnackBar(
                        backgroundColor: Colors.green,
                        duration: Duration(seconds: 5),
                        elevation: 10,
                        content: Text("Successfully logged in! You will be redirected in 3 seconds to the dashboard page.",
                            style: TextStyle(
                                fontSize: 16
                            ),
                        ),
                    ));

                    // Redirect to the dashboard page
                    var _duration = new Duration(seconds: 2);
                    return new Timer(_duration, () {
                        Navigator.of(context).pushReplacementNamed('/dashboard');
                    });
                });
            } else {
                this.setState(() => this._error = data["message"]);
            }
        });
    }

    VoidCallback authorizeLoginByServiceName(String serviceName) {
        return () {
          print("hey");
        };
    }

    /// Build widget
    /// Return a [Widget] with the content of the login screen
    @override
    Widget build(BuildContext context) {
        if (this._error != null) {
            final SnackBar errorBar = SnackBar(
                backgroundColor: Colors.redAccent,
                duration: Duration(seconds: 5),
                elevation: 10,
                content: Text(this._error,
                    style: TextStyle(
                        fontSize: 16
                    ),
                ),
            );

            // Create snackbar and display it
            this.scaffoldKey.currentState.showSnackBar(errorBar);
            this._error = null;
        }

        return AuthLayout(
            scaffoldKey: this.scaffoldKey,
            child: Container(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[

                        // Logo
                        SizedBox(
                            height: MediaQuery.of(context).size.height / 2 - 100,
                            child: Container(
                                margin: const EdgeInsets.only(top: 50),
                                child: Image.asset("assets/images/logo_dark.png",
                                    alignment: Alignment.topCenter,
                                    width: MediaQuery.of(context).size.width / 2,
                                ),
                            )
                        ),

                        // Text fields
                        Container(
                            padding: const EdgeInsets.symmetric(horizontal: 30),
                            child: Column(
                                children: <Widget>[

                                    Container(
                                        margin: const EdgeInsets.only(bottom: 10),
                                        child: Text("Login with",
                                            style: TextStyle(
                                                color: Colors.grey,
                                                fontSize: 13
                                            ),
                                        ),
                                    ),

                                    Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: <Widget>[
                                          SizedBox(
                                            width: 35,
                                            height: 35,
                                            child:  MaterialButton(
                                              padding: const EdgeInsets.only(left: 0),
                                              onPressed: this.handleOAuthLoginService(4, "Google"),
                                              color: Colors.white,
                                              child: Icon(FontAwesomeIcons.google, color: Colors.red,),
                                            ),
                                          ),
                                            Padding(padding: const EdgeInsets.only(left: 7)),
                                            SignInButton(
                                                Buttons.Facebook,
                                                mini: true,
                                                onPressed: this.handleOAuthLoginService(1, "Facebook"),
                                            ),
                                          Padding(padding: const EdgeInsets.only(left: 7)),
                                          SizedBox(
                                              width: 35,
                                              height: 35,
                                              child:  MaterialButton(
                                                padding: const EdgeInsets.only(left: 0),
                                                onPressed: this.handleOAuthLoginService(5, "Instagram"),
                                                color: Colors.pink,
                                                child: Icon(FontAwesomeIcons.instagram, color: Colors.white,),
                                              ),
                                          ),
                                          Padding(padding: const EdgeInsets.only(left: 13)),
                                          SizedBox(
                                            width: 35,
                                            height: 35,
                                            child:  MaterialButton(
                                              padding: const EdgeInsets.only(left: 0),
                                              onPressed: this.handleOAuthLoginService(8, "Dropbox"),
                                              color: Colors.white,
                                              child: Icon(FontAwesomeIcons.dropbox, color: Colors.indigo,),
                                            ),
                                          ),
                                          Padding(padding: const EdgeInsets.only(left: 7)),
                                          SignInButton(
                                                Buttons.GitHub,
                                                mini: true,
                                                onPressed: this.handleOAuthLoginService(3, "Github"),
                                            ),
                                        ],
                                    ),

                                    Divider(),

                                    Container(
                                        margin: const EdgeInsets.only(bottom: 20, top: 10),
                                        child: Text("Or login with your credentials",
                                            style: TextStyle(
                                                color: Colors.grey,
                                                fontSize: 13
                                            ),
                                        ),
                                    ),

                                    // Username
                                    Material(
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(5.0)
                                        ),
                                        elevation: 2,
                                        clipBehavior: Clip.antiAlias,
                                        child: TextField(
                                            autocorrect: false,
                                            controller: this.email,
                                            decoration: InputDecoration(
                                                hintText: "Email address",
                                            ),
                                            keyboardAppearance: Brightness.dark,
                                        ),
                                    ),

                                    SizedBox(height: 20),

                                    // Username
                                    Material(
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(5.0)
                                        ),
                                        elevation: 2,
                                        clipBehavior: Clip.antiAlias,
                                        child: TextField(
                                            controller: this.password,
                                            decoration: InputDecoration(
                                                hintText: "Password",
                                            ),
                                            obscureText: true,
                                            keyboardAppearance: Brightness.dark,
                                        )
                                    ),
                                ],
                            ),
                        ),

                        Container(
                            margin: const EdgeInsets.only(top: 20),
                            child: MaterialButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5.0)
                                ),
                                minWidth: MediaQuery.of(context).size.width - 60,
                                elevation: 5,
                                onPressed: this.handleLoginButton,
                                color: Theme.of(context).buttonColor,
                                child: Text("Login",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold
                                    ),
                                ),
                            ),
                        ),

                        Container(
                            margin: const EdgeInsets.only(top: 10, right: 30, left: 30),
                            alignment: Alignment.centerRight,
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                    InkWell(
                                        onTap: () => Navigator.of(context).pushReplacementNamed('/reset'),
                                        child: Text("Forgot password ?",
                                            style: TextStyle(
                                                color: Theme.of(context).buttonColor,
                                                fontSize: 14,
                                                fontWeight: FontWeight.bold
                                            ),
                                        ),
                                    ),

                                    InkWell(
                                        onTap: () => Navigator.of(context).pushReplacementNamed('/register'),
                                        child: Text("Register",
                                            style: TextStyle(
                                                color: Theme.of(context).buttonColor,
                                                fontSize: 14,
                                                fontWeight: FontWeight.bold
                                            ),
                                        ),
                                    ),
                                ],
                            )
                        ),
                    ],
                )
            ),
        );
    }

}