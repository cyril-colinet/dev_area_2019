import 'dart:async';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TransitionScreen extends StatefulWidget {
    @override
    _TransitionScreenState createState() => new _TransitionScreenState();
}

class _TransitionScreenState extends State<TransitionScreen>
    with SingleTickerProviderStateMixin {

    Animation<Color> animation;
    AnimationController animController;

    /// When state is started
    @override void initState() {
        super.initState();

        // Configure blinking logo
        this.animController = new AnimationController(
            vsync: this,
            duration: Duration(milliseconds: 1500)
        );
        this.animController.repeat();

        final CurvedAnimation curve = CurvedAnimation(
            parent: this.animController, curve: Curves.linear);
        this.animation = ColorTween(begin: Colors.white, end: Colors.blue).animate(curve);
        this.animation.addStatusListener((status) {
            if (status == AnimationStatus.completed) {
                this.animController.reverse();
            } else if (status == AnimationStatus.dismissed) {
                this.animController.forward();
            }

            // Rebuild
            setState(() {});
        });
        this.animController.forward();
    }

    /// On page dispose
    @override void dispose() {
        this.animController.dispose();
        super.dispose();
    }

    /// Build widget
    @override
    Widget build(BuildContext context) {
        return Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [
                        Color(0xFF5e72e4),
                        Color(0xFF825ee4),
                    ]
                )
            ),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                    SizedBox(
                        width: MediaQuery.of(context).size.width / 2,
                        child: FadeTransition(
                            opacity: this.animController,
                            child: Image.asset("assets/images/logo_dark.png"),
                        ),
                    )
                ],
            )
        );
    }

}