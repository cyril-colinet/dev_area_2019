import 'dart:async';
import 'package:area/pages/editAreaConfig.dart';
import 'package:flutter/material.dart';
import 'package:area/object/areas.dart';
import 'package:area/utils/request.dart';
import 'package:area/layouts/dashboard.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:area/utils/serviceTheme.dart';

class DashboardPage extends StatefulWidget {
    @override
    _DashboardPage createState() => _DashboardPage();
}

class _DashboardPage extends State<DashboardPage> {

    // Unique key for scaffold (drawer display)
    final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
    List<Areas> _areas = [];


    /// Init state of the app
    @override
    void initState() {
        super.initState();

        // Base request
        this.getAreas().then((res) => this.setState(() => this._areas = res));
        this.startTime();
    }


    /// Reload content every 10s
    startTime() async {
        var _duration = new Duration(seconds: 10);
        return new Timer(_duration, () {
            this.getAreas().then((res) => this.setState(() => this._areas = res));
            this.startTime();
        });
    }


    /// Get the [List] of [Areas] for a specific user
    /// This request is called every 10s
    Future<List<Areas>> getAreas() {
        Future<dynamic> request = getRequest("/api/account/areas");

        return request.then((data) {
            List<Areas> finalValues = [];
            for (int i = 0; i < data["data"].length; i++) {
                Areas tmp = Areas.fromJson(data["data"][i]);
                finalValues.add(tmp);
            }

            return finalValues;
        });
    }


    /// Format string and add uppercase to first letter
    String formatString(String src) {
        return('${src[0].toUpperCase()}${src.substring(1)}');
    }


    /// Display content of the application
    Widget displayView() {
        return Column(
            children: <Widget>[
                Container(
                    margin: const EdgeInsets.only(top: 20, bottom: 20),
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    height: 50,
                    child: MaterialButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0)
                        ),
                        elevation: 5,
                        onPressed: () => Navigator.of(context).pushNamed("/configure"),
                        color: Color(0xFF2dce89),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                                Container(
                                    margin: const EdgeInsets.only(right: 5),
                                    child: Icon(Icons.add_circle,
                                        color: Colors.white,
                                        size: 20,
                                    ),
                                ),
                                Text("Configure new AREA",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold
                                    ),
                                ),
                            ],
                        )
                    ),
                ),

                // Display content
                    (){
                    if (this._areas.length == 0) {
                        return Material(
                            elevation: 5,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0)
                            ),
                            child: ClipRRect(
                                borderRadius: BorderRadius.circular(10.0),
                                child: Container(
                                    width: MediaQuery.of(context).size.width - 40,
                                    child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                            // Image
                                            SvgPicture.asset("assets/images/waiting.svg",
                                                width: MediaQuery.of(context).size.width,
                                            ),

                                            Container(
                                                padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                                                child: Column(
                                                    children: <Widget>[
                                                        Container(
                                                            margin: const EdgeInsets.only(bottom: 20),
                                                            child: Text("You don't have any widget configured",
                                                                style: TextStyle(
                                                                    color: Color(0xFF32325d),
                                                                    fontWeight: FontWeight.bold,
                                                                    fontSize: 15
                                                                ),
                                                            ),
                                                        ),

                                                        Text("If you want to add a widget you can simply click to the button upper.",
                                                            textAlign: TextAlign.center,
                                                            style: TextStyle(
                                                                color: Color(0xFF8898aa),
                                                            ),
                                                        )
                                                    ],
                                                ),
                                            )
                                        ],
                                    ),
                                ),
                            ),
                        );
                    }

                    return Container(
                        height: MediaQuery.of(context).size.height - 220,
                        child: ListView.separated(
                            itemCount: this._areas.length,
                            separatorBuilder: (context, index) => SizedBox(height: 5),
                            itemBuilder: (BuildContext context, int index) {
                                ThemeInformation actionTheme = ServiceTheme.internal().getThemeInformation(this._areas[index].getActionService());
                                ThemeInformation reactionTheme = ServiceTheme.internal().getThemeInformation(this._areas[index].getReActionService());

                                return InkWell(
                                    onTap: () {
                                        print(this._areas[index].getActionService());
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => EditAreaPage(area: this._areas[index]),
                                            ),
                                        );},
                                    child: Container(
                                        margin: const EdgeInsets.only(bottom: 10),
                                        padding: const EdgeInsets.symmetric(horizontal: 20),
                                        child: Material(
                                            elevation: 5,
                                            shape: RoundedRectangleBorder(
                                                borderRadius: BorderRadius.circular(10.0)
                                            ),
                                            child: ClipRRect(
                                                borderRadius: BorderRadius.circular(10.0),
                                                child: Stack(
                                                    alignment: Alignment.center,
                                                    children: <Widget>[

                                                        // List
                                                        Row(
                                                            children: <Widget>[
                                                                // Action
                                                                Container(
                                                                    width: MediaQuery.of(context).size.width / 2 - 24,
                                                                    height: 110,
                                                                    decoration: BoxDecoration(
                                                                        color: actionTheme.backgroundColor,
                                                                        gradient: actionTheme.gradient,
                                                                    ),
                                                                    child: Container(
                                                                        padding: const EdgeInsets.only(top: 20, bottom: 20, right: 30, left: 20),
                                                                        child: Column(
                                                                            children: <Widget>[

                                                                                // Header
                                                                                Row(
                                                                                    children: <Widget>[
                                                                                        Icon(actionTheme.icon,
                                                                                            color: actionTheme.color,
                                                                                            size: 20,
                                                                                        ),
                                                                                        SizedBox(width: 5),
                                                                                        Text(actionTheme.title,
                                                                                            style: TextStyle(
                                                                                                color: actionTheme.color,
                                                                                                fontWeight: FontWeight.bold
                                                                                            ),
                                                                                        )
                                                                                    ],
                                                                                ),

                                                                                Container(
                                                                                    alignment: Alignment.centerLeft,
                                                                                    margin: const EdgeInsets.only(top: 10),
                                                                                    child: Text(this._areas[index].getActionDesc(),
                                                                                        textAlign: TextAlign.left,
                                                                                        maxLines: 3,
                                                                                        overflow: TextOverflow.ellipsis,
                                                                                        style: TextStyle(
                                                                                            color: actionTheme.color,
                                                                                            fontSize: 13.5
                                                                                        ),
                                                                                    ),
                                                                                ),
                                                                            ],
                                                                        ),
                                                                    )
                                                                ),

                                                                SizedBox(width: 8),

                                                                // Reaction
                                                                Container(
                                                                    width: MediaQuery.of(context).size.width / 2 - 24,
                                                                    height: 110,
                                                                    decoration: BoxDecoration(
                                                                        color: reactionTheme.backgroundColor,
                                                                        gradient: reactionTheme.gradient,
                                                                    ),
                                                                    child: Container(
                                                                        padding: const EdgeInsets.only(top: 20, bottom: 20, right: 20, left: 30),
                                                                        child: Column(
                                                                            children: <Widget>[

                                                                                // Header
                                                                                Row(
                                                                                    mainAxisAlignment: MainAxisAlignment.end,
                                                                                    children: <Widget>[
                                                                                        Icon(reactionTheme.icon,
                                                                                            color: reactionTheme.color,
                                                                                            size: 20,
                                                                                        ),
                                                                                        SizedBox(width: 5),
                                                                                        Text(reactionTheme.title,
                                                                                            style: TextStyle(
                                                                                                color: reactionTheme.color,
                                                                                                fontWeight: FontWeight.bold
                                                                                            ),
                                                                                        )
                                                                                    ],
                                                                                ),

                                                                                Container(
                                                                                    alignment: Alignment.centerRight,
                                                                                    margin: const EdgeInsets.only(top: 10),
                                                                                    child: Text(this._areas[index].getReActionDesc(),
                                                                                        textAlign: TextAlign.right,
                                                                                        maxLines: 3,
                                                                                        overflow: TextOverflow.ellipsis,
                                                                                        style: TextStyle(
                                                                                            color: reactionTheme.color,
                                                                                            fontSize: 13.5
                                                                                        ),
                                                                                    ),
                                                                                ),
                                                                            ],
                                                                        ),
                                                                    )
                                                                ),
                                                            ],
                                                        ),

                                                        // Icon in middle
                                                        Container(
                                                            padding: const EdgeInsets.all(4),
                                                            decoration: new BoxDecoration(
                                                                color: Colors.white,
                                                                shape: BoxShape.circle,
                                                            ),
                                                            child: Icon(Icons.arrow_forward,
                                                                color: Colors.black,
                                                                size: 30,
                                                            ),
                                                        )
                                                    ],
                                                )
                                            ),
                                        ),
                                    ));
                            },
                        ),
                    );
                }()
            ],
        );
    }

    /// Build widget and return [Widget] object
    @override
    Widget build(BuildContext context) {
        return DashboardLayout(
            scaffoldKey: this.scaffoldKey,
            title: "Dashboard",
            child: displayView()
        );
    }
}