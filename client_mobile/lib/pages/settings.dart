import 'dart:async';
import 'package:flutter/material.dart';
import 'package:area/object/areas.dart';
import 'package:http/http.dart' as http;
import 'package:area/utils/request.dart';
import 'package:area/layouts/dashboard.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:area/utils/serviceTheme.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsPage extends StatefulWidget {

    @override
    _SettingsPage createState() => _SettingsPage();
}

class _SettingsPage extends State<SettingsPage> {

    // Unique key for scaffold (drawer display)
    final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
    final TextEditingController password = TextEditingController(text: "https://area.epitech-nice.fr");

    /// Init state of the app
    @override
    void initState() {
        super.initState();

    }

    /// Format string and add uppercase to first letter
    String formatString(String src) {
        return('${src[0].toUpperCase()}${src.substring(1)}');
    }

    /// Display content of the application
    Widget displayView() {
        return Column(
            children: <Widget>[
                Padding(padding:  const EdgeInsets.only(top: 300)),
                SizedBox(
                    width: MediaQuery.of(context).size.width * 0.90,
                    child: Material(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5.0)
                        ),
                        elevation: 2,
                        clipBehavior: Clip.antiAlias,
                        child: TextField(
                            controller: this.password,
                            decoration: InputDecoration(
                            ),
                            keyboardAppearance: Brightness.dark,
                        )
                    ),
                ),
                Container(
                    margin: const EdgeInsets.only(top: 20),
                    child: MaterialButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5.0)
                        ),
                        minWidth: MediaQuery.of(context).size.width - 40,
                        elevation: 5,
                        onPressed: () {},
                        color: Theme.of(context).buttonColor,
                        child: Text("Change API Path",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.bold
                            ),
                        ),
                    ),
                ),
            ],
        );
    }

    /// Build widget and return [Widget] object
    @override
    Widget build(BuildContext context) {
        return DashboardLayout(
            scaffoldKey: this.scaffoldKey,
            title: "Settings",
            child: displayView()
        );
    }
}