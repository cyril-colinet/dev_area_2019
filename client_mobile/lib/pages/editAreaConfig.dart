import 'dart:async';
import 'package:flutter/material.dart';
import 'package:area/object/areas.dart';
import 'package:http/http.dart' as http;
import 'package:area/utils/request.dart';
import 'package:area/layouts/dashboard.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:area/utils/serviceTheme.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EditAreaPage extends StatefulWidget {
    final Areas area;

    EditAreaPage({Key key, @required this.area}) : super(key: key);

    @override
    _EditAreaPage createState() => _EditAreaPage(area: this.area);
}

class _EditAreaPage extends State<EditAreaPage> {

    // Unique key for scaffold (drawer display)
    final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
    final Areas area;

    _EditAreaPage({Key key, @required this.area});

    /// Format string and add uppercase to first letter
    String formatString(String src) {
        return('${src[0].toUpperCase()}${src.substring(1)}');
    }

    void removeArea() async {
        final SharedPreferences prefs = await SharedPreferences.getInstance();

        final response = await http.delete("https://area.epitech-nice.fr/api/area/" + this.area.getAreaId().toString(), headers: {
            "Authorization": "Bearer " + prefs.getString("jwt_token"),
            "Accept": "application/json",
        });

        if (response.statusCode == 200) {
            Navigator.pop(context);
        } else {
            print("failed");
        }
    }

    /// Display content of the application
    Widget displayView() {
        return Column(
            children: <Widget>[
                // Display content
                (){
                    return Container(
                        margin: const EdgeInsets.only(top: 20),
                        height: MediaQuery.of(context).size.height - 220,
                        child: ListView.separated(
                            itemCount: 1,
                            separatorBuilder: (context, index) => SizedBox(height: 5),
                            itemBuilder: (BuildContext context, int index) {
                                ThemeInformation actionTheme = ServiceTheme.internal().getThemeInformation(this.area.getActionService());
                                ThemeInformation reactionTheme = ServiceTheme.internal().getThemeInformation(this.area.getReActionService());
                                return InkWell(
                                    onTap: () {},
                                    child: Container(
                                        margin: const EdgeInsets.only(bottom: 10),
                                        padding: const EdgeInsets.symmetric(horizontal: 20),
                                        child: Material(
                                            elevation: 5,
                                            shape: RoundedRectangleBorder(
                                                borderRadius: BorderRadius.circular(10.0)
                                            ),
                                            child: ClipRRect(
                                                borderRadius: BorderRadius.circular(10.0),
                                                child: Stack(
                                                    alignment: Alignment.center,
                                                    children: <Widget>[

                                                        // List
                                                        Row(
                                                            children: <Widget>[
                                                                // Action
                                                                Container(
                                                                    width: MediaQuery.of(context).size.width / 2 - 24,
                                                                    height: 110,
                                                                    decoration: BoxDecoration(
                                                                        color: actionTheme.backgroundColor,
                                                                        gradient: actionTheme.gradient,
                                                                    ),
                                                                    child: Container(
                                                                        padding: const EdgeInsets.only(top: 20, bottom: 20, right: 30, left: 20),
                                                                        child: Column(
                                                                            children: <Widget>[

                                                                                // Header
                                                                                Row(
                                                                                    children: <Widget>[
                                                                                        Icon(actionTheme.icon,
                                                                                            color: actionTheme.color,
                                                                                            size: 20,
                                                                                        ),
                                                                                        SizedBox(width: 5),
                                                                                        Text(actionTheme.title,
                                                                                            style: TextStyle(
                                                                                                color: actionTheme.color,
                                                                                                fontWeight: FontWeight.bold
                                                                                            ),
                                                                                        )
                                                                                    ],
                                                                                ),

                                                                                Container(
                                                                                    alignment: Alignment.centerLeft,
                                                                                    margin: const EdgeInsets.only(top: 10),
                                                                                    child: Text(this.area.getActionDesc(),
                                                                                        textAlign: TextAlign.left,
                                                                                        maxLines: 3,
                                                                                        overflow: TextOverflow.ellipsis,
                                                                                        style: TextStyle(
                                                                                            color: actionTheme.color,
                                                                                            fontSize: 13.5
                                                                                        ),
                                                                                    ),
                                                                                ),
                                                                            ],
                                                                        ),
                                                                    )
                                                                ),

                                                                SizedBox(width: 8),

                                                                // Reaction
                                                                Container(
                                                                    width: MediaQuery.of(context).size.width / 2 - 24,
                                                                    height: 110,
                                                                    decoration: BoxDecoration(
                                                                        color: reactionTheme.backgroundColor,
                                                                        gradient: reactionTheme.gradient,
                                                                    ),
                                                                    child: Container(
                                                                        padding: const EdgeInsets.only(top: 20, bottom: 20, right: 20, left: 30),
                                                                        child: Column(
                                                                            children: <Widget>[

                                                                                // Header
                                                                                Row(
                                                                                    mainAxisAlignment: MainAxisAlignment.end,
                                                                                    children: <Widget>[
                                                                                        Icon(reactionTheme.icon,
                                                                                            color: reactionTheme.color,
                                                                                            size: 20,
                                                                                        ),
                                                                                        SizedBox(width: 5),
                                                                                        Text(reactionTheme.title,
                                                                                            style: TextStyle(
                                                                                                color: reactionTheme.color,
                                                                                                fontWeight: FontWeight.bold
                                                                                            ),
                                                                                        )
                                                                                    ],
                                                                                ),

                                                                                Container(
                                                                                    alignment: Alignment.centerRight,
                                                                                    margin: const EdgeInsets.only(top: 10),
                                                                                    child: Text(this.area.getReActionDesc(),
                                                                                        textAlign: TextAlign.right,
                                                                                        maxLines: 3,
                                                                                        overflow: TextOverflow.ellipsis,
                                                                                        style: TextStyle(
                                                                                            color: reactionTheme.color,
                                                                                            fontSize: 13.5
                                                                                        ),
                                                                                    ),
                                                                                ),
                                                                            ],
                                                                        ),
                                                                    )
                                                                ),
                                                            ],
                                                        ),

                                                        // Icon in middle
                                                        Container(
                                                            padding: const EdgeInsets.all(4),
                                                            decoration: new BoxDecoration(
                                                                color: Colors.white,
                                                                shape: BoxShape.circle,
                                                            ),
                                                            child: Icon(Icons.arrow_forward,
                                                                color: Colors.black,
                                                                size: 30,
                                                            ),
                                                        ),
                                                    ],
                                                )
                                            ),
                                        ),
                                    )
                                );
                            },
                        ),
                    );
                }(),
                Container(
                    margin: const EdgeInsets.only(top: 20, bottom: 20),
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    height: 50,
                    child: MaterialButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0)
                        ),
                        elevation: 5,
                        onPressed: () => this.removeArea(),
                        color: Colors.red,
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                                Container(
                                    margin: const EdgeInsets.only(right: 5),
                                    child: Icon(Icons.add_circle,
                                        color: Colors.white,
                                        size: 20,
                                    ),
                                ),
                                Text("Remove Area",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold
                                    ),
                                ),
                            ],
                        )
                    ),
                ),
            ],
        );
    }

    /// Build widget and return [Widget] object
    @override
    Widget build(BuildContext context) {
        return DashboardLayout(
            scaffoldKey: this.scaffoldKey,
            title: "Edit AREA configuration",
            child: displayView()
        );
    }
}