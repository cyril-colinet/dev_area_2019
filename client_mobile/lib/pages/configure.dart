import 'package:area/components/configure/abstractConfigurator.dart';
import 'package:area/components/configure/actions.dart';
import 'package:area/components/configure/reactions.dart';
import 'package:area/components/configure/summary.dart';
import 'package:area/layouts/dashboard.dart';
import 'package:flutter/material.dart';

/// Create new event with action and reaction
/// This class in an authenticated page
class ConfigurePage extends StatelessWidget {

    /// Unique key for scaffold (drawer display)
    final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

    /// Main controller of the wizard page
    final ConfigureAREA configurationWidget = ConfigureAREA();

    /// Display the list of available actions and reactions by service
    /// Build widget and return [Widget] object
    @override
    Widget build(BuildContext context) {
        return DashboardLayout(
            scaffoldKey: this.scaffoldKey,
            title: "Configure AREA",
            floatingActionButton: Container(
                margin: const EdgeInsets.only(top: 20, bottom: 40),
                padding: const EdgeInsets.symmetric(horizontal: 20),
                height: 50,
                child: MaterialButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0)
                    ),
                    elevation: 5,
                    onPressed: this.configurationWidget.handleNextStepAction(),
                    color: Theme.of(context).buttonColor,
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                            Container(
                                //margin: const EdgeInsets.only(right: 5),
                                child: Icon(Icons.navigate_next,
                                    color: Colors.white,
                                    size: 25,
                                ),
                            ),
                            Text("Next step",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold
                                ),
                            ),
                        ],
                    )
                ),
            ),
            child: Container(
                margin: const EdgeInsets.only(top: 20, bottom: 20),
                child: this.configurationWidget
            ),
        );
    }

}

/// Main manager of the creation of the new AREA configuration
/// This class call other classes in the project (actions and reactions)
class ConfigureAREA extends StatefulWidget {

    final _ConfigureAREA state = _ConfigureAREA();

    /// Handle the press of button
    VoidCallback handleNextStepAction() {
        return () => this.state.goToNextStep();
    }

    @override
    _ConfigureAREA createState() => this.state;
}

/// State of the [StatefulWidget] widget
class _ConfigureAREA extends State<ConfigureAREA> {

    // Class fields
    List<dynamic> _areaConfiguration = [];
    String _currentStep = "actions";
    AConfigurator _currentPage = ConfigureAction();

    /// Apply the configuration from the current page
    void applyTheConfiguration() {
        this._areaConfiguration.add(this._currentPage.selected);
    }

    /// Go to the next step
    void goToNextStep() {
        AConfigurator nextPage = this._currentPage;
        String nextStep = this._currentStep;

        // Call custom handler
        if (this._currentStep == "summary")
            nextPage.handleNextStepButton()();

        // Check if one configuration (action/reaction) is selected
        if (this._currentStep != "summary" && this._currentPage.selected.isEmpty)
            return;

        // Actions -> Reactions
        if (this._currentStep == "actions") {
            this.applyTheConfiguration();

            // Change page to reactions
            nextStep = "reactions";
            nextPage = ConfigureReactions();
        } else if (this._currentStep == "reactions") {
            this.applyTheConfiguration();

            // Change page to summary
            nextStep = "summary";
            nextPage = ConfigureSummary(
                config: () {
                    Map<String, dynamic> serializedConfiguration = {};
                    this._areaConfiguration.forEach((config)
                        => serializedConfiguration.addAll(config));
                    print(serializedConfiguration);
                    return serializedConfiguration;
                }()
            );
        }

        // Rebuild this page with the next step
        this.setState(() {
            this._currentStep = nextStep;
            this._currentPage = nextPage;
        });
    }

    /// Build and render a [Widget]
    /// All pages
    @override
    Widget build(BuildContext context) {
        return this._currentPage;
    }
}

class AreaConfiguration {

    /// Make this class singleton
    static AreaConfiguration _instance = AreaConfiguration.internal();
    AreaConfiguration.internal();
    factory AreaConfiguration() => _instance;

    // Class fields

}