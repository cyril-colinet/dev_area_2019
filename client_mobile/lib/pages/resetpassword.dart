import 'dart:async';
import 'dart:convert';
import 'package:area/layouts/auth.dart';
import 'package:flutter/material.dart';
import 'package:area/utils/request.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class ResetPasswordPage extends StatefulWidget {
  @override
  _ResetPasswordPage createState() => _ResetPasswordPage();
}

class _ResetPasswordPage extends State<ResetPasswordPage> {

  // Class fields
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  final TextEditingController email = TextEditingController();
  final TextEditingController password = TextEditingController();

  // State fields
  String _error;

  void sendResetPasswordMail() {
    if (this.email.text.isEmpty) {
      this.setState(() => this._error = "The email cannot be empty.");
      return;
    } else if (!RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(this.email.text)) {
      this.setState(() => this._error = "Please enter a valid email address.");
      return;
    }

    // Perform request
    postRequest("/api/password/forgot",
        withoutCredentials: true,
        data: {
          "email": this.email.text,
        }
    ).then((data) {
      if (data["result"] == "success") {
        this.scaffoldKey.currentState.showSnackBar(SnackBar(
          backgroundColor: Colors.green,
          duration: Duration(seconds: 5),
          elevation: 10,
          content: Text("An email has been to you email address! Follow the instruciton in the mail to reset your password! You will be redirected in 3 seconds to the login page.",
            style: TextStyle(
                fontSize: 16
            ),
          ),
        ));

        // Redirect to the dashboard page
        var _duration = new Duration(seconds: 2);
        return new Timer(_duration, () {
          Navigator.of(context).pushReplacementNamed('/login');
        });
      } else {
        this.setState(() => this._error = data["message"]);
      }
    });
  }

  /// Build widget
  /// Return a [Widget] with the content of the resetPassword screen
  @override
  Widget build(BuildContext context) {
    if (this._error != null) {
      final SnackBar errorBar = SnackBar(
        backgroundColor: Colors.redAccent,
        duration: Duration(seconds: 5),
        elevation: 10,
        content: Text(this._error,
          style: TextStyle(
              fontSize: 16
          ),
        ),
      );

      // Create snackbar and display it
      this.scaffoldKey.currentState.showSnackBar(errorBar);
      this._error = null;
    }

    return AuthLayout(
      scaffoldKey: this.scaffoldKey,
      child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[

              // Logo
              SizedBox(
                  height: MediaQuery.of(context).size.height / 2 - 100,
                  child: Container(
                    margin: const EdgeInsets.only(top: 50),
                    child: Image.asset("assets/images/logo_dark.png",
                      alignment: Alignment.topCenter,
                      width: MediaQuery.of(context).size.width / 2,
                    ),
                  )
              ),

              // Text fields
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                child: Column(
                  children: <Widget>[
                    Container(
                      margin: const EdgeInsets.only(bottom: 20, top: 10),
                      child: Text("Reset your password",
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 13
                        ),
                      ),
                    ),
                    Divider(),

                    // Username
                    Material(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0)
                      ),
                      elevation: 2,
                      clipBehavior: Clip.antiAlias,
                      child: TextField(
                        autocorrect: false,
                        controller: this.email,
                        decoration: InputDecoration(
                          hintText: "Email address",
                        ),
                        keyboardAppearance: Brightness.dark,
                      ),
                    ),


                  ],
                ),
              ),

              Container(
                margin: const EdgeInsets.only(top: 20),
                child: MaterialButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0)
                  ),
                  minWidth: MediaQuery.of(context).size.width - 60,
                  elevation: 5,
                  onPressed: this.sendResetPasswordMail,
                  color: Theme.of(context).buttonColor,
                  child: Text("Reset password",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                ),
              ),

              Container(
                  margin: const EdgeInsets.only(top: 10, right: 30, left: 30),
                  alignment: Alignment.centerRight,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      InkWell(
                        onTap: () =>  Navigator.of(context).pushReplacementNamed('/login'),
                        child: Text("Log in",
                          style: TextStyle(
                              color: Theme.of(context).buttonColor,
                              fontSize: 14,
                              fontWeight: FontWeight.bold
                          ),
                        ),
                      ),

                      InkWell(
                        onTap: () => Navigator.of(context).pushReplacementNamed('/register'),
                        child: Text("Register",
                          style: TextStyle(
                              color: Theme.of(context).buttonColor,
                              fontSize: 14,
                              fontWeight: FontWeight.bold
                          ),
                        ),
                      ),
                    ],
                  )
              ),
            ],
          )
      ),
    );
  }

}