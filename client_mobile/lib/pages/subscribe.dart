import 'dart:async';
import 'dart:convert';
import 'package:area/utils/serviceTheme.dart';
import 'package:flutter/material.dart';
import 'package:area/utils/request.dart';
import 'package:area/object/service.dart';
import 'package:area/layouts/dashboard.dart';
import 'package:area/pages/transitionscreen.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class SubscribeView extends StatefulWidget {
    @override
    _SubscribeView createState() => _SubscribeView();
}

class _SubscribeView extends State<SubscribeView> {

    final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

    // Class fields
    List<Service> _services = [];
    bool _loaded = false;
    String _errorMessage;
    String _successMessage;


    RaisedButton generateButton(Service service) {
      if (service.getLogStatus()) {
        return new RaisedButton(
          child: Text("Registered"),
          color: Colors.blueGrey,
          disabledColor: Colors.green,
        );
      } else {
        return new RaisedButton(
          child: Text("Log in"),
          onPressed: handleOAuthLoginService(service.getServiceId(), service.getName()),
          color: Colors.white,
        );
      }
    }

    VoidCallback handleOAuthLoginService(int id, String name) {
        return () {
            postRequest("/api/services/subscribe", data: {'service_id': id})
                .then((data) {
                    // Display webview
                    if (data["result"] == "ok" && data["link_oauth"] != null) {
                        FlutterWebviewPlugin webviewPlugin = FlutterWebviewPlugin();
                        StreamSubscription<WebViewStateChanged> listener;

                        // Listen to end of webview
                        void onStateChanged(WebViewStateChanged state) {
                            if (state.url.startsWith("https://area.epitech-nice.fr/")) {
                                if (state.type == WebViewState.shouldStart)
                                    webviewPlugin.hide();

                                if (state.type == WebViewState.finishLoad) {
                                    webviewPlugin.evalJavascript("document.documentElement.innerHTML").then((body) {
                                        RegExp exp = RegExp(
                                            r"<[^>]*>",
                                            multiLine: true,
                                            caseSensitive: true
                                        );
                                        return body.replaceAll(exp, '');
                                    }).then((resultJson) {
                                        Map<String, dynamic> result = json.decode(resultJson);

                                        // Check if result exists
                                        if (result["result"] == null || result["result"] != "ok") {
                                            this.setState(() {
                                                this._errorMessage = "We have som trouble to login your account. Please try again later.";
                                            });
                                            return;
                                        }

                                        // Close webview and destroy it
                                        webviewPlugin.close();
                                        listener.cancel();
                                        webviewPlugin.dispose();

                                        // Change values of variables
                                        this.setState(() {

                                            // Fake change of the request
                                            this._errorMessage = null;
                                            this._successMessage = "Successfully connected to " + name + "!";
                                            Navigator.pop(context);
                                        });
                                    });
                                }
                            }
                        }

                        // Listener
                        listener = webviewPlugin.onStateChanged.listen(onStateChanged);

                        // Show bottom modal
                        showModalBottomSheet(
                            isDismissible: false,
                            isScrollControlled: true,
                            context: context,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0)
                            ),
                            builder: (BuildContext context) {
                                return Container(
                                    height: MediaQuery.of(context).size.height - 100,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(20),
                                            topRight: Radius.circular(20),
                                        )
                                    ),
                                    child: Column(
                                        children: <Widget>[
                                            Container(
                                                height: 50,
                                                padding: const EdgeInsets.symmetric(horizontal: 10),
                                                child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                    children: <Widget>[
                                                        InkWell(
                                                            onTap: () {
                                                                webviewPlugin.close();
                                                                listener.cancel();
                                                                webviewPlugin.dispose();

                                                                // Close bottom sheet
                                                                Navigator.pop(context);
                                                            },
                                                            child: Icon(Icons.close,
                                                                size: 30,
                                                                color: Color(0xFF131313)
                                                            ),
                                                        ),

                                                        Text("Login to " + name + " service",
                                                            style: TextStyle(
                                                                color: Color(0xFF131313),
                                                                fontWeight: FontWeight.bold,
                                                                fontSize: 18
                                                            ),
                                                        ),
                                                    ],
                                                ),
                                            ),
                                            Expanded(
                                                child: WebviewScaffold(
                                                    userAgent: "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36",
                                                    clearCache: true,
                                                    clearCookies: true,
                                                    withZoom: false,
                                                    withJavascript: true,
                                                    url: data["link_oauth"],
                                                ),
                                            )
                                        ],
                                    )
                                );
                            }
                        );
                    }
                });
        };
    }

    Future getData() {
        return Future.wait([this.getServiceList()]);
    }

    Future getServiceList() {
        Future<dynamic> request = getRequest("/api/services/user");

        return request.then((data) {
            List<dynamic> values = data["data"];
            for (int i = 0; i < values.length; i++) {
                Service tmp = Service.fromJson(values[i]);
                if (tmp.getOauthNeed())
                  _services.add(tmp);
            }
        });
    }

    Future goToOauthLink(int serviceId, BuildContext context) {
        Future<Map<String, dynamic>> request = postRequest("services/subscribe", withoutCredentials: false, data: {"service_id": serviceId.toString()});

        request.then((data) {
            print("in function");
        });
    }

    String formatString(String src) {
        return('${src[0].toUpperCase()}${src.substring(1)}');
    }

    @override
    void initState() {
        super.initState();

        this.getData().then((res) => this.setState(() {
            _loaded = true;
        }));
    }

    Widget displayView() {
        return Container(
            margin: const EdgeInsets.only(top: 20),
            padding: const EdgeInsets.symmetric(horizontal: 20),
            height: ((MediaQuery.of(context).size.height / 3) *  1.8) + 110,
            child: ListView.separated(
                separatorBuilder: (context, index) => SizedBox(height: 10),
                itemCount: this._services.length,
                itemBuilder: (BuildContext context, int index) {
                    ThemeInformation themeInfo = ServiceTheme.internal().getThemeInformation(this._services[index].getName());

                    return Material(
                        elevation: 5,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
                        color: themeInfo.backgroundColor,
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(10.0),
                            child: Container(
                                decoration: BoxDecoration(
                                    color: themeInfo.backgroundColor,
                                    gradient: themeInfo.gradient
                                ),
                                child: ListTile(
                                    leading: Icon(themeInfo.icon, color: themeInfo.color),
                                    title: Text(formatString(_services[index].getName()),
                                        style: TextStyle(
                                            color: themeInfo.color,
                                            fontWeight: FontWeight.bold
                                        )
                                    ),
                                    trailing: generateButton(_services[index])
                                )
                            ),
                        ),
                    );
                },
            )
        );
    }

    Widget build(BuildContext context) {
        if (!_loaded)
            return new TransitionScreen();

        return DashboardLayout(
            scaffoldKey: this.scaffoldKey,
            title: "OAuth Services",
            child: displayView()
        );
    }
}
